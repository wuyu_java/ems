package org.lq.student.entity;


import lombok.ToString;
@ToString
public class EvaluationInfo {

  private long evaluationId;
  private long studentId;
  private String evaluationTitle;
  private String evaluationContent;
  private String evaluationCourse;
  private String evaluationTeacher;
  private java.sql.Timestamp evaluationTime;


  public long getEvaluationId() {
    return evaluationId;
  }

  public void setEvaluationId(long evaluationId) {
    this.evaluationId = evaluationId;
  }


  public long getStudentId() {
    return studentId;
  }

  public void setStudentId(long studentId) {
    this.studentId = studentId;
  }


  public String getEvaluationTitle() {
    return evaluationTitle;
  }

  public void setEvaluationTitle(String evaluationTitle) {
    this.evaluationTitle = evaluationTitle;
  }


  public String getEvaluationContent() {
    return evaluationContent;
  }

  public void setEvaluationContent(String evaluationContent) {
    this.evaluationContent = evaluationContent;
  }


  public String getEvaluationCourse() {
    return evaluationCourse;
  }

  public void setEvaluationCourse(String evaluationCourse) {
    this.evaluationCourse = evaluationCourse;
  }


  public String getEvaluationTeacher() {
    return evaluationTeacher;
  }

  public void setEvaluationTeacher(String evaluationTeacher) {
    this.evaluationTeacher = evaluationTeacher;
  }


  public java.sql.Timestamp getEvaluationTime() {
    return evaluationTime;
  }

  public void setEvaluationTime(java.sql.Timestamp evaluationTime) {
    this.evaluationTime = evaluationTime;
  }


}
