package org.lq.student.entity;

import lombok.Data;

/**
 * @author 秦洪涛
 * @create 2020-10-13-18:16
 */
@Data
public class StudentInfo {
    private int studentId;//学员编号
    private int staffId;//负责人
    private int classId;//班级编号
    private String studentName;//学员姓名
    private String studentSex;//学员性别
    private int studentAge;//学员年龄
    private String studentTellphone;//学员电话
    private String studentEmail;//邮箱
    private String studentIdcard;//身份证
    private String studentAddress;//家庭住址
    private String studentBirthday;//出生日期
    private String studentSchool;//所在院校
    private String studentQQ;//qq号
    private String studentParentsName;//家长姓名
    private String studentParentsPhone;//家长电话
    private String studentPro;//省份
    private String studentProCity;//城市
    private String studentType;//类型
    private String studentIspay;//是否缴费
    private int studentSate;//意向状态
    private String studentMark;//标识
    private String studentDesc;//描述
    private String studentNumber;//账号
    private String studentPassword;//密码

}
