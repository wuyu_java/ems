package org.lq.student.entity;

import lombok.Data;

/**
 * 学生成绩表
 */
@Data
public class StudentWriteGrade {
  private long writeGradeId;//成绩编号
  private long studentId;//学员编号
  private long staffId;//监考人和阅卷人
  private String writeGradeSubject;//科目
  private String writeGrade;//成绩
  private java.sql.Timestamp writeGradeTime;//考试时间
  private String writeGradeNote;//备注
}
