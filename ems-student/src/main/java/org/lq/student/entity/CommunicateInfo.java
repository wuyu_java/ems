package org.lq.student.entity;

import lombok.Data;

@Data
public class CommunicateInfo {

  private long communicateId;//记录编号
  private long studentId;//学员编号
  private long staffId;//负责人编号
  private java.sql.Timestamp communicateTime;//沟通时间
  private String communicateContent;//沟通内容


  public long getCommunicateId() {
    return communicateId;
  }

  public void setCommunicateId(long communicateId) {
    this.communicateId = communicateId;
  }


  public long getStudentId() {
    return studentId;
  }

  public void setStudentId(long studentId) {
    this.studentId = studentId;
  }


  public long getStaffId() {
    return staffId;
  }

  public void setStaffId(long staffId) {
    this.staffId = staffId;
  }


  public java.sql.Timestamp getCommunicateTime() {
    return communicateTime;
  }

  public void setCommunicateTime(java.sql.Timestamp communicateTime) {
    this.communicateTime = communicateTime;
  }


  public String getCommunicateContent() {
    return communicateContent;
  }

  public void setCommunicateContent(String communicateContent) {
    this.communicateContent = communicateContent;
  }

}
