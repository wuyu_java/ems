package org.lq.student.dao;

import org.lq.base.BaseDao;
import org.lq.student.entity.EvaluationInfo;

import java.util.List;

/**
 * @author hello
 * @create 2020-10 -13
 */
public interface EvaluationInfoDao extends BaseDao<EvaluationInfo> {

    @Override
//    List<EvaluationInfo> pageByValues(int startIndex, int pageSize, String... value);

    default int getCount(String... values){
        return 0;
    }

    default List<EvaluationInfo> pageByValues(int startIndex, int pageSize, String... value){
        return null;
    }

    int getCount(String value);
    List<EvaluationInfo> pageByValues(int startIndex, int pageSize, String value);




}
