package org.lq.student.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.student.dao.CommunicateInfoDao;
import org.lq.student.entity.CommunicateInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 南以南
 * @create 2020-10-13 18:35
 */
@Log4j
public class CommunicateInfoDaoImpl implements CommunicateInfoDao {
    /**
     * 添加沟通记录
     * @param communicateInfo
     * @return
     */
    @Override
    public int save(CommunicateInfo communicateInfo) {
        log.info("添加沟通记录");
        QueryRunner qr= new QueryRunner(JDBCUtil.getDataSource());
        try {
            return qr.update("insert into communicate_info(communicate_time,communicate_content,student_id,staff_id)values (now(),?,?,?)",
                    communicateInfo.getCommunicateContent(),communicateInfo.getStudentId(),communicateInfo.getStaffId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("添加沟通记录失败");
        }

        return 0;
    }

    /**
     * 修改沟通记录
     * @param communicateInfo
     * @return
     */
    @Override
    public int update(CommunicateInfo communicateInfo) {
        log.info("修改沟通记录");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return qr.update("update communicate_info set communicate_content=?,student_id=?,staff_id=? where communicate_id=?",
                    communicateInfo.getCommunicateContent(),communicateInfo.getStudentId(),communicateInfo.getStaffId(),communicateInfo.getCommunicateId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("修改沟通记录失败");
        }
        return 0;
    }

    /**
     * 删除沟通记录
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("删除沟通记录");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return new QueryRunner(JDBCUtil.getDataSource()).update("delete from communicate_info where communicate_id=?",id);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除沟通记录失败");
        }

        return 0;
    }

    /**
     * 根据id获取沟通记录
     * @param id
     * @return
     */
    @Override
    public CommunicateInfo getById(int id) {
        log.info("通过id获取沟通记录");
        try {
            return new QueryRunner(JDBCUtil.getDataSource()).query(
                    "select * from communicate_view where communicateid=?",
                    new BeanHandler<>(CommunicateInfo.class),
                    id);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据id获取沟通记录失败");
        }
        return null;
    }

    /**
     * 获取总行数
     * @return
     */
    @Override
    public int getCount() {
        log.info("获取总行数");
        int count = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long num = qr.query("select count(1) from communicate_info", new ScalarHandler<Long>());
            count = num.intValue();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("获取总行数失败");
        }
        return count;
    }

    /**
     * 页面显示
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<CommunicateInfo> pageList(int startIndex, int pageSize) {
        log.info("起始下标"+startIndex+"显示行数"+pageSize+"页面显示成功");
        List<CommunicateInfo> list = new ArrayList<>();
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = qr.query("SELECT * FROM communicate_view LIMIT ?,?",
                    new BeanListHandler<CommunicateInfo>(CommunicateInfo.class),
                    startIndex,pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("页面显示失败");
        }
        return list;
    }

    /**
     * 根据姓名查询
     * @param name
     * @return
     */
    @Override
    public int getCount(String name) {
        log.info("根据姓名查询");
        int count = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long num = qr.query("select * from communicate_view inner join v_student_info on communicate_view.studentid=v_student_info.studentID where studentName like ?", new ScalarHandler<Long>(),"%"+name +"%");
            count = num.intValue();

        } catch (Exception e) {
            e.printStackTrace();
            log.error("根据姓名查询失败");
        }
        return count;
    }

    /**
     * 根据姓名分页查询
     * @param startIndex
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public List<CommunicateInfo> pageByValues(int startIndex, int pageSize, String name) {
        log.info("根据姓名分页查询");
        List<CommunicateInfo> list = new ArrayList<>();
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = qr.query("select * from communicate_view inner join v_student_info on communicate_view.studentid=v_student_info.studentID where studentName like ? LIMIT ?,?",
                    new BeanListHandler<CommunicateInfo>(CommunicateInfo.class),
                    "%"+name+"%",startIndex,pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("根据姓名分页查询失败");
        }
        return list;
    }



}
