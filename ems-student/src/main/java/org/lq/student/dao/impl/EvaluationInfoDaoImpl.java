package org.lq.student.dao.impl;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;
import org.lq.student.dao.EvaluationInfoDao;
import org.lq.student.entity.EvaluationInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hello
 * @create 2020-10 -13
 */
public class EvaluationInfoDaoImpl implements EvaluationInfoDao {
    public  static  final Logger log = Logger.getLogger(EvaluationInfoDaoImpl.class);
    @Override
    public int save(EvaluationInfo e) {
        log.info("添加评价成功");
        try {
            return new QueryRunner(JDBCUtil.getDataSource()).update("insert into evaluation_info(student_id,evaluation_title,evaluation_content,evaluation_course,evaluation_teacher,evaluation_time) values (?,?,?,?,?,?)",
                    e.getStudentId(),e.getEvaluationTitle(),e.getEvaluationContent(),e.getEvaluationCourse(),e.getEvaluationTeacher(),e.getEvaluationTime());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("添加评价失败");

        }
        return 0;
    }

    @Override
    public int update(EvaluationInfo s) {
        log.info("修改评价数据成功");

        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return qr.update("update evaluation_info set student_id=?,evaluation_title=?, evaluation_content=?, evaluation_course=?, evaluation_teacher=?, evaluation_time=? where evaluation_id=?",
                    s.getStudentId(),s.getEvaluationTitle(),s.getEvaluationContent(),s.getEvaluationCourse(),s.getEvaluationTeacher(),s.getEvaluationTime(),s.getEvaluationId());



        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("修改评价失败");

        }

        return 0;
    }

    @Override
    public int delete(int id) {
        log.info("删除数据成功");

        try {
            return new QueryRunner(JDBCUtil.getDataSource()).update("delete from evaluation_info where evaluation_id=?",id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.info("删除数据失败");

        }
        return 0;
    }

    @Override
    public EvaluationInfo getById(int id) {
        log.info("根据id查找成功");

        try {
            return  new QueryRunner(JDBCUtil.getDataSource()).query("select * from evaluation_info_view where evaluationid = ?",
                    new BeanHandler<EvaluationInfo>(EvaluationInfo.class),id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("根据id查找失败");

        }
        return null;
    }

    @Override
    public int getCount() {
        log.info("显示全部数据");
        int count = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long num = qr.query("select count(1) from evaluation_info", new ScalarHandler<Long>());
            count = num.intValue();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("显示全部数据");

        }
        return count;
    }

    @Override
    public List<EvaluationInfo> pageList(int startIndex, int pageSize) {
        log.info("起始下标："+startIndex+"  每行显示数据："+pageSize+",分页显示成功");

        List<EvaluationInfo> list = new ArrayList<>();
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = qr.query("SELECT * FROM evaluation_info_view LIMIT ?,?",
                    new BeanListHandler<EvaluationInfo>(EvaluationInfo.class),
                    startIndex,pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("起始下标："+startIndex+"  每行显示数据："+pageSize+",分页显示失败");

        }
        return list;
    }


    @Override
    public int getCount(String title) {
        log.info("根据主题查询成功");
        try {
            return new QueryRunner(JDBCUtil.getDataSource()).query("select count(1) from evaluation_info where evaluation_title like ?",
                    new ScalarHandler<Long>(),"%"+title+"%").intValue();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("根据主题查询失败");

        }
        return 0;
    }

    @Override
    public List<EvaluationInfo> pageByValues(int startIndex, int pageSize, String title) {
        log.info("起始下标："+startIndex+"  每页显示数据："+pageSize+",查询成功");

        try {
            return new QueryRunner(JDBCUtil.getDataSource()).query("select * from evaluation_info_view where evaluationTitle like ? limit ?,? ",
                    new BeanListHandler<EvaluationInfo>(EvaluationInfo.class),"%"+title+"%",startIndex,pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("起始下标："+startIndex+"  每页显示数据："+pageSize+",查询失败");

        }
        return null;
    }
}
