package org.lq.student.dao;

import org.lq.base.BaseDao;
import org.lq.student.entity.CommunicateInfo;

import java.util.List;

/**
 * @author 南以南
 * @create 2020-10-13 18:26
 */

public interface CommunicateInfoDao extends BaseDao<CommunicateInfo> {
    default int getCount(String... values){
        return 0;
    }

    default List<CommunicateInfo> pageByValues(int startIndex, int pageSize, String...value) {

        return null;
    }
    int getCount (String values);
    List<CommunicateInfo> pageByValues ( int startIndex, int pageSize, String value);




}
