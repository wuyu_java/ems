package org.lq.student.dao;

import org.lq.base.BaseDao;
import org.lq.student.entity.StudentInfo;

import java.util.List;

/**
 * @author 秦洪涛
 * @create 2020-10-13-18:25
 */
public interface StudentInfoDao extends BaseDao<StudentInfo> {

    StudentInfo getStudentByName(String name);

    int getCount(String value);

    List<StudentInfo> pageByValues(int startIndex, int pageSize, String value);

    @Override
    default int getCount(String... values) {
        return 0;
    }

    @Override
    default List<StudentInfo> pageByValues(int startIndex, int pageSize, String... value) {
        return null;
    }
}
