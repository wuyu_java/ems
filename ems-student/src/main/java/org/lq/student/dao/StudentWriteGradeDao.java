package org.lq.student.dao;

import org.lq.base.BaseDao;
import org.lq.student.entity.StudentWriteGrade;

import java.util.List;

/**
 * @author haha
 * @create 2020-10-13 18:30
 */
public interface StudentWriteGradeDao extends BaseDao<StudentWriteGrade> {

    @Override
    default int getCount(String... values){return  0;}
    @Override
    default List<StudentWriteGrade> pageByValues(int startIndex,int pageSize,String...value){return null;}
    //根据姓名模糊查询，查找总行数
    int getCount(String values);
    //分页的模糊查询
    List<StudentWriteGrade> pageByValues(int startIndex,int pageSize,String value);
}
