package org.lq.education.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.education.dao.ClassTransactionInfoDao;
import org.lq.education.entity.ClassTransactionInfo;
import org.lq.util.JDBCUtil;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HXD
 * @create 2020-10-13 18:23
 */
@Log4j
public class ClassTransactionInfoDaoImpl implements ClassTransactionInfoDao {
    DataSource dataSource = JDBCUtil.getDataSource();
    /**
     * 添加
     *
     * @param c
     * @return
     */
    @Override
    public int save(ClassTransactionInfo c) {
        int num= 0;
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入添加");
           num = queryRunner.update("insert into class_transaction_info(" +
                            "class_id, " +
                            "class_transaction_title," +
                            "class_transaction_content, " +
                            "class_transaction_person, " +
                            "class_transaction_time, " +
                            "class_transaction_remark) values (?,?,?,?,?,?)",
                        c.getClassId(),
                        c.getClassTransactionTitle(),
                        c.getClassTransactionContent(),
                        c.getClassTransactionPerson(),
                        c.getClassTransactionTime(),
                        c.getClassTransactionRemark());
            log.info("数据访问层,添加结果"+ num );
        } catch (SQLException throwables) {
            log.error("数据访问层,添加错误",throwables);
        }
        return num;
    }

    /**
     * 修改
     *
     * @param c
     * @return
     */
    @Override
    public int update(ClassTransactionInfo c) {
        int num= 0;
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入修改");
           num = queryRunner.update("update class_transaction_info set " +
                            "class_id = ? ,"+
                            "class_transaction_title =?," +
                            "class_transaction_content =?, " +
                            "class_transaction_person =?, " +
                            "class_transaction_time =?, " +
                            "class_transaction_remark=? " +
                            "where class_transaction_id =?",
                    c.getClassId(),
                    c.getClassTransactionTitle(),
                    c.getClassTransactionContent(),
                    c.getClassTransactionPerson(),
                    c.getClassTransactionTime(),
                    c.getClassTransactionRemark(),
                    c.getClassTransactionId());
            log.info("数据访问层,修改结果："+num);
        } catch (SQLException throwables) {
            log.error("数据访问层,修改错误",throwables);
        }
        return num;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入删除");
            num = queryRunner.update("delete from class_transaction_info where class_transaction_id =?",id);
            log.info("数据访问层,删除结果"+num);
        } catch (SQLException throwables) {
            log.error("数据访问层,删除错误",throwables);
        }
        return num;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public ClassTransactionInfo getById(int id) {
        ClassTransactionInfo c = new ClassTransactionInfo();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入id查询");
            c  = queryRunner.query("select *from class_transaction_info where class_transaction_id =?",
                    new BeanHandler<>(ClassTransactionInfo.class), id);
            log.info("数据访问层,id查询结果："+c);
        } catch (SQLException throwables) {
            log.error("数据访问层,id查询错误："+throwables);
        }
        return c;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入总行数查询");
            Long aLong = queryRunner.query("select count(1) from class_transaction_info", new ScalarHandler<Long>());
            num = aLong.intValue();
            log.info("数据访问层,总行数查询结果："+num);
        } catch (SQLException throwables) {
            log.error("数据访问层,总行数查询错误",throwables);
        }

        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<ClassTransactionInfo> pageList(int startIndex, int pageSize) {
        List<ClassTransactionInfo> list = new ArrayList<>();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入分页查询");
            list = queryRunner.query("select *from class_transaction_info limit ? , ? ",
                    new BeanListHandler<ClassTransactionInfo>(ClassTransactionInfo.class),startIndex,pageSize);
            log.info("数据访问层,分页查询结果："+list);
        } catch (SQLException throwables) {
            log.error("数据访问层,分页查询失败："+throwables);
        }
        return list;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入根据条件查询总行数");

            // TODO: 条件未知
            Long aLong = queryRunner.query("select count(1) from class_transaction_info where class_transaction_title like ?",
                    new ScalarHandler<Long>(),"%"+values[0]+"%");
            num = aLong.intValue();
            log.info("数据访问层,根据条件查询总行数结果"+num);

        } catch (SQLException throwables) {
            log.error("数据访问层,根据条件查询总行数错误");

        }

        return num;
    }

    /**
     * 根据条件分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<ClassTransactionInfo> pageByValues(int startIndex, int pageSize, String... value) {
        List<ClassTransactionInfo> list = new ArrayList<>();
        QueryRunner queryRunner = new QueryRunner(dataSource);
        try {
            log.info("数据访问层,进入根据条件分页查询");

            // TODO: 条件未知
            list = queryRunner.query("select *from class_transaction_info where class_transaction_title like ? limit ? , ? ",
                    new BeanListHandler<ClassTransactionInfo>(ClassTransactionInfo.class),"%"+value[0]+"%",startIndex,pageSize);
            log.info("数据访问层,根据条件分页查询结果："+list);

        } catch (SQLException throwables) {
            log.error("数据访问层,根据条件分页查询错误",throwables);

        }
        return list;
    }
}
