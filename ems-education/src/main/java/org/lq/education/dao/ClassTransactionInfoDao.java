package org.lq.education.dao;

import org.lq.base.BaseDao;
import org.lq.education.entity.ClassTransactionInfo;

/**
 * @author HXD
 * @create 2020-10-13 18:22
 */
public interface ClassTransactionInfoDao extends BaseDao<ClassTransactionInfo> {
}
