package org.lq.education.dao;

import org.lq.base.BaseDao;
import org.lq.education.entity.AttendanceInfo;

/**
 * @author 崔竞辉
 * @create 2020-10-13 18:17
 */
public interface AttendanceDao extends BaseDao<AttendanceInfo>{

}
