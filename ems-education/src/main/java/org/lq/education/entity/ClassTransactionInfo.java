package org.lq.education.entity;

import lombok.Data;

/**
 * 班级事务信息
 *  @author HXD
 *  @create 2020-10-13 18:19
 *
 */
@Data
public class ClassTransactionInfo {

  private int classTransactionId;//班级事务信息编号
  private int classId;//班级编号
  private String classTransactionTitle;//主题
  private String classTransactionContent;//内容
  private String classTransactionPerson;//组织人
  private java.sql.Timestamp classTransactionTime;//活动日期
  private String classTransactionRemark;//备注信息
}
