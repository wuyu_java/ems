package org.lq.education.entity;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @author 崔竞辉
 * @create 2020-10-13 18:13
 */
@Data
public class AttendanceInfo {
    private int attendanceId;//考勤编号
    private int studentId;//学员编号
    private String attendanceDesc;//描述
    private String attendanceState;//状态
    private Timestamp attendanceTime;//日期
    private String attendanceRemark;//备注
}
