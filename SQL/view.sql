-- 秦洪涛 student_info
create view v_student_info  as
select `student_info`.`student_id` AS `studentID`,`student_info`.`staff_id` AS `staffId`,`student_info`.`class_id` AS `classId`,`student_info`.`student_name` AS `studentName`,`student_info`.`student_sex` AS `studentSex`,`student_info`.`student_age` AS `studentAge`,`student_info`.`student_tellphone` AS `studentTellphone`,`student_info`.`student_email` AS `studentEmail`,`student_info`.`student_idcard` AS `studentIdcard`,`student_info`.`student_address` AS `studentAddress`,`student_info`.`student_birthday` AS `studentBirthday`,`student_info`.`student_school` AS `studentSchool`,`student_info`.`student_qq` AS `studentQQ`,`student_info`.`student_parents_name` AS `studentParentsName`,`student_info`.`student_parents_phone` AS `studentParentsPhone`,`student_info`.`student_pro` AS `studentPro`,`student_info`.`student_pro_city` AS `studentProCity`,`student_info`.`student_type` AS `studentType`,`student_info`.`student_ispay` AS `studentIspay`,`student_info`.`student_sate` AS `studentSate`,`student_info`.`student_mark` AS `studentMark`,`student_info`.`student_desc` AS `studentDesc`,`student_info`.`student_number` AS `studentNumber`,`student_info`.`student_password` AS `studentPassword` from `student_info`

CREATE v_class_info as
SELECT
    `class_info`.`class_id` AS `classId`,
    `class_info`.`discipline_id` AS `disciplineId`,
    `class_info`.`syllabus_id` AS `syllabusId`,
    `class_info`.`classroom_id` AS `classroomId`,
    `class_info`.`staff_id` AS `staffId`,
    `class_info`.`class_name` AS `className`,
    `class_info`.`class_number` AS `classNumber`,
    `class_info`.`class_start_time` AS `classStartTime`,
    `class_info`.`class_end_time` AS `classEndTime`,
    `class_info`.`class_isuesd` AS `classIsuesd`,
    `class_info`.`class_state` AS `classState`,
    `class_info`.`class_desc` AS `classDesc`
FROM
    `class_info`

CREATE v_classroom_info AS
SELECT
    `classroom_info`.`classroom_id` AS `classroomId`,
    `classroom_info`.`classroom_name` AS `classroomName`,
    `classroom_info`.`classroom_max` AS `classroomMax`,
    `classroom_info`.`classroom_info` AS `classroomInfo`,
    `classroom_info`.`classroom_remark` AS `classroomRemark`,
    `classroom_info`.`classroom_mark` AS `classroomMark`
FROM
    `classroom_info`

CREATE v_discipline As
SELECT
    `discipline_info`.`discipline_id` AS `disciplineId`,
    `discipline_info`.`discipline_name` AS `disciplineName`,
    `discipline_info`.`discipline_tuition` AS `disciplineTuition`,
    `discipline_info`.`discipline_bring` AS `disciplineBring`,
    `discipline_info`.`discipline_desc` AS `disciplineDesc`,
    `discipline_info`.`discipline_isuesd` AS `disciplineIsuesd`
FROM
    `discipline_info`

create view communicate_view as
select `communicate_info`.`communicate_id` AS `communicateId`,`communicate_info`.`student_id` AS `studentId`,`communicate_info`.`staff_id` AS `staffId`,`communicate_info`.`communicate_time` AS `communicateTime`,`communicate_info`.`communicate_content` AS `communicateContent` from `communicate_info`

create view studentpayment as
select `student_payment`.`payment_id` AS `paymentId`,`student_payment`.`student_id` AS `studentId`,`student_payment`.`staff_id` AS `staffId`,`student_payment`.`payment_situation` AS `paymentSituation`,`student_payment`.`payment_method` AS `paymentMethod`,`student_payment`.`payment_time` AS `paymentTime`,`student_payment`.`discount_amount` AS `discountAmount`,`student_payment`.`should_amount` AS `shouldAmount`,`student_payment`.`real_amount` AS `realAmount`,`student_payment`.`debt_amount` AS `debtAmount`,`student_payment`.`payment_remark` AS `paymentremark` from `student_payment`

create view class_transaction_info as
select `class_transaction_info`.`class_transaction_id` AS `classTransactionId`,`class_transaction_info`.`class_id` AS `classId`,`class_transaction_info`.`class_transaction_title` AS `classTransactionTitle`,`class_transaction_info`.`class_transaction_content` AS `classTransactionContent`,`class_transaction_info`.`class_transaction_person` AS `classTransactionPerson`,`class_transaction_info`.`class_transaction_time` AS `classTransactionTime`,`class_transaction_info`.`class_transaction_remark` AS `classTransactionRemark` from `class_transaction_info`

create view attendanceinfo as
select `attendance_info`.`attendance_id` AS `attendanceId`,`attendance_info`.`student_id` AS `studentId`,`attendance_info`.`attendance_desc` AS `attendanceDesc`,`attendance_info`.`attendance_state` AS `attendanceState`,`attendance_info`.`attendance_time` AS `attendanceTime`,`attendance_info`.`attendance_remark` AS `attendanceRemark` from `attendance_info`

create view  v_staff_salary as
select `staff_salary`.`sta_staff_id` AS `staStaffId`,`staff_salary`.`staff_salary_id` AS `staffSalaryId`,`staff_salary`.`staff_id` AS `staffId`,`staff_salary`.`total_salary` AS `totalSalary`,`staff_salary`.`deduct_salary` AS `deductSalary`,`staff_salary`.`real_salary` AS `realSalary`,`staff_salary`.`is_uesd` AS `isUesd`,`staff_salary`.`staff_remark` AS `staffRemark`,`staff_salary`.`staff_salary_time` AS `staffSalaryTime` from `staff_salary`



CREATE v_class_info as
SELECT
    `class_info`.`class_id` AS `classId`,
    `class_info`.`discipline_id` AS `disciplineId`,
    `class_info`.`syllabus_id` AS `syllabusId`,
    `class_info`.`classroom_id` AS `classroomId`,
    `class_info`.`staff_id` AS `staffId`,
    `class_info`.`class_name` AS `className`,
    `class_info`.`class_number` AS `classNumber`,
    `class_info`.`class_start_time` AS `classStartTime`,
    `class_info`.`class_end_time` AS `classEndTime`,
    `class_info`.`class_isuesd` AS `classIsuesd`,
    `class_info`.`class_state` AS `classState`,
    `class_info`.`class_desc` AS `classDesc`
FROM
    `class_info`


CREATE v_classroom_info AS
SELECT
    `classroom_info`.`classroom_id` AS `classroomId`,
    `classroom_info`.`classroom_name` AS `classroomName`,
    `classroom_info`.`classroom_max` AS `classroomMax`,
    `classroom_info`.`classroom_info` AS `classroomInfo`,
    `classroom_info`.`classroom_remark` AS `classroomRemark`,
    `classroom_info`.`classroom_mark` AS `classroomMark`
FROM
    `classroom_info`

CREATE v_discipline As
SELECT
    `discipline_info`.`discipline_id` AS `disciplineId`,
    `discipline_info`.`discipline_name` AS `disciplineName`,
    `discipline_info`.`discipline_tuition` AS `disciplineTuition`,
    `discipline_info`.`discipline_bring` AS `disciplineBring`,
    `discipline_info`.`discipline_desc` AS `disciplineDesc`,
    `discipline_info`.`discipline_isuesd` AS `disciplineIsuesd`
FROM
    `discipline_info`

CREATE v_discipline_info AS
SELECT
    `discipline_info`.`discipline_id` AS `discipline_id`,
    `discipline_info`.`discipline_name` AS `discipline_name`,
    `discipline_info`.`discipline_tuition` AS `discipline_tuition`,
    `discipline_info`.`discipline_bring` AS `discipline_bring`,
    `discipline_info`.`discipline_desc` AS `discipline_desc`,
    `discipline_info`.`discipline_isuesd` AS `discipline_isuesd`,
    `discipline_info`.`discipline_id` AS `disciplineId`,
    `discipline_info`.`discipline_name` AS `disciplineName`,
    `discipline_info`.`discipline_tuition` AS `disciplineTuition`,
    `discipline_info`.`discipline_bring` AS `disciplineBring`,
    `discipline_info`.`discipline_desc` AS `disciplineDesc`,
    `discipline_info`.`discipline_isuesd` AS `disciplineIsuesd`
FROM
    `discipline_info`

CREATE v_staff_info AS
SELECT
    `staff_info`.`staff_id` AS `staffId`,
    `staff_info`.`role_id` AS `roleId`,
    `staff_info`.`staff_name` AS `staffName`,
    `staff_info`.`staff_sex` AS `staffSex`,
    `staff_info`.`staff_age` AS `staffAge`,
    `staff_info`.`staff_native_place` AS `staffNativePlace`,
    `staff_info`.`staff_idcard` AS `staffIdcard`,
    `staff_info`.`staff_brithday` AS `staffBrithday`,
    `staff_info`.`staff_office_phone` AS `staffOfficePhone`,
    `staff_info`.`staff_mobile_phone` AS `staffMobilePhone`,
    `staff_info`.`staff_eamil` AS `staffEamil`,
    `staff_info`.`staff_addr` AS `staffAddr`,
    `staff_info`.`staff_qq` AS `staffQq`,
    `staff_info`.`staff_entry_time` AS `staffEntryTime`,
    `staff_info`.`staff_eduction_level` AS `staffEductionLevel`,
    `staff_info`.`staff_remark` AS `staffRemark`,
    `staff_info`.`staff_state` AS `staffState`,
    `staff_info`.`user_number` AS `userNumber`,
    `staff_info`.`user_passowrd` AS `userPassowrd`
FROM
    `staff_info`



CREATE VIEW `view_role_anthority_info` AS select `role_anthority_info`.`role_anthority_id` AS `roleAnthorityId`,`role_anthority_info`.`role_id` AS `roleId`,`role_anthority_info`.`anthorty_id` AS `anthortyId` from `role_anthority_info`



CREATE  VIEW `v_market_active` AS (
    select
        `market_active`.`active_id`              AS `activeId`,
        `market_active`.`staff_id`               AS `staffId`,
        `market_active`.`active_name`            AS `activeName`,
        `market_active`.`active_state`           AS `activeState`,
        `market_active`.`active_start`           AS `activeStart`,
        `market_active`.`active_end`             AS `activeEnd`,
        `market_active`.`active_type`            AS `activeType`,
        `market_active`.`active_coste_emtimate`  AS `activeCosteEmtimate`,
        `market_active`.`active_coste`           AS `activeCoste`,
        `market_active`.`active_refect_estimate` AS `activeRefectEstimate`,
        `market_active`.`active_student`         AS `activeStudent`,
        `market_active`.`active_content`         AS `activeContent`
    from `market_active`)


CREATE VIEW v_email AS
SELECT
    email_info.email_id AS emailId,
    email_info.staff_id AS staffId,
    email_info.email_title AS emailTitle,
    email_info.email_content AS emailContent,
    email_info.email_time AS emailTime,
    email_info.email_man AS emailMan,
    email_info.email_addr AS emailAddress,
    email_info.email_state AS emailState
FROM
    email_info


CREATE VIEW messafe AS
SELECT `messafe_info`.`messafe_id` AS `messafeId`,`messafe_info`.`staff_id` AS `staffId`,`messafe_info`.`messafe_content` AS `messafeContent`,`messafe_info`.`messafe_man` AS `messafeMan`,`messafe_info`.`messafe_phone` AS `messafePhone`,`messafe_info`.`messafe_time` AS `messafeTime`,`messafe_info`.`messafe_state` AS `messafeState` FROM `messafe_info`



create view datadictionary as
SELECT
    data_dictionary.data_id AS dataId,
    data_dictionary.data_content AS dataContent,
    data_dictionary.data_type AS dataType,
    data_dictionary.data_desc AS dataDesc
FROM
    data_dictionary

create view staffinfo as
SELECT
    staff_info.staff_id AS staffId,
    staff_info.role_id AS roleId,
    staff_info.staff_name AS staffName,
    staff_info.staff_sex AS staffSex,
    staff_info.staff_age AS staffAge,
    staff_info.staff_native_place AS staffNativePlace,
    staff_info.staff_idcard AS staffIdcard,
    staff_info.staff_brithday AS staffBrithday,
    staff_info.staff_office_phone AS staffOfficePhone,
    staff_info.staff_mobile_phone AS staffMobilePhone,
    staff_info.staff_eamil AS staffEamil,
    staff_info.staff_addr AS staffAddr,
    staff_info.staff_qq AS staffQq,
    staff_info.staff_entry_time AS staffEntryTime,
    staff_info.staff_eduction_level AS staffEductionLevel,
    staff_info.staff_remark AS staffRemark,
    staff_info.staff_state AS staffState,
    staff_info.user_number AS userNumber,
    staff_info.user_passowrd AS userPassowrd
FROM
    staff_info

CREATE VIEW view_role_info(roleId,roleName,roleDesc,roleState) AS SELECT role_id,role_name,role_desc,role_state FROM role_info;
CREATE VIEW view_anthorty_info(anthortyId,anthortyPid,anthortyName,anthortyDesc,anthortyUrl) AS SELECT anthorty_id,anthorty_pid,anthorty_name,anthorty_desc,anthorty_url FROM anthorty_info;

create view evaluation_info_view as
select `evaluation_info`.`evaluation_id` AS `evaluationId`,`evaluation_info`.`student_id` AS `studentId`,`evaluation_info`.`evaluation_title` AS `evaluationTitle`,`evaluation_info`.`evaluation_content` AS `evaluationContent`,`evaluation_info`.`evaluation_course` AS `evaluationCourse`,`evaluation_info`.`evaluation_teacher` AS `evaluationTeacher`,`evaluation_info`.`evaluation_time` AS `evaluationTime` from `evaluation_info`

CREATE VIEW templateinfo AS
SELECT `templateinfo`.`template_id` AS `templateId`,`templateinfo`.`template_title` AS `templateTitle`,`templateinfo`.`template_content` AS `templateContent`,`templateinfo`.`template_type` AS `templateType` FROM `template_info` `templateinfo`

#学员信息表
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_student_info` AS select `studentinfo`.`student_id` AS `studentId`,`studentinfo`.`staff_id` AS `staffId`,`studentinfo`.`class_id` AS `classId`,`studentinfo`.`student_name` AS `studentName`,`studentinfo`.`student_sex` AS `studentSex`,`studentinfo`.`student_age` AS `studentAge`,`studentinfo`.`student_tellphone` AS `studentTellphone`,`studentinfo`.`student_email` AS `studentEmail`,`studentinfo`.`student_idcard` AS `studentIdcard`,`studentinfo`.`student_address` AS `studentAddress`,`studentinfo`.`student_birthday` AS `studentBirthday`,`studentinfo`.`student_school` AS `studentSchool`,`studentinfo`.`student_qq` AS `studentQq`,`studentinfo`.`student_parents_name` AS `studentParentsName`,`studentinfo`.`student_parents_phone` AS `studentParentsPhone`,`studentinfo`.`student_pro` AS `studentPro`,`studentinfo`.`student_pro_city` AS `studentProCity`,`studentinfo`.`student_type` AS `studentType`,`studentinfo`.`student_ispay` AS `studentIspay`,`studentinfo`.`student_sate` AS `studentSate`,`studentinfo`.`student_mark` AS `studentMark`,`studentinfo`.`student_desc` AS `studentDesc`,`studentinfo`.`student_number` AS `studentNumber`,`studentinfo`.`student_password` AS `studentPassword` from `student_info` `studentinfo`


#    //跟踪记录表
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_track_recond_info` AS select `trackrecordinfo`.`track_record_id` AS `trackRecordId`,`trackrecordinfo`.`student_id` AS `studentId`,`trackrecordinfo`.`track_record_title` AS `trackRecordTitle`,`trackrecordinfo`.`track_record_content` AS `trackRecordContent`,`trackrecordinfo`.`track_record_time` AS `trackRecordTime`,`trackrecordinfo`.`enrollment` AS `enrollment`,`trackrecordinfo`.`next_record_time` AS `nextRecordTime` from `track_record_info` `trackrecordinfo`


 #   //试听信息表
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_audition_info` AS select `auditioninfo`.`audition_id` AS `auditionId`,`auditioninfo`.`student_id` AS `studentId`,`auditioninfo`.`audition_time` AS `auditionTime`,`auditioninfo`.`audition_addr` AS `auditionAddr`,`auditioninfo`.`audition_course` AS `auditionCourse`,`auditioninfo`.`audition_desc` AS `auditionDesc` from `audition_info` `auditioninfo`


