package org.lq;

import static org.junit.Assert.assertTrue;

import com.google.gson.Gson;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;
import org.lq.system.dao.AnthortyInfoDao;
import org.lq.system.dao.impl.AnthortyInfoDaoImpl;
import org.lq.system.entity.AnthortyInfo;
import org.lq.system.entity.dto.AnthortyInfoDto;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() throws InvocationTargetException, IllegalAccessException {

        AnthortyInfoDao anthortyInfoDao = new AnthortyInfoDaoImpl();
        List<AnthortyInfo> list = anthortyInfoDao.pageList(0, anthortyInfoDao.getCount());

        System.out.println(list);

        AnthortyInfo parent = anthortyInfoDao.getById(1);

        AnthortyInfoDto infoDto = new AnthortyInfoDto();
        try {
            BeanUtils.copyProperties(infoDto,parent);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


        AnthortyInfoDto tree = getTree(list, infoDto);
        System.out.println(tree);
        Gson gson = new Gson();
        System.out.println(gson.toJson(tree));

//        System.out.println(parent);
//        System.out.println(infoDto);
//        for(AnthortyInfo info : list){
//            if ( info.getAnthortyPid() == infoDto.getAnthortyId()){
//                AnthortyInfoDto p1 = new AnthortyInfoDto();
//                BeanUtils.copyProperties(p1,info);
//                infoDto.getList().add(p1);
//            }
//        }
//
//
//        for(AnthortyInfo c : list){
//
//            for (AnthortyInfoDto dto : infoDto.getList()){
//                if(dto.getAnthortyId() == c.getAnthortyPid()){
//
//                    AnthortyInfoDto c1 = new AnthortyInfoDto();
//                    BeanUtils.copyProperties(c1,c);
//                    dto.getList().add(c1);
//                }
//            }
//        }



//        System.out.println(infoDto);

    }

    //递归循环->无限层次循环结构
    public AnthortyInfoDto getTree(List<AnthortyInfo> list,AnthortyInfoDto dto) throws InvocationTargetException, IllegalAccessException {
        for (AnthortyInfo info : list){
            if(info.getAnthortyPid() == dto.getAnthortyId()){
                AnthortyInfoDto child = new AnthortyInfoDto();
                BeanUtils.copyProperties(child,info);
                dto.getList().add(child);
                getTree(list,child);
            }
        }
        return dto;
    }


}
