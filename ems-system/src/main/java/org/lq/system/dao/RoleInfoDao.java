package org.lq.system.dao;

import org.lq.base.BaseDao;
import org.lq.system.entity.RoleInfo;

import java.util.List;

/**
 * @author 李冠良
 * @create 2020-10-13
 */
public interface RoleInfoDao extends BaseDao<RoleInfo> {


    /**
     * 根据角色姓名模糊查询数据的总行数
     * @param roleName
     * @return
     */
    int getCount(String roleName);

    /**
     * 根据角色姓名模糊查询的数据
     * @param startIndex
     * @param pageSize
     * @param roleName
     * @return
     */
    List<RoleInfo> pageByValues(int startIndex, int pageSize, String roleName);

    /**
     * 根据角色变化,修改角色状态
     * @param id
     * @param state
     * @return
     */
    int updateRoleById(int id,int state);
}
