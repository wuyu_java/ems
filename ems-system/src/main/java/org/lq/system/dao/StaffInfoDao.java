package org.lq.system.dao;

import lombok.extern.log4j.Log4j;
import org.lq.base.BaseDao;
import org.lq.system.entity.StaffInfo;

import java.util.List;

/**
 * @author 马秋阳
 * @create 2020-10-13 18:54
 */
public interface StaffInfoDao extends BaseDao<StaffInfo> {
    /**
     * 根据角色名称查询
     * @param roleId
     * @return
     */
    List<StaffInfo> getByRoleId(int roleId);


}
