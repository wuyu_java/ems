package org.lq.system.dao;

import org.lq.base.BaseDao;
import org.lq.system.entity.DataDictionary;

import java.util.List;

/**
 * @author 马秋阳
 * @create 2020-10-14 17:01
 */
public interface DataDictionaryDao extends BaseDao<DataDictionary> {

    List<DataDictionary> getDataByType(String type);
}
