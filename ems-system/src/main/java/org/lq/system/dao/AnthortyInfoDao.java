package org.lq.system.dao;

import org.lq.base.BaseDao;
import org.lq.system.entity.AnthortyInfo;

import java.util.List;

/**
 * 权限信息数据访问层接口
 * @author HXT
 * @create 2020-10-13 18:25
 */
public interface AnthortyInfoDao extends BaseDao<AnthortyInfo> {
    default int getCount(String... values){return 0;}
    default List<AnthortyInfo> pageByValues(int startIndex, int pageSize, String... value){return null;}


    /**
     * 根据权限信息名字进行模糊查询，返回数据条数
     * @param values
     * @return
     */
    int getCountByName(String values);

    /**
     * 根据名字模糊查询，然后指定开始位置和步长，返回集合列表
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    List<AnthortyInfo> pageByValues(int startIndex, int pageSize, String value);

    /**
     * 根据权限信息名字获取对应的权限编号
     * @param name
     * @return
     */
    int getAuthortyInfoIdByName(String name);

    /**
     * 根据上级编号获取同一级下的所有权限信息
     * @param pid
     * @return
     */
    List<AnthortyInfo> getAuthortyInfoByPid(int pid);


}
