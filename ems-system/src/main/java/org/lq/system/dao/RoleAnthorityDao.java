package org.lq.system.dao;

import org.lq.base.BaseDao;
import org.lq.system.entity.RoleAnthority;

import java.util.List;

/**
 * @author 付金晨
 * @create 2020 10 14 17:18
 */
public interface RoleAnthorityDao extends BaseDao<RoleAnthority> {

    /**
     * 查询当前角色下面是否存在指定的权限菜单
     * @param rid
     * @param aid
     * @return
     */
    long getRoleMenu(int rid,int aid);


    int deleteRAByRoleIdAndAnthoryId(int rid,int aid);
    int deleteRAByRoleId(int rid);
    int getRACountByRid(int rid);

    List<Integer> getAnthotyByRid(int rid);


}
