package org.lq.system.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.system.dao.AnthortyInfoDao;
import org.lq.system.entity.AnthortyInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 权限信息数据访问层接口实现
 *
 * @author HXT
 * @create 2020-10-13 18:36
 */
@Log4j
public class AnthortyInfoDaoImpl implements AnthortyInfoDao {
    /**
     * t添加
     *
     * @param anthortyInfo
     * @return
     */
    @Override
    public int save(AnthortyInfo anthortyInfo) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->save");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        int num = 0;
        try {
            num = queryRunner.update(
                    "INSERT INTO `ems`.`anthorty_info`( `anthorty_pid`, `anthorty_name`, `anthorty_desc`, `anthorty_url`) VALUES (?,?,?,?);",
                    anthortyInfo.getAnthortyPid(), anthortyInfo.getAnthortyName(), anthortyInfo.getAnthortyDesc(), anthortyInfo.getAnthortyUrl()
            );
        } catch (SQLException e) {
            log.error("添加权限信息出现异常", e);
        }
        log.info("-->执行结果：" + num);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->save,执行结束");
        return num;
    }

    /**
     * 修改
     *
     * @param anthortyInfo
     * @return
     */
    @Override
    public int update(AnthortyInfo anthortyInfo) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->update");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        int num = 0;
        try {
            num = queryRunner.update(
                    "UPDATE `ems`.`anthorty_info` SET `anthorty_pid` = ?, `anthorty_name` = ?, `anthorty_desc` = ?, `anthorty_url` = ? WHERE `anthorty_id` = ?",
                    anthortyInfo.getAnthortyPid(), anthortyInfo.getAnthortyName(), anthortyInfo.getAnthortyDesc(), anthortyInfo.getAnthortyUrl(), anthortyInfo.getAnthortyId()
            );
        } catch (SQLException e) {
            log.error("修改权限信息出现异常", e);
        }
        log.info("-->执行结果：" + num);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->update,执行结束");
        return num;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->delete");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        int num = 0;
        try {
            num = queryRunner.update(
                    "delete from anthorty_info where anthorty_id = ?",
                    id
            );
        } catch (SQLException e) {
            log.error("删除权限信息出现异常", e);
        }
        log.info("-->执行结果：" + num);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->delete,执行结束");
        return num;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public AnthortyInfo getById(int id) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->getById");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        AnthortyInfo anthortyInfo = null;
        try {
            anthortyInfo = queryRunner.query("select * from view_anthorty_info where anthortyId = ?", new BeanHandler<AnthortyInfo>(AnthortyInfo.class), id);
        } catch (SQLException e) {
            log.error("权限信息-通过ID查询出现异常", e);
        }
        log.info("-->执行结果：" + anthortyInfo);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->getById,执行结束");
        return anthortyInfo;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->getCount");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        int num = 0;
        try {
            Long count = queryRunner.query("select count(1) from view_anthorty_info", new ScalarHandler<Long>());
            num = count.intValue();
        } catch (SQLException e) {
            log.error("权限信息-查询总行数出现异常", e);
        }
        log.info("-->执行结果：" + num);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->getCount,执行结束");
        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AnthortyInfo> pageList(int startIndex, int pageSize) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->pageList");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        List<AnthortyInfo> list = null;
        try {
            list = queryRunner.query(
                    "select * from view_anthorty_info limit ?,?",
                    new BeanListHandler<AnthortyInfo>(AnthortyInfo.class),
                    startIndex, pageSize);
        } catch (SQLException e) {
            log.error("权限信息-分页查询出现异常", e);
        }
        log.info("-->执行结果：" + list);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->pageList,执行结束");
        return list;
    }


    /**
     * 根据权限信息名字进行模糊查询，返回数据条数
     *
     * @param values
     * @return
     */
    @Override
    public int getCountByName(String values) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->getCountByName");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        int num = 0;
        try {
            Long count = queryRunner.query(
                    "select count(1) from view_anthorty_info where anthortyName like ?",
                    new ScalarHandler<Long>(),
                    "%"+values+"%");
            num = count.intValue();
        } catch (SQLException e) {
            log.error("权限信息-按名字模糊查询总行数出现异常", e);
        }
        log.info("-->执行结果：" + num);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->getCountByName,执行结束");
        return num;
    }

    /**
     * 根据名字模糊查询，然后指定开始位置和步长，返回集合列表
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<AnthortyInfo> pageByValues(int startIndex, int pageSize, String value) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->pageByValues");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        List<AnthortyInfo> list = null;
        try {
            list = queryRunner.query(
                    "select * from view_anthorty_info where anthortyName like ? limit ?,?",
                    new BeanListHandler<AnthortyInfo>(AnthortyInfo.class),
                    "%"+value+"%",startIndex, pageSize);
        } catch (SQLException e) {
            log.error("权限信息-按名字模糊分页查询出现异常", e);
        }
        log.info("-->执行结果：" + list);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->pageList,执行结束");
        return list;
    }

    /**
     * 根据权限信息名字获取对应的权限编号
     *
     * @param name
     * @return
     */
    @Override
    public int getAuthortyInfoIdByName(String name) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->getAuthortyInfoIdByName");
        int id = 0;
        AnthortyInfo anthortyInfo = null;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            anthortyInfo = queryRunner.query(
                    "select * from view_anthorty_info where anthortyName = ?",
                    new BeanHandler<AnthortyInfo>(AnthortyInfo.class),
                    name);
            id = anthortyInfo.getAnthortyId();
        } catch (SQLException e) {
            log.error("权限信息-根据权限信息名字获取对应的权限编号出现异常", e);
        }
        log.info("-->执行结果：" + id);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->getAuthortyInfoIdByName,执行结束");
        return id;
    }

    /**
     * 根据上级编号获取同一级下的所有权限信息
     *
     * @param pid
     * @return
     */
    @Override
    public List<AnthortyInfo> getAuthortyInfoByPid(int pid) {
        log.info("-->开始执行数据访问层,[AnthortyInfoDaoImpl]->getAuthortyInfoByPid");
        List<AnthortyInfo> list = null;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = queryRunner.query(
                    "select * from view_anthorty_info where anthortyPid = ?",
                    new BeanListHandler<AnthortyInfo>(AnthortyInfo.class),
                    pid);
        } catch (SQLException e) {
            log.error("权限信息-根据上级编号获取同一级下的所有权限信息出现异常", e);
        }
        log.info("-->执行结果：" + list);
        log.info("-->数据访问层,[AnthortyInfoDaoImpl]->getAuthortyInfoByPid,执行结束");
        return list;
    }
}
