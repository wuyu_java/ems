package org.lq.system.dao.impl;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.system.dao.DataDictionaryDao;
import org.lq.system.entity.DataDictionary;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * @author 马秋阳
 * @create 2020-10-14 17:01
 */
public class DataDictionaryDaoImpl implements DataDictionaryDao {
    /**
     * t添加
     *
     * @param dataDictionary
     * @return
     */
    @Override
    public int save(DataDictionary dataDictionary) {
        int num = 0;
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = runner.update("insert into data_dictionary (data_content, data_type, data_desc) values (?,?,?);",
                    dataDictionary.getDataContent(),dataDictionary.getDataType(),dataDictionary.getDataDesc());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return num;
    }

    /**
     * 修改
     *
     * @param dataDictionary
     * @return
     */
    @Override
    public int update(DataDictionary dataDictionary) {
        int num = 0;
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = runner.update("update data_dictionary set data_content=?, data_type=?, data_desc=? where data_id=?",
                    dataDictionary.getDataContent(),dataDictionary.getDataType(),dataDictionary.getDataDesc(),dataDictionary.getDataId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return num;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        int num = 0;
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = runner.update("delete from data_dictionary where data_id = ?",id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return num;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public DataDictionary getById(int id) {
        DataDictionary dd = null;
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            dd = runner.query("select * from datadictionary where dataId = ?",
                    new BeanHandler<>(DataDictionary.class),
                    id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dd;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        int count = 0;
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long num = runner.query("select count(1) from datadictionary",
                    new ScalarHandler<Long>());
            count = num.intValue();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<DataDictionary> pageList(int startIndex, int pageSize) {
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        List<DataDictionary> all = null;
        try {
            all = runner.query("select * from datadictionary limit ?,?",
                    new BeanListHandler<DataDictionary>(DataDictionary.class),
                    startIndex,pageSize);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        int count = 0;
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long num = runner.query("select count(1) from datadictionary where dataContent like ?",
                    new ScalarHandler<Long>(),
                    "%"+values[0]+"%");
            count = num.intValue();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * 根据条件分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<DataDictionary> pageByValues(int startIndex, int pageSize, String... value) {
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        List<DataDictionary> all = null;
        try {
            all = runner.query("select * from datadictionary where dataContent like ? limit ?,?",
                    new BeanListHandler<DataDictionary>(DataDictionary.class),
                    "%"+value[0]+"%",startIndex,pageSize);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }

    @Override
    public List<DataDictionary> getDataByType(String type) {
        QueryRunner runner = new QueryRunner(JDBCUtil.getDataSource());
        List<DataDictionary> all = null;
        try {
            all = runner.query("select * from datadictionary where dataType = ?",
                    new BeanListHandler<DataDictionary>(DataDictionary.class),
                    type);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all;
    }
}
