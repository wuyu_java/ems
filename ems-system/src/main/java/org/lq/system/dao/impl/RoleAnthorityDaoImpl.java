package org.lq.system.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.base.BaseDao;
import org.lq.system.dao.RoleAnthorityDao;
import org.lq.system.entity.RoleAnthority;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 付金晨
 * @create 2020 10 13 22:26
 */
@Log4j
public class RoleAnthorityDaoImpl implements RoleAnthorityDao {

    /**
     * t添加
     *
     * @param role_anthority
     * @return
     */
    @Override
    public int save(RoleAnthority role_anthority) {
         int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("insert into role_anthority_info(role_id,anthorty_id) values (?,?)",
                    role_anthority.getRoleId(),
                    role_anthority.getAnthortyId());
            log.info("角色权限添加成功");
        } catch (SQLException throwables) {
            log.error("角色权限添加失败",throwables);
        }
        return num;
    }

    /**
     * 修改
     *
     * @param role_anthority
     * @return
     */
    @Override
    public int update(RoleAnthority role_anthority) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("update  role_anthority_info set role_id =?,anthorty_id=? where role_anthority_id = ?",
                    role_anthority.getRoleId(),
                    role_anthority.getAnthortyId(),
                    role_anthority.getRoleAnthorityId());
            log.info("角色权限修改成功");
        } catch (SQLException throwables) {
            log.error("角色权限修改失败",throwables);
        }
        return num;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("delete from role_anthority_info where role_anthority_id = ?",
                   id);
            log.info("角色权限删除成功");
        } catch (SQLException throwables) {
            log.error("角色权限删除失败",throwables);
        }
        return num;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public RoleAnthority getById(int id) {
        RoleAnthority roleAnthority = new RoleAnthority();
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            roleAnthority = queryRunner.query("select * from view_role_anthority_info where role_anthority_id = ?",new BeanHandler<>(RoleAnthority.class),id);
            log.info("角色查询成功");
        } catch (SQLException throwables) {
            log.error("角色查询失败",throwables);
        }
        return roleAnthority;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long count = queryRunner.query("select count(1) from view_role_anthority_info", new ScalarHandler<Long>());
            num = count.intValue();
            log.info("获得角色权限总行数成功");
        } catch (SQLException throwables) {
            log.error("获得角色权限总行数失败",throwables);
        }

        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<RoleAnthority> pageList(int startIndex, int pageSize) {
        List<RoleAnthority> list = new ArrayList<>();
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = queryRunner.query("select * from view_role_anthority_info limit ?,?", new BeanListHandler<RoleAnthority>(RoleAnthority.class), startIndex, pageSize);
           log.info("角色权限分页查询成功");
        } catch (SQLException throwables) {
            log.error("角色权限分页查询失败",throwables);
        }
        return list;
    }

    /**
     * 根据角色编号或权限编号查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long query = queryRunner.query("select count(1) from view_role_anthority_info where role_id = ? or anthorty_id =?", new ScalarHandler<Long>(), "%" + values + "%");
            num = query.intValue();
            log.info("获得角色权限总行数成功");
        } catch (SQLException throwables) {
            log.error("获得角色权限总行数失败",throwables);
        }
        return num;
    }

    /**
     * 根据角色编号或权限编号分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<RoleAnthority> pageByValues(int startIndex, int pageSize, String... value) {
        List<RoleAnthority> list  = new ArrayList<>();
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = queryRunner.query("select * from view_role_anthority_info where role_id = ? or anthorty_id =? limit ?,?",new BeanListHandler<RoleAnthority>(RoleAnthority.class),"%"+value+"%",startIndex,pageSize);
            log.info("角色权限模糊分页查询成功");
        } catch (SQLException throwables) {
            log.error("角色权限模糊分页查询失败",throwables);
        }
        return list;
    }

    /**
     * 查询当前角色下面是否存在指定的权限菜单
     *
     * @param rid
     * @param aid
     * @return
     */
    @Override
    public long getRoleMenu(int rid, int aid) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long query = queryRunner.query("select count(1) from view_role_anthority_info where roleId = ? and anthortyId =?", new ScalarHandler<Long>(),rid,aid);
            num = query.intValue();
            log.info("获得角色权限数成功");
        } catch (SQLException throwables) {
            log.error("获得角色权限数失败",throwables);
        }
        return num;
    }

    @Override
    public int deleteRAByRoleIdAndAnthoryId(int rid, int aid) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("delete from role_anthority_info where role_id = ? and anthorty_id =?",
                    rid,aid);
            log.info("角色权限删除成功");
        } catch (SQLException throwables) {
            log.error("角色权限删除失败",throwables);
        }
        return num;
    }

    @Override
    public int deleteRAByRoleId(int rid) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("delete from role_anthority_info where role_id = ? ",
                    rid);
            log.info("角色权限删除成功");
        } catch (SQLException throwables) {
            log.error("角色权限删除失败",throwables);
        }
        return num;
    }

    @Override
    public int getRACountByRid(int rid) {
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long query = queryRunner.query("select count(1) from view_role_anthority_info where roleId = ? ", new ScalarHandler<Long>(), rid);
            num = query.intValue();
            log.info("获得角色权限总行数成功");
        } catch (SQLException throwables) {
            log.error("获得角色权限总行数失败",throwables);
        }
        return num;
    }

    @Override
    public List<Integer> getAnthotyByRid(int rid) {
        try {
            return new QueryRunner(JDBCUtil.getDataSource()).query("select anthorty_id from role_anthority_info where role_id = ?",new ColumnListHandler<>(),rid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
