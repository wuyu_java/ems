package org.lq.system.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * 权限信息实体类
 * @author HXT
 * @create 2020-10-13 18:15
 */
@Data
public class AnthortyInfo {

    private int anthortyId;//权限编号
    private int anthortyPid;//上级编号
    @SerializedName("title") //通过注解修改生成JSON的别名
    private String anthortyName;//权限名称
    private String anthortyDesc;//权限描述
    private String anthortyUrl;//权限url



}
