package org.lq.system.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author 付金晨
 * @create 2020 10 13 22:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleAnthority {

    private int roleAnthorityId;//角色权限编号
    private int roleId;//角色编号
    private  int anthortyId;//权限编号
}
