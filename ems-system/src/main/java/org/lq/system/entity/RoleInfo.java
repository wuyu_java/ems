package org.lq.system.entity;

import lombok.Data;

/**
 * @author 李冠良
 * @create 2020-10-13
 */
@Data
public class RoleInfo {
    private int roleId;  //角色编号
    private String roleName;  //角色名称
    private String roleDesc;  //角色名称
    private String roleState;  //状态
}
