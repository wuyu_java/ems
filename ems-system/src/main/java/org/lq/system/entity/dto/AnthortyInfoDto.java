package org.lq.system.entity.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.lq.system.entity.AnthortyInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 无语
 * @create 2020-10-20 10:03
 */
@Data
public class AnthortyInfoDto extends AnthortyInfo {
    @SerializedName("children")
    private List<AnthortyInfoDto> list=new ArrayList<>();
    private boolean spread = false;


    @Override
    public String toString() {

        return super.toString()+"AnthortyInfoDto{" +
                "list=" + list +
                '}';
    }
}
