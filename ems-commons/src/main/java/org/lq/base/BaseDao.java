package org.lq.base;

import java.util.List;

/**
 * @author 无语
 * @create 2020-10-13 16:20
 */
public interface BaseDao<T> {

    /**
     * t添加
     * @param t
     * @return
     */
    int save(T t);

    /**
     * 修改
     * @param t
     * @return
     */
    int update(T t);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    T getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<T> pageList(int startIndex,int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String...values);

    /**
     * 根据条件分页查询
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    List<T> pageByValues(int startIndex,int pageSize,String...value);



}
