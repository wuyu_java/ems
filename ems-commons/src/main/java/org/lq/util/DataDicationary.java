package org.lq.util;

import javax.xml.crypto.Data;

/**
 *
 * 数据字典类型
 * @author 无语
 * @create 2020-10-19 9:25
 */
public enum DataDicationary {


    ACTIVE_STATE("active_state","营销活动状态"),
    ACTIVE_TYPE("active_type","营销活动类型"),
    ACTIVE_REFECT_ESTIMATE("active_refect_estimate","预期反应"),
    STUDENT_SATE("student_sate","学生意向状态"),
    PAYMENT_METHOD("payment_method","缴费方式"),
    EDUCATIONAL_LEVEL("educational_level","教育水平");

    private String value;
    private String title;

     DataDicationary(String value,String title){
        this.value = value;
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }
}
