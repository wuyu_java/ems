package org.lq.util;

import java.util.ResourceBundle;

/**
 * @author 无语
 * @create 2020-10-06 18:32
 */
public class PropertiesUtils {


    private  static ResourceBundle resourceBundle;
    static {
        //读取src下面的content.properties配置文件
        resourceBundle = ResourceBundle.getBundle("content");
    }

    public static String getKey(String key){
        return resourceBundle.getString(key);
    }

}
