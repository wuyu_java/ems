package org.lq.util;

import lombok.Data;
import lombok.extern.log4j.Log4j;

import java.util.List;

/**
 * @author 无语
 * @create 2020-10-03 18:15
 */
@Data
@Log4j
public class PageBean<T> {

    private int totalPage;//总页数
    private int totalRows;//总行数
    private int currentPage;//当前页
    private int pageSize;//每页显示的行数
    private List<T> data;//分页数据

    private boolean firstPage;//是否是第一页
    private boolean lastPage;//是否为最后一页

    public boolean isFirstPage() {
        return currentPage == 1;
    }

    public boolean isLastPage() {
        return currentPage == getTotalPage();
    }


    public int getTotalPage() {
        this.totalPage = countTotalPage(getPageSize(),getTotalRows());
        log.info("总页数："+totalPage);
        return totalPage;
    }

    /**
     * 计算总页数的静态方法
     * @param pageSize 每页显示的条数
     * @param totalRows 同行数
     * @return
     */
    public static int countTotalPage(final int pageSize,final int totalRows){
        log.info("每页显示的行数:"+pageSize+"  总行数："+totalRows);
        return totalRows % pageSize ==0 ? totalRows/pageSize : totalRows/pageSize+1;
    }

    /**
     * 计算当前页数的开始位置
     * @param pageSize
     * @param currentPage
     * @return
     */
    public static int countOffset(final int pageSize,final int currentPage){
        return (currentPage-1) * pageSize;
    }




}
