package org.lq.util;

/**
 *
 * 通用常量
 * @author 无语
 * @create 2020-10-06 18:37
 */
public interface CommonConstant {

    String FTP_HOST = "ftp.host";
    String FTP_PORT = "ftp.port";
    String FTP_USERNAME = "ftp.username";
    String FTP_PASSWORD = "ftp.password";

}
