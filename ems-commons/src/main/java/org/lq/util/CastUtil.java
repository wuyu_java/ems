package org.lq.util;

import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * 操作工具类
 *
 * @author 无语
 * @create 2020-10-13 16:25
 */
@Log4j
public class CastUtil {


    /**
     * 年月入日
     */
    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
    public static final String DATE_PATTERN = "yyyyMMdd";
    /**
     * 	带时分秒的
     */
    public static final String TIME_DATE_PATTERN="yyyy-MM-dd hh:mm:ss";
    public static final String TIME_DATE_PATTERN2="yyyy-MM-dd-hh-mm-ss";


    /**
     * 	时间格式化器
     * @param date
     * @return
     */
    public static String format(Date date) {
        return date == null ? "" : format(date, DEFAULT_DATE_PATTERN);
    }


    /**
     * 	时间格式化器
     * @param date
     * @param pattern
     * @return
     */
    public static String format(Date date,String pattern) {
        //		new Date()//当前时间
        //		System.currentTimeMillis() 时间错

        return date == null ? "" : new SimpleDateFormat(pattern).format(date);
    }

    /**
     * 	将字符串转换成java.util时间
     * @param strDate
     * @return yyyy-MM-dd
     * @throws ParseException
     */
    public static java.util.Date parse(String strDate) throws ParseException{
        return parse(strDate, DEFAULT_DATE_PATTERN);
    }

    /**
     * 将字符串转换成时间类型
     * @param strDate
     * @param pattern
     * @return
     * @throws ParseException
     */
    public static java.util.Date parse(String strDate,String pattern) throws ParseException {
        return strDate == null && "".equals(strDate) ? null : new SimpleDateFormat(pattern).parse(strDate);
    }

    /**
     * 将字符串转换成时间类型
     * @param strDate
     * @param pattern
     * @return
     * @throws ParseException
     */
    public static java.sql.Date parse2(String strDate,String pattern) throws ParseException {
        return strDate == null && "".equals(strDate) ? null : new java.sql.Date(new SimpleDateFormat(pattern).parse(strDate).getTime());
    }
    /**
     * 	将java.util.date转换成java.sql.date
     * @param date
     * @return
     */
    public static java.sql.Date utilDateToSqlDate(java.util.Date date){
        if(date == null) {
            date = new Date();
        }
        //		java.sql.Date.parse(s)
        return new java.sql.Date(date.getTime());
    }






    public static String castString(Object obj){
        return castString(obj,"");
    }

    public static String castString(Object obj,String defaultValue){
        return obj!=null ? String.valueOf(obj) : defaultValue;
    }

    public static double castDouble(Object obj){
        return castDouble(obj,0);
    }

    public static double castDouble(Object obj,double defaultValue){
     double doubleValue = defaultValue;
     if (obj != null){
         String strValue = castString(obj);
         if (StringUtils.isNotEmpty(strValue)){
             try {
                 doubleValue = Double.parseDouble(strValue);
             } catch (NumberFormatException e) {
                 doubleValue = defaultValue;
                 log.error("转换异常",e);
             }
         }
     }
     return doubleValue;
    }

    public static int castInt(Object obj){
        return castInt(obj,0);
    }

    public static int castInt(Object obj,int defaultValue){
        int intValue = defaultValue;
        if (obj!=null){
            String strValue = castString(obj);
            if (StringUtils.isNotEmpty(strValue)){
                try {
                    intValue = Integer.parseInt(strValue);
                } catch (NumberFormatException e) {
                    intValue = defaultValue;
                    log.error("int转换异常",e);
                }
            }
        }



        return intValue;
    }



}
