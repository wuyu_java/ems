package org.lq.ems.classinfo.servlet;

import com.mysql.fabric.ShardIndex;
import org.apache.commons.beanutils.BeanUtils;
import org.lq.classinfo.entity.SyllabusInfo;
import org.lq.ems.classinfo.service.SyllabusInfoService;
import org.lq.ems.classinfo.service.impl.SyllabusInfoServiceImpl;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @author 刘明昕
 * @created 2020-10-14 17:57
 */
@WebServlet("/SyllabusInfoServlet")
public class SyllabusInfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SyllabusInfoService syllabusInfoService = new SyllabusInfoServiceImpl();
        int pageSize = 10;
        int pageIndex = 1;
        Map<String, String[]> map = req.getParameterMap();
        String m = map.get("m")[0];
        SyllabusInfo syllabusInfo = new SyllabusInfo();
        switch (m) {
            case "all":
                PageBean<SyllabusInfo> bean = syllabusInfoService.pageList(pageIndex, pageSize);
                break;
            case "add":
                try {
                    BeanUtils.populate(syllabusInfo,map);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                if (syllabusInfoService.save(syllabusInfo) > 0) {
                    resp.sendRedirect("syllabusinfo_list.html");
                }
                break;
            case "update":
                try {
                    BeanUtils.populate(syllabusInfo,map);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                if (syllabusInfoService.update(syllabusInfo) > 0) {
                    resp.sendRedirect("syllabusinfo_list.html");
                }
                break;
            case "delete":
                String id = map.get("id")[0];
                syllabusInfoService.delete(Integer.parseInt(id));
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
