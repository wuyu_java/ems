package org.lq.ems.classinfo.servlet;
import org.apache.commons.beanutils.BeanUtils;
import org.lq.classinfo.entity.ClassroomInfo;
import org.lq.ems.classinfo.service.ClassroominfoService;
import org.lq.ems.classinfo.service.impl.ClassinforoomServiceImpl;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
/**
 * 教室类
 * @author 邹倩倩
 * @create 2020 10 14 2020-10-14
 */
@WebServlet("/ClassroomInfoServlet")
public class ClassroomInfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;");

        ClassroominfoService classroominfoService = new ClassinforoomServiceImpl();

        String m = req.getParameter("m");
        if(m==null){
            m="all";
        }
        //获取表单数据
        Map<String, String[]> parameterMap = req.getParameterMap();
        String value =  req.getParameter("value");
        value = value==null?"":value;
        String currentPage =  req.getParameter("currentPage");
        int index = currentPage==null?1:Integer.parseInt(currentPage);
        int pageSize = 6;
        switch (m){
            //显示教室信息
            case "all":
                int totalRow = classroominfoService.getCount();
                PageBean pageBean = new PageBean();
                pageBean.setTotalRows(totalRow);
                pageBean = classroominfoService.pageByValues(index,pageSize,value);

                req.getSession().setAttribute("page",pageBean);
                resp.sendRedirect("#");
                break;
            //添加教室
            case "add":
                ClassroomInfo classroomInfo_add = new ClassroomInfo();
                try {
                    BeanUtils.populate(classroomInfo_add,parameterMap);
                    classroominfoService.save(classroomInfo_add);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                resp.sendRedirect("#");
                break;
            //修改之前的数据回显
            case "preupdate":
                ClassroomInfo classroomInfo_preupdate = new ClassroomInfo();
                try {
                    BeanUtils.populate(classroomInfo_preupdate,parameterMap);
                    req.setAttribute("classroomInfo",classroomInfo_preupdate);
                    resp.sendRedirect("#");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                resp.sendRedirect("#");
                break;
            //修改教室信息
            case "update":
                ClassroomInfo classroomInfo_update = new ClassroomInfo();
                try {
                    BeanUtils.populate(classroomInfo_update,parameterMap);
                    System.out.println(classroominfoService.update(classroomInfo_update));

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
                resp.sendRedirect("#");
                break;
            //删除教室信息
            case "delete":
                String id = req.getParameter("classroomId");
                try {
                        System.out.println(classroominfoService.delete(Integer.parseInt(id)));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                }
                resp.sendRedirect("#");
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req,resp);
    }

}
