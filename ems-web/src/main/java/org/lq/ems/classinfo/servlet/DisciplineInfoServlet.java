package org.lq.ems.classinfo.servlet;

import org.lq.classinfo.entity.DisciplineInfo;
import org.lq.ems.classinfo.service.DisciplineInfoService;
import org.lq.ems.classinfo.service.impl.DisciplineInfoServiceImpl;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author Sky
 * @create 2020-10-14 19:15
 */
@WebServlet("/DisciplineInfoServlet.do")
public class DisciplineInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DisciplineInfoService disciplineInfoService = new DisciplineInfoServiceImpl();
        HttpSession session = request.getSession();
        int pageIndex = 1;
        int pageSize = 2;

        String search = request.getParameter("search");
        if (search == null) {
            // 从session中获取查询条件
            search = (String) session.getAttribute("search");
            // 如果都是null,说明表单提交了查询条件,将search修改成空字符串""
            search = search == null ? "" : search;
        } else {
            // 如果不等于null,说明表单提交了查询条件,将查询条件存储到session中,为了提供给下一次,下一页,上一页使用
            session.setAttribute("search", search);
        }

        String id = request.getParameter("disciplineId");
        String name = request.getParameter("disciplineName");
        String disciplineTuition = request.getParameter("disciplineTuition");
        String disciplineBring = request.getParameter("disciplineBring");
        String disciplineDesc = request.getParameter("disciplineDesc");


        DisciplineInfo info = new DisciplineInfo();




        String method = request.getParameter("method");
        switch (method){
            case "page":
                PageBean<DisciplineInfo> pageBean = disciplineInfoService.pageDisciplineInfoByValues(pageIndex, pageSize, search);
                request.setAttribute("page", pageBean);
                request.getRequestDispatcher("index.jsp").forward(request, response);
                break;
            case "add":
                info.setDisciplineName(name);
                info.setDisciplineTuition(Integer.parseInt(disciplineTuition));
                info.setDisciplineBring(Integer.parseInt(disciplineBring));
                info.setDisciplineDesc(disciplineDesc);
                boolean flag = disciplineInfoService.saveDisciplineInfo(info);
                System.out.println(flag);
                break;
            case "update":
                info.setDisciplineId(Integer.parseInt(id));
                info.setDisciplineName(name);
                info.setDisciplineTuition(Integer.parseInt(disciplineTuition));
                info.setDisciplineBring(Integer.parseInt(disciplineBring));
                info.setDisciplineDesc(disciplineDesc);
                boolean updateFlag = disciplineInfoService.updateDisciplineInfo(info);
                System.out.println(updateFlag);
                break;
            case "byid":
                DisciplineInfo disciplineInfo = disciplineInfoService.getDisciplineInfoById(Integer.parseInt(id));
                request.setAttribute("disciplineInfo",disciplineInfo);
                break;
            case "delete":
                boolean b = disciplineInfoService.deleteDisciplineInfo(Integer.parseInt(id));
                System.out.println(b);
                break;
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
