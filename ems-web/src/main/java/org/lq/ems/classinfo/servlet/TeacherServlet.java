package org.lq.ems.classinfo.servlet;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j;
import org.lq.classinfo.entity.TeacherInfo;
import org.lq.ems.classinfo.service.TeacherInfoService;
import org.lq.ems.classinfo.service.impl.TeacherInfoServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 张三
 * @create 2020-10-14 10:38
 */
@Log4j
@WebServlet("/TeacherServlet")
public class TeacherServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        String m = req.getParameter("m");
        switch (m){
            case "all":
                all(req,resp);
                break;
            case "byId":
                byId(req,resp);
                break;
        }
    }

    public void byId(HttpServletRequest req, HttpServletResponse resp) {


    }

    public void all(HttpServletRequest req, HttpServletResponse resp){
        try {
            PrintWriter out = resp.getWriter();
            String index = req.getParameter("page");
            String size = req.getParameter("limit");
            int pageIndex = Integer.parseInt(index);
            int pageSize = Integer.parseInt(size);
            TeacherInfoService teacherInfoService = new TeacherInfoServiceImpl();

            String name = req.getParameter("name");
            String sex = req.getParameter("sex");
            List<TeacherInfo> teacherInfos = null;
            if(name != null){
                teacherInfos = teacherInfoService.pageByName(pageIndex,pageSize,name);
                teacherInfos.forEach((e) -> System.out.println(e.toString()));
            }else if(sex != null){
                teacherInfos = teacherInfoService.pageBySex(pageIndex,pageSize,sex);
                teacherInfos.forEach((e) -> System.out.println(e.toString()));
            }else {
                teacherInfos = teacherInfoService.pageList(pageIndex, pageSize);
                teacherInfos.forEach((e) -> System.out.println(e.toString()));
            }
            Gson gson = new Gson();
            Map<String, Object> map = new HashMap<>();
            map.put("code", 0);
            map.put("msg", "提示消息");
            map.put("count", teacherInfos.size());
            map.put("data", teacherInfos);
            out.print(gson.toJson(map));

        } catch (IOException e) {
            log.error("不能得到输出流->"+e);
        }
    }

}
