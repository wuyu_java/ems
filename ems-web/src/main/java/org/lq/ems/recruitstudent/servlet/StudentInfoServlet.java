package org.lq.ems.recruitstudent.servlet;

import org.lq.ems.recruitstudent.service.StudentInfoService;
import org.lq.ems.recruitstudent.service.impl.StudentInfoServiceImpl;
import org.lq.recruitstudent.entity.StudentInfo;
import org.apache.commons.beanutils.BeanUtils;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fhx
 * @create 2020-10-14-8:44
 */
@WebServlet("/StudentInfoServlet")
public class StudentInfoServlet extends HttpServlet {
    StudentInfoService studentInfoService = new StudentInfoServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response){
        String m = request.getParameter("m");
        Map<String,String[]> parm = new HashMap<>();
        int pageSize = 10;
        int pageIndex = 1;



        StudentInfo student = new StudentInfo();
        try {
            BeanUtils.populate(student,parm);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        switch (m){
            case "all":
                all(pageIndex,pageSize);
                break;
            case "add":
                add(request,response);
                break;
            case "delete":
                delete(request,response);
                break;
            case "update":
                update(request,response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);
    }

    public void add(HttpServletRequest request, HttpServletResponse response){
        Map<String,String[]> parm = new HashMap<>();
        StudentInfo studentInfo = new StudentInfo();
        try {
            BeanUtils.populate(studentInfo,parm);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if (studentInfoService.save(studentInfo)>0){
            System.out.println("添加成功");
        }
        else {
            System.out.println("添加失败");
        }
    }

    public void update(HttpServletRequest request, HttpServletResponse response){
        Map<String,String[]> parm = new HashMap<>();
        StudentInfo studentInfo = new StudentInfo();
        try {
            BeanUtils.populate(studentInfo,parm);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if (studentInfoService.update(studentInfo)>0){
            System.out.println("修改成功");
        }
        else {
            System.out.println("修改失败");
        }
    }
    public void delete(HttpServletRequest request, HttpServletResponse response){
        String  studentId = request.getParameter("studentId");
        if (studentInfoService.delete(Integer.parseInt(studentId))>0){
            System.out.println("删除成功");
        }
        else {
            System.out.println("删除失败");
        }
    }
    public void all(int PageSize,int pageIndex){
        PageBean<StudentInfo> list = new PageBean<>();
        list = studentInfoService.pageList(pageIndex,PageSize);
    }
}
