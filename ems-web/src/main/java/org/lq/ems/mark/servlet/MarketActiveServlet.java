package org.lq.ems.mark.servlet;

import com.google.gson.Gson;
import org.lq.ems.mark.service.MarketActiveService;
import org.lq.ems.mark.service.impl.MarketActiveServiceImpl;
import org.lq.mark.entity.MarketActive;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Aaron
 * @create 2020-10-14 8:52
 */
@WebServlet("/MarketActiveServlet.do")
public class MarketActiveServlet extends HttpServlet {
    MarketActiveService mas = new MarketActiveServiceImpl();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int pageIndex = 1;
        int pagesize = 2;
        String index_str = req.getParameter("index");
        if (index_str!=null){
            pageIndex = Integer.parseInt(index_str);
        }

        String active_id_str = req.getParameter("activeId");
        String staff_id_str = req.getParameter("staffId");
        int active_id = 0;
        int staff_id = 0;
        if (active_id_str!=null){
            active_id = CastUtil.castInt(active_id_str);
        }
        if (staff_id_str!=null){
            staff_id = CastUtil.castInt(staff_id_str);
        }

        String name = req.getParameter("name");
        if (name == null){
            name = (String) req.getSession().getAttribute("query");
            name = name == null ? "":name;
        }
        req.setAttribute("query",name);

        String state_str = req.getParameter("state");
        int state=0;
        if (state_str!=null){
            state = CastUtil.castInt(state_str);
        }
        String start_str = req.getParameter("start");
        String end_str = req.getParameter("end");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Timestamp start = null;
        Timestamp end = null;
        try {
            Date st = sdf.parse(start_str);
            Date ed = sdf.parse(end_str);
            start = new Timestamp(st.getTime());
            end = new Timestamp(ed.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        String type_str = req.getParameter("type");
        int type = 0;
        if (type_str!=null){
            type = CastUtil.castInt(type_str);
        }

        String costeEmtimate_str = req.getParameter("costeEmtimate");
        double costeEmtimate = 0;
        if (costeEmtimate_str!=null){
            costeEmtimate = CastUtil.castDouble(costeEmtimate_str);
        }

        String coste_str = req.getParameter("coste");
        double coste= 0;
        if (coste_str!=null){
            coste = CastUtil.castDouble(coste_str);
        }

        String refectEstimate_str = req.getParameter("refectEstimate");
        int refectEstimate = 0;
        if (refectEstimate_str!=null){
            refectEstimate = CastUtil.castInt(refectEstimate_str);
        }

        String student = req.getParameter("student");
        String content = req.getParameter("content");

        MarketActive m = new MarketActive();
        m.setActiveId(active_id);
        m.setStaffId(staff_id);
        m.setActiveName(name);
        m.setActiveState(state);
        m.setActiveStart(start);
        m.setActiveEnd(end);
        m.setActiveType(type);
        m.setActiveCosteEmtimate(costeEmtimate);
        m.setActiveCoste(coste);
        m.setActiveRefectEstimate(refectEstimate);
        m.setActiveStudent(student);
        m.setActiveContent(content);

        String method = req.getParameter("m");
        switch (method){
            case "add":
                if (mas.save(m)){
                    req.setAttribute("marketActives",mas.pageByValues(pageIndex,pagesize,name));
                    req.getRequestDispatcher("marketActive.jsp").forward(req,resp);
                }else{
                    resp.sendRedirect("addMarketActive.jsp");
                }
                break;
            case "delete":
                mas.delete(active_id);
                req.setAttribute("marketActives",mas.pageByValues(pageIndex,pagesize,name));
                req.getRequestDispatcher("marketActive.jsp").forward(req,resp);
                break;
            case "update":
                if (mas.update(m)){
                    req.setAttribute("marketActives",mas.pageByValues(pageIndex,pagesize,name));
                    req.getRequestDispatcher("marketActive.jsp").forward(req,resp);
                }else{
                    resp.sendRedirect("updateMarketActive.jsp");
                }
                break;
            case "all":
                req.setAttribute("marketActives",mas.pageByValues(pageIndex,pagesize,name));
                req.getRequestDispatcher("marketActive.jsp").forward(req,resp);
                break;
            case "byid":
                req.setAttribute("marketActive",mas.getById(active_id));
                req.getRequestDispatcher("updateMarketActive.jsp").forward(req,resp);
                break;
        }

        PrintWriter out = resp.getWriter();
        String met = req.getParameter("met");
        switch (met){
            case "add":
                out.print(mas.save(m));
                break;
            case "delete":
                out.print(mas.delete(m.getActiveId()));
                break;
            case "update":
                out.print(mas.update(m));
                break;
            case "all":
                PageBean<MarketActive> pageBean=mas.pageByValues(pageIndex,pagesize,name);
                Gson gson = new Gson();
                out.print(gson.toJson(pageBean));
                break;
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
}
