package org.lq.ems.mark.servlet;

import org.lq.ems.mark.service.EmailService;
import org.lq.ems.mark.service.impl.EmailServiceImpl;
import org.lq.mark.entity.Email;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author ming
 * @create 2020-10-18 17:17
 */
@WebServlet("/EmailServlet.do")
public class EmailServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> parameterMap = req.getParameterMap();
        EmailService emailService = new EmailServiceImpl();
        PrintWriter out = resp.getWriter();
        String emailId = req.getParameter("emailId");
        String staffId = req.getParameter("staffId");
        String emailTitle = req.getParameter("emailTitle");
        String emailContent = req.getParameter("emailContent");
        String emailMan = req.getParameter("emailMan");
        String emailState = req.getParameter("emailState");
        String method = req.getParameter("method");
        String emailAddress = req.getParameter("emailAddress");
        Email email = new Email();
        email.setStaffId(Integer.parseInt(staffId));
        email.setEmailTitle(emailTitle);
        email.setEmailContent(emailContent);
        email.setEmailMan(emailMan);
        email.setEmailAddress(emailAddress);
        email.setEmailState(emailState);
        
        switch (method) {
            case "add":
                break;
            case "delete":
                break;
            case "update":
                break;
            case "findById":
                break;
            case "all":
                PageBean<Email> pageBean = emailService.pageList(1, 10);
                break;

        }
     }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
}
