package org.lq.ems.system.servlet;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.AnthortyInfoService;
import org.lq.ems.system.service.impl.AnthortyInfoServiceImpl;
import org.lq.system.entity.AnthortyInfo;
import org.lq.util.CastUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author HXT
 * @create 2020-10-14 8:36
 */
@Log4j
@WebServlet("/view/system/anthorty/AnthortyInfoServlet.do")
public class AnthortyInfoServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        AnthortyInfoService anthortyInfoService = new AnthortyInfoServiceImpl();
        PrintWriter out = resp.getWriter();
        //接收前台数据
        String str_anthortyId = req.getParameter("anthortyId");
        int anthortyId = CastUtil.castInt(str_anthortyId);
        String str_anthortyPid = req.getParameter("anthortyPid");
        int anthortyPid = CastUtil.castInt(str_anthortyPid);
        String anthortyName = req.getParameter("anthortyName");
        String anthortyDesc = req.getParameter("anthortyDesc");
        String anthortyUrl = req.getParameter("anthortyUrl");
        String search = req.getParameter("search");//传入搜索所需要的值

        AnthortyInfo a =new AnthortyInfo();
        a.setAnthortyId(anthortyId);
        a.setAnthortyPid(anthortyPid);
        a.setAnthortyName(anthortyName);
        a.setAnthortyDesc(anthortyDesc);
        a.setAnthortyUrl(anthortyUrl);


        //前台传入分页所需要的数据，页数（第几页），步长（每页显示的数量）
        String str_page = req.getParameter("page");
        String str_limit = req.getParameter("limit");
        int pageIndex = CastUtil.castInt(str_page);
        int limit = CastUtil.castInt(str_limit);
        //前台传入需要执行的业务类型
        String m = req.getParameter("m");


        switch (m){
            case "all":
                //显示数据信息，调用业务层查询，根据页数和步长进行查询出集合,search为搜索值，为空时默认查询所有
                List<AnthortyInfo> list = anthortyInfoService.pageByValues(pageIndex,limit,search);
                Gson gson = new Gson();
                out.print(gson.toJson(list));
                break;
            case "save":
                out.print(anthortyInfoService.save(a)>0);
                break;
            case "update":
                out.print(anthortyInfoService.update(a)>0);
                break;
            case "delete":
                boolean bool = anthortyInfoService.delete(anthortyId)>0;
                out.print(bool);
                break;
            case "tree":
                out.print(anthortyInfoService.getTreeJSON());
                break;
            case "byid":
                out.print(new Gson().toJson(anthortyInfoService.getById(anthortyId)));
                break;
            case "parents":
                out.print(new Gson().toJson(anthortyInfoService.getParent()));
                break;


        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
}
