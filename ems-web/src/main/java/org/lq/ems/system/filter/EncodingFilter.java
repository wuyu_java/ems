package org.lq.ems.system.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 马秋阳
 * @create 2020-10-13 21:52
 */
@WebFilter("*.a")
public class EncodingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("utf-8");
        HttpServletResponse res = (HttpServletResponse) response;
//        res.setContentType("text/html;charset=utf-8");
//        res.setCharacterEncoding("utf-8");
        res.setHeader("Access-Control-Allow-Origin","*");
        res.setHeader("Access-Control-Allow-Methods","*");
        res.setHeader("Access-Control-Allow-Headers","*");
        res.setHeader("Access-Control-Allow-Credentials","true");
        res.setHeader("Access-Control-Max-Age","3600");
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {}
}
