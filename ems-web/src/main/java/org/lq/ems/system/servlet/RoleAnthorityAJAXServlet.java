package org.lq.ems.system.servlet;

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.lq.ems.system.service.AnthortyInfoService;
import org.lq.ems.system.service.RoleAnthorityService;
import org.lq.ems.system.service.impl.AnthortyInfoServiceImpl;
import org.lq.ems.system.service.impl.RoleAnthorityServiceImpl;
import org.lq.util.CastUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 无语
 * @create 2020-10-21 16:14
 */
@WebServlet(value = "/view/system/anthotychange/RoleAnthorityAJAXServlet.do")
public class RoleAnthorityAJAXServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");

        String data = request.getParameter("data");
//        String str_aid = request.getParameter("aid");
//        String str_pid = request.getParameter("pid");
        String str_rid = request.getParameter("rid");
//        int aid = CastUtil.castInt(str_aid);
//        int pid = CastUtil.castInt(str_pid);
        int rid = CastUtil.castInt(str_rid);
        RoleAnthorityService roleAnthorityService = new RoleAnthorityServiceImpl();
        String method = request.getParameter("method");
        if (StringUtils.equals(method,"grant")){
//            roleAnthorityService.grantAuthorization(aid,pid,rid);
            roleAnthorityService.grantAuthorization(data,rid);
        }else if(StringUtils.equals(method,"revocation")){
//            roleAnthorityService.revocationAuthorization(aid,pid,rid);
            roleAnthorityService.revocationAuthorization(data,rid);
        }else{
            Map<String,Object> map = new HashMap<>();
            AnthortyInfoService anthortyInfoService = new AnthortyInfoServiceImpl();
            map.put("tree",anthortyInfoService.getTree());
            map.put("echo",roleAnthorityService.getAnthotyByRid(rid));
            response.getWriter().print(new Gson().toJson(map));
        }

    }
}
