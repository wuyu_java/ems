package org.lq.ems.system.servlet;

import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.RoleAnthorityService;
import org.lq.ems.system.service.impl.RoleAnthorityServiceImpl;
import org.lq.system.entity.RoleAnthority;


import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author 付金晨
 * @create 2020 10 14 8:29
 */
@Log4j
@WebServlet("/RoleAnthorityInfoServlet.do")
public class RoleAnthorityInfoServlet extends HttpServlet {
    private RoleAnthorityService service = (RoleAnthorityService) new RoleAnthorityServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
           req.setCharacterEncoding("utf-8");
           resp.setContentType("text/html;charset=utf-8");
           HttpSession session = req.getSession();

           String m = req.getParameter("m");

          String id_str= req.getParameter("roleAnthorityId");
          RoleAnthority roleAnthority=null;
        String returnURL = "";
           switch (m){
               case "all":
              findAll(req,resp);
                   break;
               case "add":
                 roleAnthority = returnRoleAnthority(req,resp);
                 if (service.save(roleAnthority)<0){
                     returnURL = "";
                 }else {
                     findAll(req,resp);
                 }
                   break;
               case "update":
                   roleAnthority = returnRoleAnthority(req,resp);
                   roleAnthority.setRoleAnthorityId(Integer.parseInt(id_str));
                   //修改失败
                   if (service.update(roleAnthority)<0){
                       returnURL = "";
                   }else {
                       findAll(req,resp);
                   }
                 break;
               case "delete":
                    service.delete(Integer.parseInt(id_str));
                    findAll(req,resp);
                   break;
               case  "getId":
                    roleAnthority = service.getById(Integer.parseInt(id_str));
                    req.setAttribute("roleAnthority",roleAnthority);
                    returnURL = "";//获取到对象后返回修改页面
                   break;
           }
           req.getRequestDispatcher(returnURL).forward(req,resp);
    }

    /**
     * 分页查看
     * @param request
     * @param response
     */
    protected void findAll(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
        int pageIndex = 1;//默认开始页数
        int pageSize = 10;//每页显示的数据
        String index_str = request.getParameter("index");
        if (index_str != null){
            pageIndex = Integer.parseInt(index_str);
        }
        String search = request.getParameter("search");
        //如果接收的值为空
        if (search == null) {
            //先从session中取值
            search = (String) request.getSession().getAttribute("search");
            //若仍然为空,则赋值为"",查询全部
            search = search == null ? "" : search;
        }
        request.getSession().setAttribute("search", search);   //存放到session中

       PageBean<RoleAnthority> pageBean = (PageBean<RoleAnthority>) service.pageList(pageIndex,pageSize);
       request.setAttribute("pageBean",pageBean);

    }

    protected RoleAnthority returnRoleAnthority(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
        RoleAnthority roleAnthority = new RoleAnthority();
        String roleId = request.getParameter("roleId");
        String anthorityId = request.getParameter("anthorityId");

        roleAnthority.setRoleId(Integer.parseInt(roleId));
        roleAnthority.setAnthortyId(Integer.parseInt(anthorityId));
        return  roleAnthority;

    }

}
