package org.lq.ems.system.servlet;

import com.google.gson.Gson;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.lq.ems.system.service.StaffInfoService;
import org.lq.ems.system.service.impl.StaffInfoServiceImpl;
import org.lq.system.entity.StaffInfo;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 马秋阳
 * @create 2020-10-14 16:49
 */
@WebServlet("/view/system/staffinfo/AjaxStaffInfoServlet.do")
public class AjaxStaffInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ConvertUtils.register(new Converter() {
            @Override
            public Object convert(Class type, Object value) {
                System.out.println(type);
                System.out.println(value);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Timestamp st = null;
                try {
                    Date parse = sdf.parse(value.toString());
                    st = new Timestamp(parse.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return st;
            }
        }, Timestamp.class);
        StaffInfo sfi = new StaffInfo();
        Map<String, String[]> parameterMap = request.getParameterMap();
        try {
            BeanUtils.populate(sfi,parameterMap);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        String m = request.getParameter("m");
        StaffInfoService ss = new StaffInfoServiceImpl();

        //Ajax分页需要的参数接收和转化
        String page_str = request.getParameter("page");
        String limit_str = request.getParameter("limit");
        String values = request.getParameter("values");
        int page = CastUtil.castInt(page_str);
        int limit = CastUtil.castInt(limit_str);

        Gson gson = new Gson();
        PrintWriter out = response.getWriter();

        switch (m){
            case "all":
                PageBean<StaffInfo> pageBean = ss.pageByValues(page, limit, values);
                Map<String,Object> map =  new HashMap<>();
                map.put("code",0);
                map.put("msg","提示信息");
                map.put("count",pageBean.getData().size());
                map.put("data",pageBean.getData());
                out.print(gson.toJson(map));
                break;
            case "add":
                out.print(ss.save(sfi)>0);
                break;
            case "update":
                out.print(ss.update(sfi)>0);
                break;
            case "delete":
                out.print(ss.delete(sfi.getStaffId())>0);
                break;
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);
    }
}
