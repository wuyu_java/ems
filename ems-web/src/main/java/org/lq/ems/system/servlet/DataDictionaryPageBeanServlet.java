package org.lq.ems.system.servlet;

import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.DataDictionaryService;
import org.lq.ems.system.service.impl.DataDictionaryServiceImpl;
import org.lq.system.entity.DataDictionary;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author HXT
 * @create 2020-10-18 20:04
 */
@WebServlet(value = "/DataDictionaryPageBeanServlet.do")
@Log4j
public class DataDictionaryPageBeanServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("text/html;charset=utf-8");
        DataDictionaryService dataDictionaryService = new DataDictionaryServiceImpl();
        String str_dataId = request.getParameter("dataId");
        int dataId = CastUtil.castInt(str_dataId);
        String dataContent = request.getParameter("dataContent");
        String dataType = request.getParameter("dataType");
        String dataDesc = request.getParameter("dataDesc");

        DataDictionary dataDictionary = new DataDictionary();
        dataDictionary.setDataId(dataId);
        dataDictionary.setDataContent(dataContent);
        dataDictionary.setDataType(dataType);
        dataDictionary.setDataDesc(dataDesc);

        String returnURL = "";

        String m = request.getParameter("m");
        m = m == null ? "all" : m;
        switch (m) {
            case "all":
                all(request, response);
                break;
            case "add":
                int i = dataDictionaryService.save(dataDictionary);
                if (i > 0) {
                    all(request, response);
                }
                break;
            case "delete":
                int delete = dataDictionaryService.delete(dataId);
                if (delete > 0) {
                    all(request, response);
                }
                break;
            case "byid":
                DataDictionary dictionary = dataDictionaryService.getById(dataId);
                request.setAttribute("d",dictionary);
                //重定向至回显界面
                request.getRequestDispatcher("view/system/datadictionary/datadictionary_update.jsp").forward(request, response);
                break;
            case "update":
                int update = dataDictionaryService.update(dataDictionary);
                if (update > 0) {
                    all(request, response);
                } else {
                    returnURL = "DataDictionaryPageBeanServlet.do?m=byid&dataId=" + str_dataId;
                }
                break;
        }
        request.getRequestDispatcher(returnURL).forward(request, response);

    }

    protected void all(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取Session对象
        HttpSession session = request.getSession();
        //设置字符集
        request.setCharacterEncoding("utf-8");
        //设置步长
        int pageSize = 5;
        //设置初始页
        int pageIndex = 1;
        //获取前台页面想要获取的页数
        String str_index = request.getParameter("index");
        //获取前台输入框要查询的值
        String search = request.getParameter("search");
        log.info("请求查询参数:[search="+search+"]");
        if (search == null) {
            search = (String) session.getAttribute("search");
            search = search == null ? "" : search;
        } else {
            session.setAttribute("search", search);
        }
        //如果前台请求的页数不为空，将初始页改为前台请求的页数
        if (str_index != null) {
            pageIndex = Integer.parseInt(str_index);
        }

        DataDictionaryService dataDictionaryService = new DataDictionaryServiceImpl();
        PageBean<DataDictionary> pageBean = dataDictionaryService.findPageBean(pageIndex, pageSize, search);
        request.setAttribute("page", pageBean);

        request.getRequestDispatcher("view/system/datadictionary/datadictionary_list.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
