package org.lq.ems.system.servlet;

import org.lq.ems.system.service.AnthortyInfoService;
import org.lq.ems.system.service.impl.AnthortyInfoServiceImpl;
import org.lq.system.entity.AnthortyInfo;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author HXT
 * @create 2020-10-16 11:00
 */
@WebServlet(value = "/AnthortyInfoPageBeanServlet.do")
public class AnthortyInfoPageBeanServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        AnthortyInfoService anthortyInfoService = new AnthortyInfoServiceImpl();

        //接收前台数据
        String str_anthortyId = request.getParameter("anthortyId");
        int anthortyId = CastUtil.castInt(str_anthortyId);
        String str_anthortyPid = request.getParameter("anthortyPid");
        int anthortyPid = CastUtil.castInt(str_anthortyPid);
        String anthortyName = request.getParameter("anthortyName");
        String anthortyDesc = request.getParameter("anthortyDesc");
        String anthortyUrl = request.getParameter("anthortyUrl");

        AnthortyInfo a = new AnthortyInfo();
        a.setAnthortyId(anthortyId);
        a.setAnthortyPid(anthortyPid);
        a.setAnthortyName(anthortyName);
        a.setAnthortyDesc(anthortyDesc);
        a.setAnthortyUrl(anthortyUrl);

        String returnURL = "";

        //前台传入需要执行的业务类型
        String m = request.getParameter("m");
        switch (m) {
            case "all":
                all(request,response);
                break;
            case "add":
                int i = anthortyInfoService.save(a);
                if (i>0){
                    all(request, response);
                }
                break;
            case "delete":
                int delete = anthortyInfoService.delete(anthortyId);
                if (delete>0){
                    all(request, response);
                }
                break;
            case "byid":
                //重定向至回显界面
                request.getRequestDispatcher("").forward(request, response);
                break;
            case "update":
                int update = anthortyInfoService.update(a);
                if (update>0){
                    all(request,response);
                }else{
                    returnURL = ""+str_anthortyId;//修改页面（修改失败的话，传入id，重新跳回修改页面）
                }
                break;
        }

        //根据当前页，步长，搜索内容，获取一个pageBean对象（对象内封装了属性）
        request.getRequestDispatcher(returnURL).forward(request, response);


    }

    protected void all(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取Session对象
        HttpSession session = request.getSession();
        //设置字符集
        request.setCharacterEncoding("utf-8");
        //设置步长
        int pageSize = 5;
        //设置初始页
        int pageIndex = 1;
        //获取前台页面想要获取的页数
        String str_index = request.getParameter("index");
        //获取前台输入框要查询的值
        String search = request.getParameter("search");

        if (search == null) {
            search = (String) session.getAttribute("search");
            search = search == null ? "" : search;
        } else {
            session.setAttribute("search", search);
        }
        //如果前台请求的页数不为空，将初始页改为前台请求的页数
        if (str_index != null) {
            pageIndex = Integer.parseInt(str_index);
        }
        AnthortyInfoService anthortyInfoService = new AnthortyInfoServiceImpl();
        PageBean<AnthortyInfo> pageBean = anthortyInfoService.findPageBean(pageIndex, pageSize, search);
        request.setAttribute("page", pageBean);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
