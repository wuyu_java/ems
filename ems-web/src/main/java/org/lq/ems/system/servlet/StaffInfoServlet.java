package org.lq.ems.system.servlet;
import java.sql.Timestamp;

import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.StaffInfoService;
import org.lq.ems.system.service.impl.StaffInfoServiceImpl;
import org.lq.system.entity.StaffInfo;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;

/**
 * @author 马秋阳
 * @create 2020-10-13 21:50
 */
@WebServlet("/StaffInfoServlet.do")
@Log4j
public class StaffInfoServlet extends HttpServlet {
    StaffInfoService sis = new StaffInfoServiceImpl();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String m = req.getParameter("m");

        String staffId = req.getParameter("staffId");
        String staffName = req.getParameter("staffName");
        String staffAge = req.getParameter("staffAge");
        String staffSex = req.getParameter("staffSex");
        String staffNativePlace = req.getParameter("staffNativePlace");
        String staffIdcard = req.getParameter("staffIdcard");
        String staffBrithday = req.getParameter("staffBrithday");
        String staffOfficePhone = req.getParameter("staffOfficePhone");
        String staffEamil = req.getParameter("staffEamil");
        String staffMobilePhone = req.getParameter("staffMobilePhone");
        String staffAddr = req.getParameter("staffAddr");
        String staffQq = req.getParameter("staffQq");
        String staffEntryTime = req.getParameter("staffEntryTime");
        String staffEductionLevel = req.getParameter("staffEductionLevel");
        String staffRemark = req.getParameter("staffRemark");
        String userNumber = req.getParameter("userNumber");
        String userPassowrd = req.getParameter("userPassowrd");
        StaffInfo staffInfo = new StaffInfo();
//        staffInfo.setStaffId(0);
//        staffInfo.setRoleId(0);
        staffInfo.setStaffName(staffName);
        staffInfo.setStaffSex(staffSex);
        staffInfo.setStaffAge(CastUtil.castInt(staffAge));
        staffInfo.setStaffNativePlace(staffNativePlace);
        staffInfo.setStaffIdcard(staffIdcard);
        staffInfo.setStaffOfficePhone(staffOfficePhone);
        staffInfo.setStaffMobilePhone(staffMobilePhone);
        staffInfo.setStaffEamil(staffEamil);
        staffInfo.setStaffAddr(staffAddr);
        staffInfo.setStaffQq(staffQq);
        try {
            staffInfo.setStaffBrithday((Timestamp) CastUtil.parse(staffBrithday,CastUtil.TIME_DATE_PATTERN));//这个不会赋值,从前台传回来的是一个字符串
            staffInfo.setStaffEntryTime((Timestamp) CastUtil.parse(staffEntryTime,CastUtil.TIME_DATE_PATTERN));//时间类型,和上面一样不会处理
        } catch (ParseException e) {
            e.printStackTrace();
        }
        staffInfo.setStaffEductionLevel(staffEductionLevel);
        staffInfo.setStaffRemark(staffRemark);
//        staffInfo.setStaffState("");
        staffInfo.setUserNumber(userNumber);
        staffInfo.setUserPassowrd(userPassowrd);

        switch (m){
            case "all":
                all(req,resp);
                break;
            case "add":
                if(sis.save(staffInfo)>0){
                    log.info("添加数据成功");
                    all(req,resp);
                }else {
                    log.warn("添加数据失败");
                    resp.sendRedirect("");
                }
                break;
            case "update":
                staffInfo.setStaffId(CastUtil.castInt(staffId));
                if(sis.update(staffInfo)>0){
                    log.info("修改数据成功");
                    all(req,resp);
                }else {
                    log.warn("修改数据失败");
                    resp.sendRedirect("");
                }
                break;
            case "delete":
                sis.delete(CastUtil.castInt(staffId));
                all(req,resp);
                break;
            case "byId":
                StaffInfo byId = sis.getById(CastUtil.castInt(staffId));
                req.setAttribute("byId",byId);
                req.getRequestDispatcher("").forward(req,resp);
                break;
        }
    }

    protected void all(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        HttpSession session = req.getSession();
        int pageIndex = 1;
        int pageSize = 1;
        String page = req.getParameter("page");
        if(page != null){
            pageIndex = CastUtil.castInt(page);
        }

        String values = req.getParameter("values");
        if(values == null){
            values = (String) session.getAttribute("values");
            values = values == null ? "" : values;
        }else {
            session.setAttribute("values",values);
        }

        PageBean<StaffInfo> pageBean = sis.pageByValues(pageIndex, pageSize, values);
        req.setAttribute("page",pageBean);
        req.getRequestDispatcher("").forward(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }
}
