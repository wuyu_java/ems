package org.lq.ems.system.servlet;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.RoleInfoService;
import org.lq.ems.system.service.impl.RoleInfoServiceImpl;
import org.lq.system.entity.RoleInfo;
import org.lq.util.CastUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 李冠良
 * @create 2020-10-14
 */
@Log4j
@WebServlet("/view/system/roleinfo/AjaxRoleInfoServlet.do")
public class AjaxRoleInfoServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        String method = request.getParameter("method");
        String id_str = request.getParameter("roleId");
        String roleName = request.getParameter("roleName");
        String roleDesc = request.getParameter("roleDesc");
        String roleState = request.getParameter("roleState");
        int role_id = CastUtil.castInt(id_str);

        RoleInfo roleInfo = new RoleInfo();
        roleInfo.setRoleId(role_id);
        roleInfo.setRoleName(roleName);
        roleInfo.setRoleDesc(roleDesc);
        roleInfo.setRoleState(roleState);

        //Ajax分页需要的参数接收和转化
        String pageIndex_str = request.getParameter("page");
        String pageSize_str = request.getParameter("limit");
        int pageIndex = CastUtil.castInt(pageIndex_str);
        int paseSize = CastUtil.castInt(pageSize_str);

        int startIndex = (pageIndex - 1) * paseSize;

        RoleInfoService roleInfoService = new RoleInfoServiceImpl();
        PrintWriter out = response.getWriter();

        String search = request.getParameter("search");
        search = search == null ? "" :search;
        Gson gson = new Gson();
        switch (method) {
            case "all":
                List<RoleInfo> list = roleInfoService.pageByValues(startIndex, paseSize,search);
                Map<String,Object> param = new HashMap<>();
                param.put("code",0);
                param.put("msg","返回成功");
                param.put("count",roleInfoService.getCount(search));
                param.put("data",list);
                out.print(gson.toJson(param));
                break;
            case "enable":
                List<RoleInfo> liststate = roleInfoService.pageByValuesState(pageIndex, paseSize,search);
                Map<String,Object> param2 = new HashMap<>();
                param2.put("code",0);
                param2.put("msg","返回成功");
                param2.put("count",roleInfoService.getCountState(search));
                param2.put("data",liststate);
                out.print(gson.toJson(param2));
                break;
            case "save":
                out.print(roleInfoService.save(roleInfo) > 0);
                break;
            case "update":
                out.print(roleInfoService.update(roleInfo) > 0);
                break;
            case "delete":
                out.print(roleInfoService.delete(role_id) > 0);
                break;
            case "state":
                String state = request.getParameter("state");
                out.print(roleInfoService.updateRoleById(role_id,CastUtil.castInt(state))>0);
                break;
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
