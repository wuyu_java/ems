package org.lq.ems.system.servlet;

import org.lq.ems.system.service.RoleInfoService;
import org.lq.ems.system.service.impl.RoleInfoServiceImpl;
import org.lq.system.entity.RoleInfo;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 李冠良
 * @create 2020-10-14
 */
@WebServlet("/RoleInfoServlet.do")
public class RoleInfoServlet extends HttpServlet {

    private RoleInfoService roleInfoService = new RoleInfoServiceImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String method = request.getParameter("method");

        String id_str = request.getParameter("id");
        RoleInfo roleInfo = null;
        String returnURL = "";      //主页面
        switch (method) {
            case "all":
                findAll(request,response);
                break;
            case "save":
                roleInfo = returnRoleInfo(request, response);
                if (roleInfoService.save(roleInfo) < 0){
                    returnURL = "";     //添加失败返回的页面
                }else {
                    findAll(request,response);
                }
                break;
            case "update":
                roleInfo = returnRoleInfo(request, response);
                roleInfo.setRoleId(Integer.parseInt(id_str));
                //如果修改失败
                if (roleInfoService.update(roleInfo) < 0){
                    returnURL = "";         //修改失败返回的页面
                }else {
                    findAll(request,response);
                }
                break;
            case "byId":
                roleInfo = (RoleInfo) roleInfoService.getById(Integer.parseInt(id_str));
                request.setAttribute("roleInfo",roleInfo);
                returnURL = "";      //获取到对象之后返回修改页面
                break;
            case "delete":
                roleInfoService.delete(Integer.parseInt(id_str));
                findAll(request,response);
                break;
        }

        request.getRequestDispatcher(returnURL).forward(request, response);

    }

    /**
     * 根据传入数据查询全部
     * @param request
     * @param response
     */
    protected void findAll(HttpServletRequest request, HttpServletResponse response){
        int pageindex = 1;  //默认页数为1
        int pagesize = 10;  //默认每页显示的数据条数

        String index_str = request.getParameter("index");
        if (index_str != null) {
            pageindex = Integer.parseInt(index_str);
        }

        String search = request.getParameter("search");
        //如果接收的值为空
        if (search == null) {
            //先从session中取值
            search = (String) request.getSession().getAttribute("search");
            //若仍然为空,则赋值为"",查询全部
            search = search == null ? "" : search;
        }
        request.getSession().setAttribute("search", search);   //存放到session中

        PageBean<RoleInfo> pageBean = roleInfoService.findPageBean(pageindex, pagesize, search);
        request.setAttribute("pageBean", pageBean);
    }

    /**
     * 根据接收到值返回对象
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected RoleInfo returnRoleInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RoleInfo roleInfo = new RoleInfo();
        String roleName = request.getParameter("roleName");
        String roleDesc = request.getParameter("roleDesc");
        String roleState = request.getParameter("roleState");

        roleInfo.setRoleName(roleName);
        roleInfo.setRoleDesc(roleDesc);
        roleInfo.setRoleState(roleState);

        return roleInfo;
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
