package org.lq.ems.finance.servlet;

import org.lq.ems.finance.service.StudentPaymentService;
import org.lq.ems.finance.service.impl.StudentPaymentServiceImpl;
import org.lq.finance.entity.StudentPayment;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * @author 岳鑫
 * @creat 2020-10-13 22:00
 */
@WebServlet("/StudentPayServlet.do")
public class StudentPayServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String m = req.getParameter("m");

        StudentPaymentService service = new StudentPaymentServiceImpl();


        String studentId = req.getParameter("studentId");
        String staffId = req.getParameter("staffId");
        String paymentSituation = req.getParameter("paymentSituation");
        String paymentMethod = req.getParameter("paymentMethod");
        String paymentTime = req.getParameter("paymentTime");
        String discountAmount = req.getParameter("discountAmount");
        String shouldAmount = req.getParameter("shouldAmount");
        String realAmount = req.getParameter("realAmount");
        String debtAmount = req.getParameter("debtAmount");
        String paymentremark = req.getParameter("paymentremark");
        StudentPayment studentPayment = new StudentPayment();

        studentPayment.setStudentId(CastUtil.castInt(studentId));
        studentPayment.setStaffId(CastUtil.castInt(staffId));
        studentPayment.setPaymentSituation(paymentSituation);
        studentPayment.setPaymentMethod(CastUtil.castInt(paymentMethod));
        //时间
//        studentPayment.setPaymentTime();

        studentPayment.setDiscountAmount(CastUtil.castDouble(discountAmount));
        studentPayment.setShouldAmount(CastUtil.castDouble(shouldAmount));
        studentPayment.setRealAmount(CastUtil.castDouble(realAmount));
        studentPayment.setDebtAmount(CastUtil.castDouble(debtAmount));
        studentPayment.setPaymentremark(paymentremark);




        int pageIndex = 1;
        int pageSize = 5;
        String index_str = req.getParameter("index");
        if(index_str != null){
            pageIndex = Integer.parseInt(index_str);

        }

        String name = req.getParameter("name");
        if(name ==null){
            name = (String) req.getSession().getAttribute("query");
            name = name == null ? "" : name;
        }
        req.getSession().setAttribute("query",name);

        String id_str = req.getParameter("id");


        switch (m){
            case "all":
                PageBean<StudentPayment> pageBean = service.pageByValues(pageIndex,pageSize,name);
                req.setAttribute("page",pageBean);
                break;
            case "byid":
                req.setAttribute("s",service.getById(Integer.parseInt(id_str)));

                break;
            case "delete":
                service.delete(Integer.parseInt(id_str));
                pageBean = service.pageByValues(pageIndex,pageSize,name);
                req.setAttribute("page",pageBean);
                break;
            case "update":
                studentPayment.setPaymentId(CastUtil.castInt(id_str));
                if(service.update(studentPayment)){

                }else {

                }
                break;
            case "add":
                if(service.save(studentPayment)){

                }else {

                }
                break;
        }


    }
}
