package org.lq.ems.student.servlet;

import org.lq.student.ems.student.service.CommunicateInfoService;
import org.lq.student.ems.student.service.impl.CommunicateInfoServiceImpl;
import org.lq.student.entity.CommunicateInfo;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @author 南以南
 * @create 2020-10-18 13:15
 */
@WebServlet("/CommunicateInfoAddServlet.do")
public class CommunicateInfoAddServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String returnURL = "CommunicateInfoServlet.do?m=all";
        String communicate_content = request.getParameter("communicate_content");
        String student_id = request.getParameter("student_id");


        CommunicateInfo c = new CommunicateInfo();
        c.setStudentId(Integer.parseInt(request.getParameter(student_id)));
        c.setCommunicateContent(communicate_content);

        CommunicateInfoService communicateInfoService = new CommunicateInfoServiceImpl();


        String m = request.getParameter("m");
        if ("add".equals(m)) {
            if (communicateInfoService.save(c) ) {
                returnURL = "main.jsp";
            }

        } else {
            String id_str = request.getParameter("student_id");
            c.setStudentId(Integer.parseInt(id_str));

            if (communicateInfoService.update(c) ) {
                returnURL = "CommunicateInfoServlet.do?m=byid&id=" + id_str;
            }
        }
        response.sendRedirect(returnURL);
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
