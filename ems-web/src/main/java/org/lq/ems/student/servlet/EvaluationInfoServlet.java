package org.lq.ems.student.servlet;

import org.lq.student.ems.student.service.EvaluationInfoService;
import org.lq.student.ems.student.service.impl.EvaluationInfoServiceImpl;
import org.lq.student.entity.EvaluationInfo;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hello
 * @create 2020-10 -18
 */

@WebServlet("/EvaluationInfoServlet.do")
public class EvaluationInfoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EvaluationInfoService evaluationInfoService = new EvaluationInfoServiceImpl();
        String m = request.getParameter("m");
        int pageIndex =1;
        int pageSize =3;
        String index_str = request.getParameter("index");

        String title = request.getParameter("title");//获取主题

        if(index_str!=null){
            pageIndex=Integer.parseInt(index_str);
        }
        String name = request.getParameter("name");
        if (name==null){
            name=(String) request.getSession().getAttribute("query");
            name =name ==null?"":name;

        }

        request.getSession().setAttribute("query",name);
        String id_str = request.getParameter("id");

        String returnURL = "main.jsp";
        switch (m){
            case "all" :
                PageBean<EvaluationInfo> pageBean = evaluationInfoService.pageByValues(pageIndex, pageSize, name);
                request.setAttribute("page",pageBean);
                break;
//            case "byid":
//                request.setAttribute("s",evaluationInfoService.getByid(Integer.parseInt(id_str)));
//                returnURL = "Student.jsp";
//                break;

            case "bytitle":
                PageBean<EvaluationInfo> pageBean2 = evaluationInfoService.pageByValues(pageIndex, pageSize, title);
                request.setAttribute("page",pageBean2);
//                request.setAttribute("s",evaluationInfoService.getByid(Integer.parseInt(id_str)));
//                returnURL = "Student.jsp";
                break;
            case "delete":
                boolean i  = evaluationInfoService.delete(Integer.parseInt(id_str));
                pageBean = evaluationInfoService.pageByValues(pageIndex, pageSize, name);
                request.setAttribute("page",pageBean);
                break;
        }

        request.getRequestDispatcher(returnURL).forward(request,response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
    }
}
