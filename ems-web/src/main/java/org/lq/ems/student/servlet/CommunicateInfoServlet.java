package org.lq.ems.student.servlet.CommunicateInfo;

import org.lq.student.ems.student.service.CommunicateInfoService;
import org.lq.student.ems.student.service.StudentInfoService;
import org.lq.student.ems.student.service.impl.CommunicateInfoServiceImpl;
import org.lq.student.ems.student.service.impl.StudentInfoServiceImpl;
import org.lq.student.entity.CommunicateInfo;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author 南以南
 * @create 2020-10-18 12:59
 */
@WebServlet("/CommunicateInfoServlet.do")
public class CommunicateInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CommunicateInfoService communicateInfoService = new CommunicateInfoServiceImpl();
        String m = request.getParameter("m");
        int startIndex = 1;
        int pageSize = 5;

        String index_str = request.getParameter("index");
        if(index_str!=null){
            startIndex = Integer.parseInt(index_str);
        }
        String name = request.getParameter("student_name");

        if(name==null){
            name = (String) request.getSession().getAttribute("query");
            name = name == null ? "" :name;
        }
        request.getSession().setAttribute("query",name);
        String id_str = request.getParameter("communicate_id");

        String returnURL = "main.jsp";
        switch (m){
            case "all":
                PageBean<CommunicateInfo> pageBean = communicateInfoService.pageByValues(startIndex,pageSize,name);
                request.setAttribute("page",pageBean);
                break;
            case "byid":
                request.setAttribute("s",communicateInfoService.getById(Integer.parseInt(id_str)));
                returnURL ="CommunicateInfo.jsp";
                break;
            case "delete":
                boolean i = communicateInfoService.delete(Integer.parseInt(id_str));
                pageBean = communicateInfoService.pageByValues(startIndex,pageSize,name);
                request.setAttribute("page",pageBean);
                break;
        }
        request.getRequestDispatcher(returnURL).forward(request,response);

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);

    }
}
