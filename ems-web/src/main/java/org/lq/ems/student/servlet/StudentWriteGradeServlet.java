package org.lq.ems.student.servlet;

import org.apache.commons.beanutils.BeanUtils;
import org.lq.student.ems.student.service.StudentWriteGradeService;
import org.lq.student.ems.student.service.impl.StudentWriteGradeServiceImpl;
import org.lq.student.entity.StudentWriteGrade;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @author haha
 * @create 2020-10-18 22:19
 */
@WebServlet("/StudentWriteGradeServlet")
public class StudentWriteGradeServlet extends HttpServlet {
    private StudentWriteGradeService writeGradeService=new StudentWriteGradeServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String m = request.getParameter("m");
        switch (m){
            case "pageList":
                pageList(request,response);
                break;
            case "pageByValues":
                pageByValues(request,response);
                break;
            case "save":
                save(request,response);
                break;
            case "update":
                update(request,response);
                break;
            case "delete":
                delete(request,response);
                break;
        }
    }
    protected void pageList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int pageSize=5;
        int pageIndex=1;
        PageBean<StudentWriteGrade> page = writeGradeService.pageList(pageIndex, pageSize);
        request.setAttribute("page",page);
        request.getRequestDispatcher("studentWriteGrade.jsp").forward(request,response);
    }
    protected void pageByValues(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int pageSize=5;
        int pageIndex=1;
        String search = request.getParameter("search");
        PageBean<StudentWriteGrade> page = writeGradeService.pageByValues(pageIndex, pageSize,search);
        request.setAttribute("page",page);
        request.getRequestDispatcher("studentWriteGrade.jsp").forward(request,response);
    }
    protected void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String[]> map = request.getParameterMap();
        StudentWriteGrade studentWriteGrade=new StudentWriteGrade();
        try {
            //把map数据分转到对象里
            BeanUtils.populate(studentWriteGrade,map);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if(writeGradeService.save(studentWriteGrade)){
            //跳转到显示全部页面
            response.sendRedirect("studentWriteGrade.jsp");
        }
    }
    protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String[]> map = request.getParameterMap();
        StudentWriteGrade studentWriteGrade=new StudentWriteGrade();
        try {
            BeanUtils.populate(studentWriteGrade,map);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        if(writeGradeService.update(studentWriteGrade)){
            //跳转到显示全部页面
            response.sendRedirect("studentWriteGrade.jsp");
        }
    }
    protected void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String writeGradeId = request.getParameter("writeGradeId");
        if(writeGradeService.delete(CastUtil.castInt(writeGradeId))){
            //跳转到显示全部页面
            response.sendRedirect("studentWriteGrade.jsp");
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);
    }
}
