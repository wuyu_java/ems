package org.lq.ems.student.servlet;
import java.sql.Timestamp;

import org.lq.student.ems.student.service.EvaluationInfoService;
import org.lq.student.ems.student.service.impl.EvaluationInfoServiceImpl;
import org.lq.student.entity.EvaluationInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hello
 * @create 2020-10 -19
 */
@WebServlet("/EvaluationInfoAddServlet")
public class EvaluationInfoAddServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String returnURl = "EvaluationInfoServlet.do?m=all";
        String stu_id = request.getParameter("stu_id");
        String title = request.getParameter("title");
        String content = request.getParameter("content");
        String course = request.getParameter("course");
        String teacher = request.getParameter("teacher");


        EvaluationInfo e = new EvaluationInfo();
        e.setStudentId(Integer.parseInt(stu_id));
        e.setEvaluationTitle(title);
        e.setEvaluationContent(content);
        e.setEvaluationCourse(course);
        e.setEvaluationTeacher(teacher);
        e.setEvaluationTime(new Timestamp(new java.util.Date().getTime()));


        EvaluationInfoService evaluationInfoService = new EvaluationInfoServiceImpl();
        String m = request.getParameter("m");
        if ("add".equals(m)){
            if(evaluationInfoService.save(e)){
                returnURl="main.jsp";
            }
        }else {




        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request,response);
    }
}
