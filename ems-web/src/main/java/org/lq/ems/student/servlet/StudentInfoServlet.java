package org.lq.ems.student.servlet;

import org.lq.student.ems.student.service.StudentInfoService;
import org.lq.student.ems.student.service.impl.StudentInfoServiceImpl;
import org.lq.student.entity.StudentInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import java.io.IOException;

/**
 * @author 秦洪涛
 * @create 2020-10-14-8:40
 */
@WebServlet("/StudentInfoServlet.do")
public class StudentInfoServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String method = request.getParameter("method");
        StudentInfoService service = new StudentInfoServiceImpl();
        String id_str = request.getParameter("id");
        int id = 0;

        int pageSize=5;
        int startIndex=1;
        String value="";

        if (id_str != null) {
            id = Integer.parseInt(id_str);
        }


        int staffId = Integer.parseInt(request.getParameter("staffId"));
        int classId = Integer.parseInt(request.getParameter("classId"));
        String studentName = request.getParameter("studentName");
        String studentSex = request.getParameter("studentSex");
        int studentAge = Integer.parseInt(request.getParameter("studentAge"));
        String studentTellphone = request.getParameter("studentTellphone");
        String studentEmail = request.getParameter("studentEmail");
        String studentIdcard = request.getParameter("studentIdcard");
        String studentAddress = request.getParameter("studentAddress");
        String studentBirthday = request.getParameter("studentBirthday");
        String studentQQ = request.getParameter("studentQQ");
        String studentSchool = request.getParameter("studentSchool");
        String studentParentsName = request.getParameter("studentParentsName");
        String studentParentsPhone = request.getParameter("studentParentsPhone");
        String studentPro = request.getParameter("studentPro");
        String studentProCity = request.getParameter("studentProCity");
        String studentType = request.getParameter("studentType");
        String studentIspay = request.getParameter("studentIspay");
        int studentSate = Integer.parseInt(request.getParameter("studentSate"));
        String studentMark = request.getParameter("studentMark");
        String studentDesc = request.getParameter("studentDesc");
        String studentNumber = request.getParameter("studentNumber");
        String studentPassword = request.getParameter("studentPassword");
        StudentInfo s = new StudentInfo();

        s.setStaffId(staffId);
        s.setClassId(classId);
        s.setStudentName(studentName);
        s.setStudentSex(studentSex);
        s.setStudentAge(studentAge);
        s.setStudentTellphone(studentTellphone);
        s.setStudentEmail(studentEmail);
        s.setStudentIdcard(studentIdcard);
        s.setStudentAddress(studentAddress);
        s.setStudentBirthday(studentBirthday);
        s.setStudentSchool(studentSchool);
        s.setStudentQQ(studentQQ);
        s.setStudentParentsName(studentParentsName);
        s.setStudentParentsPhone(studentParentsPhone);
        s.setStudentPro(studentPro);
        s.setStudentProCity(studentProCity);
        s.setStudentType(studentType);
        s.setStudentIspay(studentIspay);
        s.setStudentSate(studentSate);
        s.setStudentMark(studentMark);
        s.setStudentDesc(studentDesc);
        s.setStudentNumber(studentNumber);
        s.setStudentPassword(studentPassword);



        switch (method) {
            case "all":
                request.setAttribute("studentinfo", service.pageByValues(startIndex,pageSize,value));
                request.getRequestDispatcher("studentinfo.jsp").forward(request, response);
                break;
            case "add":
                if (service.save(s)) {
                    request.setAttribute("studentinfo", service.pageByValues(startIndex,pageSize,value));
                    request.getRequestDispatcher("studentinfo.jsp").forward(request, response);
                } else {
                    response.sendRedirect("add.jsp");
                }
                break;
            case "byid":
                request.setAttribute("getstudent", service.getStuById(id));
                request.getRequestDispatcher("update.jsp").forward(request, response);
                break;
            case "update":
                s.setStudentId(id);
                if (service.update(s)) {
                    request.setAttribute("studentinfo",  service.pageByValues(startIndex,pageSize,value));
                    request.getRequestDispatcher("studentinfo.jsp").forward(request, response);
                } else {
                    response.sendRedirect("update.jsp");
                }
                break;
            case "delete":
                service.delete(id);
                request.setAttribute("studentinfo", service.pageByValues(startIndex,pageSize,value));
                request.getRequestDispatcher("studentinfo.jsp").forward(request, response);
                break;

        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            this.doPost(request,response);
    }
}
