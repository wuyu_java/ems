<%@page language="java" isELIgnored="false" contentType="text/html; charset=utf-8"
        import="org.lq.util.DataDicationary,org.apache.commons.lang3.StringUtils" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>首页</title>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="${static_result}/css/bootstrap.min.css">
<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="${static_result}js/jquery.min.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="${static_result}js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${static_result}page/pagination.css">
    <script type="text/javascript" src="${static_result}page/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="${static_result}page/jquery.pagination.js"></script>

    <link rel="stylesheet" type="text/css" href="${static_result}layui/css/layui.css">
    <script src="${static_result}layui/layui.js"></script>

</head>

<body>
<div style="padding:0px; margin:0px;">
 <ul class="breadcrumb" style="  margin:0px; " >
    	<li><a href="#">系统管理</a></li>
        <li>数据字典</li>
    </ul>
</div>
<div class="row alert alert-info"  style="margin:0px; padding:3px;" >
<form class="form-horizontal" action="DataDictionaryPageBeanServlet.do" method="post">
	<div class="col-sm-2">类型:</div>
    <div class="col-sm-3">
    	<input type="text" name="search" value="${search}" class="form-control input-sm"/>
    </div>
    <input type="submit"   class="btn btn-danger"     value="查询"/>
    <a  class="btn btn-success"  href="view/system/datadictionary/datadictionary_add.jsp"   >添加</a>
    </form>
</div>
<div class="row" style="padding:15px; padding-top:0px; ">
	<table class="table  table-condensed table-striped">
    	<tr>
        	<th>编号</th>
            <th>名称</th>
            <th>类型</th>
            <th>描述</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${page.data}" var="d">
       	<tr>
        	<td>${d.dataId}</td>
            <td>${d.dataContent}</td>
            <td>${DataDicationary.valueOf(StringUtils.upperCase(d.dataType)).getTitle()}</td>
            <td>${d.dataDesc}</td>
            <th><a href="${static_result}DataDictionaryPageBeanServlet.do?m=byid&dataId=${d.dataId}">修改</a>
                <a href="#" data-id="${d.dataId}" class="del">删除</a></th>
        </tr>
        </c:forEach>

        <tr>
            <td colspan="5">
                <div id="page" class="m-style"></div>
            </td>
        </tr>

    </table>
</div>
<script>
    $(function () {
        $("#page").pagination({
            homePage: '首页',
            endPage: '末页',
            prevContent: '上页',
            nextContent: '下页',
            current:${page.currentPage},
            pageCount:${page.totalPage},
            coping:true,
            callback:function(index){
                console.log(index.getCurrent());
                location.href = "DataDictionaryPageBeanServlet.do?m=all&index="+index.getCurrent();
            }
        });
    })
</script>


<script>
    layui.use('layer', function() { //独立版的layer无需执行这一句
        var $ = layui.jquery, layer = layui.layer; //独立版的layer无需执行这一句
        $(".del").click(function (e) {
            var _this = this;
            e.preventDefault();
            layer.confirm('确定删除吗?', {icon: 3, title:'提示'}, function(index){
                //do something
                location.href = "${static_result}DataDictionaryPageBeanServlet.do?m=delete&dataId="+_this.dataset.id;
                layer.close(index);
            });


        })
    })
</script>

</body>
</html>
