package org.lq.classinfo.entity;

/**
 * 课程表实体类
 */
public class SyllabusInfo {

  private int syllabusId;     //课程id
  private String syllabusYi;  //星期一课程
  private String syllabusEr;  //星期二课程
  private String syllabusSan; //星期三课程
  private String syllabusSi;  //星期四课程
  private String syllabusWu;  //星期五课程
  private String syllabusLiu; //星期六课程
  private String syllabusQi;  //星期日课程
  private String isUesd;      //是否被使用
  private String syllabusName;//课程名称

  @Override
  public String toString() {
    return "SyllabusInfo{" +
            "syllabusId=" + syllabusId +
            ", syllabusYi='" + syllabusYi + '\'' +
            ", syllabusEr='" + syllabusEr + '\'' +
            ", syllabusSan='" + syllabusSan + '\'' +
            ", syllabusSi='" + syllabusSi + '\'' +
            ", syllabusWu='" + syllabusWu + '\'' +
            ", syllabusLiu='" + syllabusLiu + '\'' +
            ", syllabusQi='" + syllabusQi + '\'' +
            ", isUesd='" + isUesd + '\'' +
            ", syllabusName='" + syllabusName + '\'' +
            '}';
  }

  public int getSyllabusId() {
    return syllabusId;
  }

  public void setSyllabusId(int syllabusId) {
    this.syllabusId = syllabusId;
  }


  public String getSyllabusYi() {
    return syllabusYi;
  }

  public void setSyllabusYi(String syllabusYi) {
    this.syllabusYi = syllabusYi;
  }


  public String getSyllabusEr() {
    return syllabusEr;
  }

  public void setSyllabusEr(String syllabusEr) {
    this.syllabusEr = syllabusEr;
  }


  public String getSyllabusSan() {
    return syllabusSan;
  }

  public void setSyllabusSan(String syllabusSan) {
    this.syllabusSan = syllabusSan;
  }


  public String getSyllabusSi() {
    return syllabusSi;
  }

  public void setSyllabusSi(String syllabusSi) {
    this.syllabusSi = syllabusSi;
  }


  public String getSyllabusWu() {
    return syllabusWu;
  }

  public void setSyllabusWu(String syllabusWu) {
    this.syllabusWu = syllabusWu;
  }


  public String getSyllabusLiu() {
    return syllabusLiu;
  }

  public void setSyllabusLiu(String syllabusLiu) {
    this.syllabusLiu = syllabusLiu;
  }


  public String getSyllabusQi() {
    return syllabusQi;
  }

  public void setSyllabusQi(String syllabusQi) {
    this.syllabusQi = syllabusQi;
  }


  public String getIsUesd() {
    return isUesd;
  }

  public void setIsUesd(String isUesd) {
    this.isUesd = isUesd;
  }


  public String getSyllabusName() {
    return syllabusName;
  }

  public void setSyllabusName(String syllabusName) {
    this.syllabusName = syllabusName;
  }

}
