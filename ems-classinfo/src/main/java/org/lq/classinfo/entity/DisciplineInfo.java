package org.lq.classinfo.entity;

import lombok.Data;
import lombok.ToString;

/**
 * 学科信息实体类
 */
@Data
@ToString
public class DisciplineInfo {

  private int disciplineId;// 学科编号
  private String disciplineName;// 学科名称
  private int disciplineTuition;// 学科费用
  private int disciplineBring;// 学科课时
  private String disciplineDesc;// 备注
  private String disciplineIsuesd;// 是否有效

}
