package org.lq.classinfo.entity;

import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;

/**
 * @author wynn
 * @create2020-10-13 18:10
 */
@Data
@ToString
public class ClassInfo {
    private int classId;//班级编号
    private int disciplineId;//学科编号
    private int syllabusId;//课程表编号
    private int classroomId;//教室编号
    private int staffId;//员工编号
    private String className;//班级名称
    private int classNumber;//班级人数
    private Timestamp classStartTime;//开班时间
    private Timestamp classEndTime;//结束时间
    private String classIsuesd;//是否有效
    private String classState;//状态
    private String classDesc;//描述信息



}
