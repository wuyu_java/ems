package org.lq.classinfo.entity;

import lombok.Data;

import java.lang.reflect.Field;
import java.sql.Timestamp;

/**
 * 教师信息实体类
 * @author 张三
 * @create 2020-10-13 18:26
 */
@Data
public class TeacherInfo {
    private int staffId; //员工编号
    private int roleId = 4; //角色编号 默认教师
    private String staffName; //姓名
    private String staffSex; //性别
    private int staffAge; //年龄
    private String staffNativePlace; //员工籍贯
    private String staffIdcard; //身份证号码
    private Timestamp staffBrithday; //出生年月
    private String staffOfficePhone; //办公电话
    private String staffMobilePhone; //移动电话
    private String staffEamil; //电子邮箱
    private String staffAddr; //家庭住址
    private String staffQq; //QQ号
    private Timestamp staffEntryTime; //入职时间
    private String staffEductionLevel; //教育水平
    private String staffRemark; //备注信息
    private String staffState; //状态 1在职 2离职
    private String userNumber; //账号
    private String userPassowrd; //密码
    
}
