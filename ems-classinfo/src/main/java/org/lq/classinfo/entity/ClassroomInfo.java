package org.lq.classinfo.entity;

import lombok.Data;

/**
 *  教室实体类
 *  * @author 邹倩倩
 */
@Data
public class ClassroomInfo {

  private int classroomId;//教室编号
  private String classroomName;//教室名称
  private int classroomMax;//容纳人数
  private String classroomInfo;//设备信息爱
  private String classroomRemark;//备注信息
  private String classroomMark;//标识



}
