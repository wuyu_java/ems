package org.lq.classinfo.dao;

import org.lq.base.BaseDao;
import org.lq.classinfo.entity.TeacherInfo;

import java.util.List;

/**
 * @author 张三
 * @create 2020-10-13 18:28
 */
public interface TeacherInfoDao extends BaseDao<TeacherInfo> {

    int getCountByName(String name);

    int getCountBySex(String sex);

    List<TeacherInfo> pageByName(int startIndex, int pageSize, String name);

    List<TeacherInfo> pageBySex(int startIndex, int pageSize, String sex);
}
