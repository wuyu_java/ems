package org.lq.classinfo.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.classinfo.dao.ClassInfoDao;
import org.lq.classinfo.entity.ClassInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 韩伟伟
 * @create2020-10-13 18:32
 */
@Log4j
public class ClassInfoDaoImpl implements ClassInfoDao {
    @Override
    public int save(ClassInfo classInfo) {
        int num=0;
        try {
            log.info("添加班级进入...");
            num = new QueryRunner(JDBCUtil.getDataSource()).update("insert into class_info(discipline_id,syllabus_id,classroom_id,staff_id,class_name,class_number,class_start_time,class_end_time,class_isuesd,class_state,class_desc)" +
                            "values(?,?,?,?,?,?,?,?,?,?,?)",
                    classInfo.getDisciplineId(), classInfo.getSyllabusId(), classInfo.getClassroomId(), classInfo.getStaffId(),
                    classInfo.getClassName(), classInfo.getClassNumber(), classInfo.getClassStartTime(), classInfo.getClassEndTime(),
                    classInfo.getClassIsuesd(),classInfo.getClassState(),classInfo.getClassDesc()
            );
            log.info("添加班级,结果:"+num);
            return num;
        } catch (SQLException e) {
            log.error("添加班级-->save错误"+e);
        }
        return 0;
    }

    @Override
    public int update(ClassInfo classInfo) {
        int num = 0;
        try {
            log.info("修改班级进入...");
            num = new QueryRunner(JDBCUtil.getDataSource()).update("update class_info set discipline_id=?,syllabus_id=?,classroom_id=?,staff_id=?," +
                            "class_name=?,class_number=?,class_start_time=?,class_end_time=?,class_isuesd=?,class_state=?,class_desc=? where class_id=?",
                    classInfo.getDisciplineId(), classInfo.getSyllabusId(), classInfo.getClassroomId(), classInfo.getStaffId(),
                    classInfo.getClassName(), classInfo.getClassNumber(), classInfo.getClassStartTime(), classInfo.getClassEndTime(),
                    classInfo.getClassIsuesd(),classInfo.getClassState(), classInfo.getClassDesc(),classInfo.getClassId());
            log.info("修改班级,结果:"+num);
            return num;
        } catch (SQLException e) {
            log.error("修改班级-->update错误"+e);
        }
        return 0;
    }

    @Override
    public int delete(int id) {
        int num = 0;
        try {
            log.info("删除班级进入...");
            num = new QueryRunner(JDBCUtil.getDataSource()).update("delete from class_info where class_id=?",id);
            log.info("删除班级,结果:"+num);
            return num;
        } catch (SQLException e) {
            log.error("删除班级-->delete错误"+e);
        }
        return 0;
    }

    @Override
    public ClassInfo getById(int id) {
        ClassInfo classInfo = null;
        try {
            log.info("根据ID查询班级进入...");
            classInfo = new QueryRunner(JDBCUtil.getDataSource()).query("select * from v_class_info where classId=?", new BeanHandler<>(ClassInfo.class), id);
            log.info("根据ID查询班级,结果:"+classInfo);
            return classInfo;
        } catch (SQLException e) {
            log.error("根据ID查询班级-->getById错误"+e);
        }
        return null;
    }

    /**
     * 总行数
     * @return
     */
    @Override
    public int getCount() {
        int count=0;
        try {
            log.info("获取总行数进入...");
            count = new QueryRunner(JDBCUtil.getDataSource()).query("select count(1) from v_class_info", new ScalarHandler<Long>()).intValue();
            log.info("获取总行数,结果:"+count);
            return count;
        } catch (SQLException e) {
            log.error("获取总行数-->getCount错误"+e);
        }
        return 0;
    }
    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<ClassInfo> pageList(int startIndex, int pageSize) {
        List<ClassInfo> list = new ArrayList<>();
        try {
            log.info("分页查询进入...");
            list = new QueryRunner(JDBCUtil.getDataSource()).query("select * from v_class_info limit ?,?",new BeanListHandler<>(ClassInfo.class),startIndex,pageSize);
            log.info("分页查询,结果:"+list);
            return list;
        } catch (SQLException e) {
            log.error("分页查询-->pageList错误"+e);
        }
        return null;
    }
    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        int count = 0;
        try {
            log.info("根据条件查询总行数进入...");
            count = new QueryRunner(JDBCUtil.getDataSource()).query("select count(1) from v_class_info where className like ?",
                    new ScalarHandler<Long>(),"%"+values[0]+"%").intValue();
            log.info("根据条件查询总行数,结果:"+count);
            return count;
        } catch (SQLException e) {
            log.error("根据条件查询总行数-->getCount错误"+e);
        }
        return 0;
    }
    /**
     * 根据条件分页查询
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<ClassInfo> pageByValues(int startIndex, int pageSize, String... value) {
        List<ClassInfo> list = new ArrayList<>();
        try {
            log.info("根据条件分页查询进入...");
            list = new QueryRunner(JDBCUtil.getDataSource()).query("select * from v_class_info where className like ? limit ?,?",
                    new BeanListHandler<>(ClassInfo.class),"%"+value[0]+"%",
                    startIndex,pageSize);
            log.info("根据条件分页查询,结果:"+list);
            return list;
        } catch (SQLException e) {
            log.error("根据条件分页查询-->pageByValues错误"+e);
        }
        return null;
    }

}
