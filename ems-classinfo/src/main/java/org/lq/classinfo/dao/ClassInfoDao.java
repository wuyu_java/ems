package org.lq.classinfo.dao;

import org.lq.base.BaseDao;
import org.lq.classinfo.entity.ClassInfo;

/**
 * @author wynn
 * @create2020-10-13 18:20
 */
public interface ClassInfoDao extends BaseDao<ClassInfo> {

}
