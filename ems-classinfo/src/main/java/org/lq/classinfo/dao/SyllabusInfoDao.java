package org.lq.classinfo.dao;

import org.lq.base.BaseDao;
import org.lq.classinfo.entity.SyllabusInfo;

/**
 * @author 刘明昕
 * @created 2020-10-13 18:12
 */
public interface SyllabusInfoDao extends BaseDao<SyllabusInfo> {

}
