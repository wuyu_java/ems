package org.lq.classinfo.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.*;
import org.lq.classinfo.dao.ClassroominfoDao;
import org.lq.classinfo.entity.ClassroomInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * 教室表数据连接层实现类
 * @author 邹倩倩
 * @create 2020 10 13 2020-10-13
 */
@Log4j
public class ClassroominfoDaoImpl implements ClassroominfoDao {
    /**
     * t添加教室
     *
     * @param classroomInfo
     * @return
     */
    @Override
    public int save(ClassroomInfo classroomInfo) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.update("insert into classroom_info(classroom_name,classroom_max,classroom_info,classroom_remark,classroom_mark) values (?,?,?,?,?)",
                    classroomInfo.getClassroomName(),classroomInfo.getClassroomMax(),classroomInfo.getClassroomInfo(),classroomInfo.getClassroomRemark(),classroomInfo.getClassroomMark());
        } catch (SQLException e) {
            log.error("教室数据访问层添加报错：" + e);
        }
        return 0;
    }

    /**
     * 修改
     *
     * @param classroomInfo
     * @return
     */
    @Override
    public int update(ClassroomInfo classroomInfo) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.update("update classroom_info set classroom_name=?,classroom_max=?,classroom_info=?,classroom_remark=?,classroom_mark=? where classroom_id=?",
                    classroomInfo.getClassroomName(),classroomInfo.getClassroomMax(),classroomInfo.getClassroomInfo(),classroomInfo.getClassroomRemark(),classroomInfo.getClassroomMark(),classroomInfo.getClassroomId());
        } catch (SQLException e) {
            log.error("教室数据访问层修改报错：" + e);
        }
        return 0;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.update("delete from classroom_info where classroom_id=?",id);
        } catch (SQLException e) {
            log.error("教室数据访问层删除报错：" + e);
        }
        return 0;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public ClassroomInfo getById(int id) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("haha");
        try {
            return queryRunner.query("select * from v_classroom_info where classroomId=?",new BeanHandler<>(ClassroomInfo.class),id);
        } catch (SQLException e) {
            log.error("教室数据访问层通过ID查询报错：" + e);
        }
        return null;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.query("select count(1) from v_classroom_info",new ScalarHandler<Long>()).intValue();
        } catch (SQLException e) {
            log.error("教室数据访问层总行数报错：" + e);
        }
        return 0;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<ClassroomInfo> pageList(int startIndex, int pageSize) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.query("select * from v_classroom_info limit ?,?",new BeanListHandler<>(ClassroomInfo.class),startIndex,pageSize);
        } catch (SQLException e) {
            log.error("教室数据访问层分页查询数报错：" + e);
        }
        return null;
    }

    /**
     * 根据班级名称查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.query("select count(1) from v_classroom_info where classroomName like ?",new ScalarHandler<Long>(),"%"+values[0]+"%").intValue();
        } catch (SQLException e) {
            log.error("教室数据访问层根据班级名称查询总行数数报错：" + e);
        }
        return 0;
    }

    /**
     * 根据班级名称分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<ClassroomInfo> pageByValues(int startIndex, int pageSize, String... value) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.query("select * from v_classroom_info where classroomName like ? limit ?,?",new BeanListHandler<>(ClassroomInfo.class),"%"+value[0]+"%",startIndex,pageSize);
        } catch (SQLException e) {
            log.error("教室数据访问层根据班级名称分页查询报错：" + e);
        }
        return null;
    }
}
