package org.lq.classinfo.dao.impl;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.classinfo.dao.DisciplineInfoDao;
import org.lq.classinfo.entity.DisciplineInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Sky
 * @create 2020-10-13 18:24
 */
@Log4j
public class DisciplineInfoDaoImpl implements DisciplineInfoDao {
    /**
     * 学科信息添加
     *
     * @param disciplineInfo
     * @return
     */
    @Override
    public int save(DisciplineInfo disciplineInfo) {
        log.info("添加变量:"+disciplineInfo);
        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .update("insert into discipline_info(discipline_name," +
                            "discipline_tuition," +
                            "discipline_bring," +
                            "discipline_desc," +
                            "discipline_isuesd) values (?,?,?,?,'0')",
                            disciplineInfo.getDisciplineName(),
                            disciplineInfo.getDisciplineTuition(),
                            disciplineInfo.getDisciplineBring(),
                            disciplineInfo.getDisciplineDesc());
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("学科信息添加错误:"+e.getMessage());
        }
        return 0;
    }

    /**
     * 学科信息修改
     *
     * @param disciplineInfo
     * @return
     */
    @Override
    public int update(DisciplineInfo disciplineInfo) {
        log.info("修改变量:"+disciplineInfo);
        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .update("update discipline_info set discipline_name=?," +
                    "discipline_tuition=?," +
                    "discipline_bring=?," +
                    "discipline_desc=?," +
                    "discipline_isuesd='0' " +
                    "where discipline_id = ?",
                    disciplineInfo.getDisciplineName(),
                    disciplineInfo.getDisciplineTuition(),
                    disciplineInfo.getDisciplineBring(),
                    disciplineInfo.getDisciplineDesc(),
                    disciplineInfo.getDisciplineId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("学科信息修改错误:"+throwables.getMessage());
        }
        return 0;
    }

    /**
     * 删除学科信息
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("删除变量:"+id);
        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .update("delete from discipline_info where discipline_id = ?",
                    id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("学科信息删除错误:"+throwables.getMessage());
        }
        return 0;
    }

    /**
     * 通过ID查询学科信息
     *
     * @param id
     * @return
     */
    @Override
    public DisciplineInfo getById(int id) {
        log.info("通过ID查询学科信息变量:"+id);
        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .query("select * from v_discipline_info where discipline_id = ?",
                            new BeanHandler<>(DisciplineInfo.class),id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("通过ID查询学科信息错误:"+throwables.getMessage());
        }
        return null;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {

        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .query("select count(1) from v_discipline_info", new ScalarHandler<Long>()).intValue();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("获取总行数错误:"+throwables.getMessage());
        }
        return 0;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<DisciplineInfo> pageList(int startIndex, int pageSize) {
        log.info("开始位置:"+startIndex+";每页显示的行数:"+pageSize);
        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .query("select * from v_discipline_info limit ?,?",
                    new BeanListHandler<>(DisciplineInfo.class),
                    startIndex,pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("分页查询学科信息错误:"+throwables.getMessage());
        }
        return null;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        log.info("条件查询学科信息总行数变量:"+values);
        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .query("select count(1) from v_discipline_info where discipline_name like ?",
                            new ScalarHandler<Long>(),
                            "%"+values[0]+"%").intValue();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("条件查询学科信息总行数错误:"+throwables.getMessage());
        }
        return 0;
    }

    /**
     * 根据条件分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<DisciplineInfo> pageByValues(int startIndex, int pageSize, String... value) {
        log.info("开始位置:"+startIndex+";每页显示行数:"+pageSize+"条件变量:"+value);
        try {
            return new QueryRunner(JDBCUtil.getDataSource())
                    .query("select * from v_discipline_info where discipline_name like ? limit ?,?",
                            new BeanListHandler<>(DisciplineInfo.class),
                            "%"+value[0]+"%",startIndex,pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("条件分页查询学科信息错误:"+throwables.getMessage());
        }
        return null;
    }
}
