package org.lq.classinfo.dao;

import org.lq.base.BaseDao;
import org.lq.classinfo.entity.ClassroomInfo;

/**
 * 教室数据连接层接口
 * @author 邹倩倩
 * @create 2020 10 13 2020-10-13
 */
public interface ClassroominfoDao extends BaseDao<ClassroomInfo> {

}
