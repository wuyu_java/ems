package org.lq.classinfo.dao;

import org.lq.base.BaseDao;
import org.lq.classinfo.entity.DisciplineInfo;

/**
 * @author Sky
 * @create 2020-10-13 18:20
 */
public interface DisciplineInfoDao extends BaseDao<DisciplineInfo> {
}
