package org.lq.recruitstudent.entity;

/**
 * @Author 冯志远
 * @create 2020-10-13 19:07
 */
public class AuditionInfo {
    private int auditionId;
    private int studentId;
    private java.sql.Timestamp auditionTime;
    private String auditionAddr;
    private String auditionCourse;
    private String auditionDesc;


    public int getAuditionId() {
        return auditionId;
    }

    public void setAuditionId(int auditionId) {
        this.auditionId = auditionId;
    }


    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }


    public java.sql.Timestamp getAuditionTime() {
        return auditionTime;
    }

    public void setAuditionTime(java.sql.Timestamp auditionTime) {
        this.auditionTime = auditionTime;
    }


    public String getAuditionAddr() {
        return auditionAddr;
    }

    public void setAuditionAddr(String auditionAddr) {
        this.auditionAddr = auditionAddr;
    }


    public String getAuditionCourse() {
        return auditionCourse;
    }

    public void setAuditionCourse(String auditionCourse) {
        this.auditionCourse = auditionCourse;
    }


    public String getAuditionDesc() {
        return auditionDesc;
    }

    public void setAuditionDesc(String auditionDesc) {
        this.auditionDesc = auditionDesc;
    }
}
