package org.lq.recruitstudent.entity;


import java.sql.Timestamp;

/**
 * 跟踪信息实体类
 */
public class TrackRecordInfo {

  private int trackRecordId;//记录编号
  private int studentId;//学院编号
  private String trackRecordTitle;//主题
  private String trackRecordContent;//具体内容
  private Timestamp trackRecordTime;//联系时间
  private int enrollment;//意向状况
  private Timestamp nextRecordTime;//下次联系时间

  public TrackRecordInfo() {
  }

  public int getTrackRecordId() {
    return this.trackRecordId;
  }

  public int getStudentId() {
    return this.studentId;
  }

  public String getTrackRecordTitle() {
    return this.trackRecordTitle;
  }

  public String getTrackRecordContent() {
    return this.trackRecordContent;
  }

  public Timestamp getTrackRecordTime() {
    return this.trackRecordTime;
  }

  public int getEnrollment() {
    return this.enrollment;
  }

  public Timestamp getNextRecordTime() {
    return this.nextRecordTime;
  }

  public void setTrackRecordId(int trackRecordId) {
    this.trackRecordId = trackRecordId;
  }

  public void setStudentId(int studentId) {
    this.studentId = studentId;
  }

  public void setTrackRecordTitle(String trackRecordTitle) {
    this.trackRecordTitle = trackRecordTitle;
  }

  public void setTrackRecordContent(String trackRecordContent) {
    this.trackRecordContent = trackRecordContent;
  }

  public void setTrackRecordTime(Timestamp trackRecordTime) {
    this.trackRecordTime = trackRecordTime;
  }

  public void setEnrollment(int enrollment) {
    this.enrollment = enrollment;
  }

  public void setNextRecordTime(Timestamp nextRecordTime) {
    this.nextRecordTime = nextRecordTime;
  }

  public boolean equals(final Object o) {
    if (o == this) return true;
    if (!(o instanceof TrackRecordInfo)) return false;
    final TrackRecordInfo other = (TrackRecordInfo) o;
    if (!other.canEqual((Object) this)) return false;
    if (this.getTrackRecordId() != other.getTrackRecordId()) return false;
    if (this.getStudentId() != other.getStudentId()) return false;
    final Object this$trackRecordTitle = this.getTrackRecordTitle();
    final Object other$trackRecordTitle = other.getTrackRecordTitle();
    if (this$trackRecordTitle == null ? other$trackRecordTitle != null : !this$trackRecordTitle.equals(other$trackRecordTitle))
      return false;
    final Object this$trackRecordContent = this.getTrackRecordContent();
    final Object other$trackRecordContent = other.getTrackRecordContent();
    if (this$trackRecordContent == null ? other$trackRecordContent != null : !this$trackRecordContent.equals(other$trackRecordContent))
      return false;
    final Object this$trackRecordTime = this.getTrackRecordTime();
    final Object other$trackRecordTime = other.getTrackRecordTime();
    if (this$trackRecordTime == null ? other$trackRecordTime != null : !this$trackRecordTime.equals(other$trackRecordTime))
      return false;
    if (this.getEnrollment() != other.getEnrollment()) return false;
    final Object this$nextRecordTime = this.getNextRecordTime();
    final Object other$nextRecordTime = other.getNextRecordTime();
    if (this$nextRecordTime == null ? other$nextRecordTime != null : !this$nextRecordTime.equals(other$nextRecordTime))
      return false;
    return true;
  }

  protected boolean canEqual(final Object other) {
    return other instanceof TrackRecordInfo;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    result = result * PRIME + this.getTrackRecordId();
    result = result * PRIME + this.getStudentId();
    final Object $trackRecordTitle = this.getTrackRecordTitle();
    result = result * PRIME + ($trackRecordTitle == null ? 43 : $trackRecordTitle.hashCode());
    final Object $trackRecordContent = this.getTrackRecordContent();
    result = result * PRIME + ($trackRecordContent == null ? 43 : $trackRecordContent.hashCode());
    final Object $trackRecordTime = this.getTrackRecordTime();
    result = result * PRIME + ($trackRecordTime == null ? 43 : $trackRecordTime.hashCode());
    result = result * PRIME + this.getEnrollment();
    final Object $nextRecordTime = this.getNextRecordTime();
    result = result * PRIME + ($nextRecordTime == null ? 43 : $nextRecordTime.hashCode());
    return result;
  }

  public String toString() {
    return "TrackRecordInfo(trackRecordId=" + this.getTrackRecordId() + ", studentId=" + this.getStudentId() + ", trackRecordTitle=" + this.getTrackRecordTitle() + ", trackRecordContent=" + this.getTrackRecordContent() + ", trackRecordTime=" + this.getTrackRecordTime() + ", enrollment=" + this.getEnrollment() + ", nextRecordTime=" + this.getNextRecordTime() + ")";
  }
}
