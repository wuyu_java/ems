package org.lq.recruitstudent.dao;

import org.lq.base.BaseDao;
import org.lq.recruitstudent.entity.AuditionInfo;

/**
 * @Author 冯志远
 * @create 2020-10-13 18:56
 */
public interface AuditionInfoDao extends BaseDao<AuditionInfo> {

}
