package org.lq.recruitstudent.dao;

import org.lq.base.BaseDao;
import org.lq.recruitstudent.entity.TrackRecordInfo;

/**
 * @author 一颗大李子
 * @create 2020-10-13 18:44
 */
public interface TrackRecordInfoDao extends BaseDao<TrackRecordInfo> {

}
