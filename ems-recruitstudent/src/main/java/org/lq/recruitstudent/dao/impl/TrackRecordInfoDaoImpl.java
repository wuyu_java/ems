package org.lq.recruitstudent.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.recruitstudent.dao.TrackRecordInfoDao;
import org.lq.recruitstudent.entity.TrackRecordInfo;
import org.lq.util.CastUtil;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 一颗大李子
 * @create 2020-10-13 18:49
 */
@Log4j
public class TrackRecordInfoDaoImpl implements TrackRecordInfoDao {
    /**
     * 添加
     *
     * @param trackRecordInfo
     * @return
     */
    @Override
    public int save(TrackRecordInfo trackRecordInfo) {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->save(TrackRecordInfo trackRecordInfo),开始执行");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return qr.update("insert into V_track_recond_info (" +
                    "studentId," +
                    "trackRecordTitle," +
                    "trackRecordContent," +
                    "trackRecordTime," +
                    "enrollment," +
                    "nextRecordTime) values (?,?,?,?,?,?)",
                    trackRecordInfo.getStudentId(),
                    trackRecordInfo.getTrackRecordTitle(),
                    trackRecordInfo.getTrackRecordContent(),
                    trackRecordInfo.getTrackRecordTime(),
                    trackRecordInfo.getEnrollment(),
                    trackRecordInfo.getNextRecordTime());
        } catch (SQLException throwables) {
            log.error("添加出现异常",throwables);
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->save(TrackRecordInfo trackRecordInfo),执行结束");
        return 0;
    }

    /**
     * 修改
     *
     * @param trackRecordInfo
     * @return
     */
    @Override
    public int update(TrackRecordInfo trackRecordInfo) {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->update(TrackRecordInfo trackRecordInfo),开始执行");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return qr.update("update V_track_recond_info set studentId=?," +
                            "trackRecordTitle=?," +
                            "trackRecordContent=?," +
                            "trackRecordTime=?," +
                            "enrollment=?," +
                            "nextRecordTime=? " +
                            "where trackRecordId=?",
                    trackRecordInfo.getStudentId(),
                    trackRecordInfo.getTrackRecordTitle(),
                    trackRecordInfo.getTrackRecordContent(),
                    trackRecordInfo.getTrackRecordTime(),
                    trackRecordInfo.getEnrollment(),
                    trackRecordInfo.getNextRecordTime(),
                    trackRecordInfo.getTrackRecordId());
        } catch (SQLException throwables) {
            log.error("修改出现异常",throwables);
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->update(TrackRecordInfo trackRecordInfo),执行结束");
        return 0;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->delete(int id),开始执行");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return qr.update("delete from V_track_recond_info where trackRecordId = ?",id);
        } catch (SQLException throwables) {
            log.error("删除出现异常",throwables);
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->delete(int id),执行结束");
        return 0;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public TrackRecordInfo getById(int id) {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->getById(int id),开始执行");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return qr.query("select * from V_track_recond_info where trackRecordId=?",
                    new BeanHandler<>(TrackRecordInfo.class),
                    id);
        } catch (SQLException throwables) {
            log.error("id查询出现异常",throwables);
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->getById(int id),执行结束");
        return null;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->getCount(),开始执行");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        int num = 0;
        try {
            num = qr.query("select count(1) from V_track_recond_info",
                    new ScalarHandler<Long>()).intValue();
        } catch (SQLException throwables) {
            log.error("获取总行数出现异常",throwables);
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->getCount(),执行结束");
        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<TrackRecordInfo> pageList(int startIndex, int pageSize) {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->pageList(int startIndex, int pageSize),开始执行");
        List<TrackRecordInfo> list = new ArrayList<>();
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = qr.query("select * from V_track_recond_info limit ?,?",
                    new BeanListHandler<TrackRecordInfo>(TrackRecordInfo.class),
                    startIndex,pageSize);
        } catch (SQLException throwables) {
            log.error("分页出现异常",throwables);
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->pageList(int startIndex, int pageSize),执行结束");
        return list;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->getCount(String... values),开始执行");
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        int num = 0;
        try {
            Long query = qr.query("select count(1) from V_track_recond_info a,v_student_info b " +
                            "where a.studentId=b.studentId and b.studentName like ? and a.enrollment = ?",
                    new ScalarHandler<Long>(), "%"+values[0]+"%", values[1]);
            num = query.intValue();
        } catch (SQLException throwables) {
            log.error("根据条件获取总行数出现异常",throwables);
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->getCount(String... values),执行结束");
        return num;
    }

    /**
     * 根据条件分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<TrackRecordInfo> pageByValues(int startIndex, int pageSize, String... value) {
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->pageByValues(int startIndex, int pageSize, String... value),开始执行");
        List<TrackRecordInfo> list = new ArrayList<>();
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = qr.query("select * from V_track_recond_info a,v_student_info b " +
                            "where a.studentId=b.studentId and b.studentName like ? and a.enrollment = ? limit ?,?",
                    new BeanListHandler<TrackRecordInfo>(TrackRecordInfo.class),
                    "%"+value[0]+"%", value[1], startIndex, pageSize);
        } catch (SQLException throwables) {
            log.error("根据条件分页出现异常",throwables);
            throwables.printStackTrace();
        }
        log.info("数据访问层==[TrackRecordInfoDaoImpl]->pageByValues(int startIndex, int pageSize, String... value),执行结束");
        return list;
    }
}
