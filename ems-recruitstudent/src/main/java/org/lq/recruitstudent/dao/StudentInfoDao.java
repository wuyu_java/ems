package org.lq.recruitstudent.dao;

import org.lq.base.BaseDao;
import org.lq.recruitstudent.entity.StudentInfo;

import java.util.List;

/**
 * @author 黑猫警长
 * @create 2020-10-13
 */
public interface StudentInfoDao extends BaseDao<StudentInfo> {
    /**
     * 根据条件查询总行数
     * @param name
     * @param state
     * @return
     */
    int getCount(String name,int state);

    /**
     * 根据条件模糊分页查询
     * @param startIndex 开始位置
     * @param pageSize  不长
     * @param name
     * @param state
     * @return
     */
    List<StudentInfo> pageByValues(int startIndex, int pageSize,String name,int state);
    /**
     * 根据条件查询总行数
     * @param name
     * @param state
     * @return
     */
    int getCount(String name);

    /**
     * 根据名字模糊查询
     * @param startIndex
     * @param pageSize
     * @param name
     * @return
     */
    List<StudentInfo> findByName(int startIndex, int pageSize,String name);
}
