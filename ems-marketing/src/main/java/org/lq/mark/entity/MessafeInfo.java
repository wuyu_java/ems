package org.lq.mark.entity;

import lombok.Data;
import lombok.ToString;

@Data
public class MessafeInfo {

  private int messafeId;
  private int staffId;
  private String messafeContent;
  private String messafeMan;
  private String messafePhone;
  private java.sql.Timestamp messafeTime;
  private String messafeState;


  public int getMessafeId() {
    return messafeId;
  }

  public void setMessafeId(int messafeId) {
    this.messafeId = messafeId;
  }


  public int getStaffId() {
    return staffId;
  }

  public void setStaffId(int staffId) {
    this.staffId = staffId;
  }


  public String getMessafeContent() {
    return messafeContent;
  }

  public void setMessafeContent(String messafeContent) {
    this.messafeContent = messafeContent;
  }


  public String getMessafeMan() {
    return messafeMan;
  }

  public void setMessafeMan(String messafeMan) {
    this.messafeMan = messafeMan;
  }


  public String getMessafePhone() {
    return messafePhone;
  }

  public void setMessafePhone(String messafePhone) {
    this.messafePhone = messafePhone;
  }


  public java.sql.Timestamp getMessafeTime() {
    return messafeTime;
  }

  public void setMessafeTime(java.sql.Timestamp messafeTime) {
    this.messafeTime = messafeTime;
  }


  public String getMessafeState() {
    return messafeState;
  }

  public void setMessafeState(String messafeState) {
    this.messafeState = messafeState;
  }



}
