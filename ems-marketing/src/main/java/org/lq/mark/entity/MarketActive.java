package org.lq.mark.entity;

import lombok.Data;
import lombok.extern.log4j.Log4j;

import java.sql.Timestamp;


/**
 * 营销活动实体类
 * @author 郑奥宇
 * @create 2020-10-13 19:01
 */
@Data
@Log4j
public class MarketActive {

  private int activeId;//活动编号
  private int staffId;//负责人
  private String activeName;//活动名称
  private int activeState;//活动状态
  private Timestamp activeStart;//开始时间
  private Timestamp activeEnd;//结束时间
  private int activeType;//活动类型
  private double activeCosteEmtimate;//成本预算
  private double activeCoste;//实际成本
  private int activeRefectEstimate;//预期反应
  private String activeStudent;//预期学员
  private String activeContent;//描述


}
