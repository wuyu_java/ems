package org.lq.mark.entity;

import lombok.Data;

/**
 * @anthor 孙少阳
 * @create 2020-13 18:09
 */
@Data
public class TemplateInfo {
    private int templateId;  //模板编号
    private String templateTitle;  //模板标题
    private String templateContent; //模板内容
    private String templateType; //模板类型
}
