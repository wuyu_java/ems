package org.lq.mark.entity;

import lombok.Data;
import lombok.extern.log4j.Log4j;


import java.sql.Timestamp;

/**
 * @author ming
 * @create 2020-10-13 18:46
 */
@Log4j
@Data
public class Email {
    private int emailId;//邮件编号
    private int staffId;//员工编号
    private String emailTitle;//主题
    private String emailContent;//内容
    private Timestamp emailTime;//发送时间
    private String emailMan;//接收人
    private String emailAddress;//接收地址
    private String emailState;//邮件状态

}
