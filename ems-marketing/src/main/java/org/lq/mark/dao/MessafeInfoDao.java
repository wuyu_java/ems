package org.lq.mark.dao;

import org.lq.base.BaseDao;
import org.lq.mark.entity.MessafeInfo;

/**
 * @author 张冲
 * @create 2020-10-13-18:25
 */
public interface MessafeInfoDao extends BaseDao<MessafeInfo> {
}
