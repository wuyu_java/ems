package org.lq.mark.dao.impl;
import java.sql.Timestamp;


import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.mark.dao.MessafeInfoDao;
import org.lq.mark.entity.MessafeInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 张冲
 * @create 2020-10-13-18:38
 */
@Log4j
public class MessafeInfoDaoImpl implements MessafeInfoDao {


    /**
     * 信息添加
     * @param m
     * @return
     */
    @Override
    public int save(MessafeInfo m) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("信息添加进入");
        int add=0;
        try {

            add = queryRunner.update("insert into messafe_info (staff_id, messafe_content, messafe_man, messafe_phone, messafe_time, messafe_state) values (?,?,?,?,?,?);", m.getStaffId(), m.getMessafeContent(), m.getMessafeMan(), m.getMessafePhone(), m.getMessafeTime(), m.getMessafeState());
            log.info("信息数据添加"+add);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("信息添加报错"+e);
        }

        log.info("信息添加退出");
        return add;
    }

    /**
     * 信息修改
     * @param m
     * @return
     */
    @Override
    public int update(MessafeInfo m) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("信息修改进入");
        int update = 0;
        try {
            update = queryRunner.update("update messafe_info set staff_id = ? ,messafe_content = ? ,messafe_man = ? ,messafe_phone = ? ,messafe_time = ? ,messafe_state = ?  where messafe_id = ? ", m.getStaffId(), m.getMessafeContent(), m.getMessafeMan(), m.getMessafePhone(), m.getMessafeTime(), m.getMessafeState(), m.getMessafeId());
            log.info("信息数据修改"+update);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("信息修改报错"+e);
        }

        log.info("信息修改退出");
        return update;
    }

    /**
     * 信息删除
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("信息删除进入");
        int delete = 0;
        try {
            delete = queryRunner.update("delete from messafe_info where messafe_id=?",id);
            log.info("信息数据删除"+delete);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("信息删除报错"+e);
        }

        log.info("信息删除退出");
        return delete;
    }

    /**
     * 根据id进行查询
     * @param id
     * @return
     */
    @Override
    public MessafeInfo getById(int id) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("信息根据id的查询开始");
        MessafeInfo m = null;

        try {
            m = queryRunner.query("select * from messafe where messafeId =?;", new BeanHandler<MessafeInfo>(MessafeInfo.class),id);
            log.info("信息数据根据id查询"+m);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("信息根据id的查询报错"+e);
        }

        log.info("信息根据id的查询退出");
        return m;
    }

    /**
     * 查询总行数
     * @return
     */
    @Override
    public int getCount() {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("信息查询总行数开始");
        int m = 0;
        try {
            Long query = queryRunner.query("select count(1) from messafe ;", new ScalarHandler<Long>());
            m=query.intValue();
            log.info("信息数据查询总行数"+m);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("信息查询总行数报错"+e);
        }

        log.info("信息查询总行数退出");
        return m;
    }

    /**
     * 分页查询
     * @param startIndex  开始位置
     * @param pageSize 每页查询的行数
     * @return
     */
    @Override
    public List<MessafeInfo> pageList(int startIndex, int pageSize) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("分页查询开始");
        List<MessafeInfo> list =new ArrayList<>();

        try {
            list= queryRunner.query("select * from messafe  limit ?,?;", new BeanListHandler<MessafeInfo>(MessafeInfo.class),startIndex,pageSize);
            log.info("信息数据根据id查询"+list);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("信息根据id的查询报错"+list);
        }

        log.info("信息根据id的查询退出");
        return list;
    }

    /**
     * 查询总行数
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("信息查询总行数开始");
        int m = 0;
        try {
            Long query = queryRunner.query("select count(1) from messafe where  messafePhone  like  ?;", new ScalarHandler<Long>(),"%"+values[0]+"%");
            m=query.intValue();
            log.info("信息数据查询总行数"+m);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("信息查询总行数报错"+e);
        }

        log.info("信息查询总行数退出");
        return m;
    }

    /**
     * 分页查询
     * @param startIndex 开始位置
     * @param pageSize 每行查询的个数
     * @param value
     * @return
     */
    @Override
    public List<MessafeInfo> pageByValues(int startIndex, int pageSize, String... value) {
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        log.info("分页查询开始");
        List<MessafeInfo> list =new ArrayList<>();

        try {
            list= queryRunner.query("select * from messafe where messafePhone like ? limit ?,?;", new BeanListHandler<MessafeInfo>(MessafeInfo.class),"%"+value[0]+"%",startIndex,pageSize);
            log.info("分页数据查询"+list);
        } catch (SQLException e) {
            e.printStackTrace();
            log.error("分页查询报错"+list);
        }

        log.info("分页查询退出");
        return list;
    }

    public static void main(String[] args) {

//        MessafeInfoDaoImpl mes  = new MessafeInfoDaoImpl();
//        MessafeInfo m = new MessafeInfo();
//        m.setMessafeId(23);
//        m.setStaffId(1);
//        m.setMessafeContent("力");
//        m.setMessafeMan("吃了");
//        m.setMessafePhone("15156");
//        m.setMessafeTime(new Timestamp(new java.util.Date().getTime()));
//        m.setMessafeState("正常");
//        mes.update(m);
//       mes.pageByValues(1,2,"15156");




    }


}

