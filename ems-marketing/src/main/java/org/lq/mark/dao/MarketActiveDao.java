package org.lq.mark.dao;

import org.lq.base.BaseDao;
import org.lq.mark.entity.MarketActive;
/**
 *
 * 营销活动接口
 * @author 郑奥宇
 * @create 2020-10-13 19:01
 */
public interface MarketActiveDao extends BaseDao<MarketActive> {


}
