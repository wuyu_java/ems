package org.lq.mark.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.mark.dao.MarketActiveDao;
import org.lq.mark.entity.MarketActive;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 营销活动接口实现
 * @author 郑奥宇
 * @create 2020-10-13 19:01
 */
@Log4j
public class MarketActiveDaoImpl implements MarketActiveDao {
    /**
     * 添加
     *
     * @param marketActive
     * @return
     */
    @Override
    public int save(MarketActive marketActive) {
        log.info("执行数据访问层->添加save");
        int num = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = qr.update("insert into market_active(staff_id,active_name,active_state,active_start,active_end,active_type,active_coste_emtimate" +
                    ",active_coste,active_refect_estimate,active_student,active_content)values(?,?,?,?,?,?,?,?,?,?,?)",
                    marketActive.getStaffId(),marketActive.getActiveName(),marketActive.getActiveState(),marketActive.getActiveStart(),
                    marketActive.getActiveEnd(),marketActive.getActiveType(),marketActive.getActiveCosteEmtimate(),marketActive.getActiveCoste(),
                    marketActive.getActiveRefectEstimate(),marketActive.getActiveStudent(),marketActive.getActiveContent());
        } catch (SQLException e) {
            log.error("添加营销活动出现异常"+e);
        }
        log.info("数据访问层->添加 执行结果"+num);
        log.info("结束数据访问层->添加save");
        return num;
    }

    /**
     * 修改
     *
     * @param marketActive
     * @return
     */
    @Override
    public int update(MarketActive marketActive) {
        log.info("执行数据访问层->修改update");
        int num = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = qr.update("update v_market_active set staffId=?,activeName=?,activeState=?,activeStart=?,activeEnd=?,activeType=?,activeCosteEmtimate=?" +
                            ",activeCoste=?,activeRefectEstimate=?,activeStudent=?,activeContent=? where activeId=?",
                    marketActive.getStaffId(),
                    marketActive.getActiveName(),
                    marketActive.getActiveState(),
                    marketActive.getActiveStart(),
                    marketActive.getActiveEnd(),
                    marketActive.getActiveType(),
                    marketActive.getActiveCosteEmtimate(),
                    marketActive.getActiveCoste(),
                    marketActive.getActiveRefectEstimate(),
                    marketActive.getActiveStudent(),
                    marketActive.getActiveContent(),
                    marketActive.getActiveId());
        } catch (SQLException e) {

            log.error("修改营销活动出现异常"+e);
        }
        log.info("数据访问层->修改 执行结果"+num);
        log.info("结束数据访问层->修改update");

        return num;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("执行数据访问层->通过id删除->delete");
        int num = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = qr.update("delete from market_active where active_id = ?",id);
        } catch (SQLException e) {
            log.error("删除营销活动出现异常"+e);

        }
        log.info("数据访问层->删除 执行结果"+num);
        log.info("结束数据访问层->通过id删除->delete");
        return num;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public MarketActive getById(int id) {
        log.info("执行数据访问层->通过id查询->getById");
        MarketActive m = null;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            m = qr.query("select * from v_market_active where activeId=?",new BeanHandler<>(MarketActive.class),id);
        } catch (SQLException e) {
            log.error("通过id查询营销活动出现异常"+e);
        }
        log.info("数据访问层->通过id查询 执行结果"+m);
        log.info("结束数据访问层->通过id查询->getById");
        return m;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("执行数据访问层->总行数->getCount");
        int num = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long count = qr.query("select count(1) from v_market_active",new ScalarHandler<Long>());
            num = count.intValue();
        } catch (SQLException e) {
            log.error("查询总行数出现异常"+e);
        }
        log.info("数据访问层->总行数 执行结果"+num);
        log.info("结束数据访问层->总行数->getCount");
        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<MarketActive> pageList(int startIndex, int pageSize) {
        log.info("执行数据访问层->分页查询->pageList");
        List<MarketActive> list = new ArrayList<>();
        QueryRunner qr= new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = qr.query("select * from v_market_active limit ?,?",new BeanListHandler<MarketActive>(MarketActive.class),startIndex,pageSize);
        } catch (SQLException e) {
            log.error("分页查询出现异常"+e);
        }
        log.info("数据访问层->分页查询 执行结果"+list);
        log.info("结束数据访问层->分页查询->pageList");
        return list;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        log.info("执行数据访问层->总行数->getCount(条件)");
        int num = 0;
        QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long count = qr.query("select count(1) from v_market_active where activeName like ?",new ScalarHandler<Long>(),"%"+values[0]+"%");
            num = count.intValue();
        } catch (SQLException e) {
            log.error("查询总行数(条件)出现异常"+e);

        }
        log.info("数据访问层->总行数 执行结果"+num);
        log.info("结束数据访问层->总行数->getCount(条件)");
        return num;
    }

    /**
     * 根据条件分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<MarketActive> pageByValues(int startIndex, int pageSize, String... value) {
        log.info("执行数据访问层->分页查询(条件)->pageList");
        List<MarketActive> list = new ArrayList<>();
        QueryRunner qr= new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = qr.query("select * from v_market_active where activeName like ?limit ?,?"
                    ,new BeanListHandler<MarketActive>(MarketActive.class),"%"+value[0]+"%",startIndex,pageSize);
        } catch (SQLException e) {
            log.error("分页查询(条件)出现异常"+e);
        }
        log.info("数据访问层->分页查询(条件) 执行结果"+list);
        log.info("结束数据访问层->分页查询(条件)->pageList");
        return list;
    }
}
