package org.lq.mark.dao;

import org.lq.base.BaseDao;
import org.lq.mark.entity.TemplateInfo;

/**
 *
 * 模板类接口
 * @anthor 孙少阳
 * @create 2020-13 18:18
 */
public interface TemplateInfoDao extends BaseDao<TemplateInfo> {

    /**
     * 全部属性添加，包含id
     * @param templateInfo
     * @return
     */
    int add(TemplateInfo templateInfo);

}
