package org.lq.mark.dao;

import org.lq.base.BaseDao;
import org.lq.mark.entity.Email;

/**
 * @author ming
 * @create 2020-10-13 18:52
 */
public interface EmailDao extends BaseDao<Email> {
}
