package org.lq.mark.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.lq.mark.dao.TemplateInfoDao;
import org.lq.mark.entity.TemplateInfo;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * 模板类接口实现
 * @anthor 孙少阳
 * @create 2020-13 18:22
 */
@Log4j
public class TemplateInfoDaoImpl implements TemplateInfoDao {

    @Override
    public int save(TemplateInfo templateInfo) {
        log.info("TemplateInfo添加进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.update("insert into template_info(template_title,template_content,template_type) values (?,?,?)",
                    templateInfo.getTemplateTitle(),
                    templateInfo.getTemplateContent(),
                    templateInfo.getTemplateType());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("TemplateInfo添加失败");
        }
        log.info("TemplateInfo添加成功！");
        return 0;
    }
    @Override
    public int add(TemplateInfo templateInfo) {
        log.info("TemplateInfo添加进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.update("insert into template_info(template_id,template_title,template_content,template_type) values (?,?,?,?)",
                    templateInfo.getTemplateId(),
                    templateInfo.getTemplateTitle(),
                    templateInfo.getTemplateContent(),
                    templateInfo.getTemplateType());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("TemplateInfo添加失败");
        }
        log.info("TemplateInfo添加成功！");
        return 0;
    }

    @Override
    public int update(TemplateInfo templateInfo) {
        log.info("TemplateInfo修改进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.update("update template_info set template_title=?,template_content=?,template_type=?  where template_id=?",
                    templateInfo.getTemplateTitle(),
                    templateInfo.getTemplateContent(),
                    templateInfo.getTemplateType(),
                    templateInfo.getTemplateId());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("TemplateInfo修改失败");
        }
        log.info("TemplateInfo修改成功！");
        return 0;
    }

    @Override
    public int delete(int id) {
        log.info("TemplateInfo删除进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.update("delete from template_info where template_id=?",id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("TemplateInfo删除失败");
        }
        log.info("TemplateInfo删除成功！");
        return 0;
    }

    @Override
    public TemplateInfo getById(int id) {
        log.info("TemplateInfo按ID得到进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return queryRunner.query("select * from templateinfo where templateid = ?",new BeanHandler<TemplateInfo>(TemplateInfo.class),id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.info("TemplateInfo按ID得到成失败");
        }
        log.info("TemplateInfo按ID得到成功！");
        return null;
    }

    @Override
    public int getCount() {
        log.info("TemplateInfo无条件查询总数进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long query = queryRunner.query("select count(1) from template_info ", new ScalarHandler<>());
            return query.intValue();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("TemplateInfo无条件查询总数失败");
        }
        log.info("TemplateInfo无条件查询总数成功");
        return 0;
    }

    @Override
    public List<TemplateInfo> pageList(int startIndex, int pageSize) {
        log.info("Template无条件分页进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return  queryRunner.query("select * from templateinfo limit ?,?",new BeanListHandler<>(TemplateInfo.class),
                    startIndex,
                    pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("Template无条件分页失败");
        }
        log.info("Template无条件分页成功");
        return null;
    }

    @Override
    public int getCount(String... values) {
        log.info("TemplateInfo条件查询总数进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long query = queryRunner.query("select count(1) from template_info where template_title like ? ",
                    new ScalarHandler<>(),
                    values[0]);
            return query.intValue();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("TemplateInfo条件查询总数失败");
        }
        log.info("TemplateInfo条件查询总数成功");
        return 0;
    }

    @Override
    public List<TemplateInfo> pageByValues(int startIndex, int pageSize, String... value) {
        log.info("Template条件分页进入");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            return  queryRunner.query("select * from templateinfo where templatetitle like ? limit ?,?",
                    new BeanListHandler<>(TemplateInfo.class),
                    value[0],
                    startIndex,
                    pageSize);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            log.error("Template条件分页失败");
        }
        log.info("Template条件分页成功");
        return null;
    }


}
