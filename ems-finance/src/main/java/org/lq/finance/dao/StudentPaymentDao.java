package org.lq.finance.dao;

import org.lq.base.BaseDao;
import org.lq.finance.entity.StudentPayment;

import java.util.List;

/**
 * @author 岳鑫
 * @creat 2020-10-13 18:22
 */
public interface StudentPaymentDao extends BaseDao<StudentPayment> {

    
}
