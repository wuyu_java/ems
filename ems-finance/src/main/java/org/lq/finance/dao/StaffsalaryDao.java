package org.lq.finance.dao;

import org.lq.base.BaseDao;
import org.lq.finance.entity.StaffSalary;

/**
 * @author 骆杨
 * @create 2020-10-13 下午 6:31
 */
public interface StaffsalaryDao extends BaseDao<StaffSalary> {
}
