package org.lq.finance.dao.impl;

import lombok.extern.log4j.Log4j;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.*;
import org.lq.finance.dao.StudentPaymentDao;
import org.lq.finance.entity.StudentPayment;
import org.lq.util.JDBCUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 学员缴费---->数据访问层
 * @author 岳鑫
 * @creat 2020-10-13 18:27
 */
@Log4j
public class StudentPaymentDaoImpl implements StudentPaymentDao {
    @Override
    public int save(StudentPayment studentPayment) {
        log.info("------------->save<--star------------------");
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("insert into student_payment(student_id, staff_id, payment_situation, payment_method, payment_time, discount_amount, should_amount, real_amount, debt_amount, payment_remark)" +
                    " VALUES (?,?,?,?,now(),?,?,?,?,?)",
                    studentPayment.getStudentId(),
                    studentPayment.getStaffId(),
                    studentPayment.getPaymentSituation(),
                    studentPayment.getPaymentMethod(),
                    studentPayment.getDiscountAmount(),
                    studentPayment.getShouldAmount(),
                    studentPayment.getRealAmount(),
                    studentPayment.getDebtAmount(),
                    studentPayment.getPaymentremark());
        } catch (SQLException throwables) {
            log.error("---------->save<----------"+throwables);
        }
        log.info("------->save<--end-------------"+num);
        return num;
    }

    @Override
    public int update(StudentPayment studentPayment) {
        log.info("---->update(StudentPayment studentPayment)<--star-------");
        int num = 0;
        QueryRunner queryRunner  = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("update student_payment set student_id = ?,staff_id = ?,payment_situation = ?,payment_method = ?,payment_time = ?," +
                    "discount_amount = ?,should_amount = ?,real_amount = ?,debt_amount = ?,payment_remark = ? where payment_id = ?",
                    studentPayment.getStudentId(),
                    studentPayment.getStaffId(),
                    studentPayment.getPaymentSituation(),
                    studentPayment.getPaymentMethod(),
                    studentPayment.getPaymentTime(),
                    studentPayment.getDiscountAmount(),
                    studentPayment.getShouldAmount(),
                    studentPayment.getRealAmount(),
                    studentPayment.getDebtAmount(),
                    studentPayment.getPaymentremark(),
                    studentPayment.getPaymentId());
        } catch (SQLException throwables) {
            log.error("----->update(StudentPayment studentPayment)<-----"+throwables);
        }
        log.info("------->修改学生缴费<--end--------"+num);
        return num;
    }

    @Override
    public int delete(int id) {
        log.info("-------->delete(int id)<--star-------");
        int num = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            num = queryRunner.update("delete from student_payment where payment_id = ?",id);
        } catch (SQLException throwables) {
            log.error("------->delete(int id)<----------"+throwables);
        }
        log.info("---------->delete(int id)<--end-------------");
        return num;
    }

    @Override
    public StudentPayment getById(int id) {
        log.info("------------->getById(int id)<--star------------------");
        StudentPayment studentPayment = new StudentPayment();
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            studentPayment =   queryRunner.query("select * from studentpayment where paymentId = ?",new BeanHandler<StudentPayment>(StudentPayment.class),id);
        } catch (SQLException throwables) {
            log.error("------------->getById(int id)<------"+throwables);
        }
        log.info("------------->getById(int id)<--end-------------");
        return studentPayment;
    }

    @Override
    public int getCount() {
        log.info("------------->getCount()<--star------------------");
        int count = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
         Long num = queryRunner.query("select count(1) from studentpayment",new ScalarHandler<Long>());
         count = num.intValue();
        } catch (SQLException throwables) {
            log.error("------->getCount()<---------"+throwables);
        }
        log.info("------->getCount()<--end----------"+count);
        return count;
    }

    @Override
    public List<StudentPayment> pageList(int startIndex, int pageSize) {
        log.info("--------->pageList()<--star-------------");
        List<StudentPayment> list = new ArrayList<>();
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = queryRunner.query("select * from studentpayment LIMIT ?,?",new BeanListHandler<StudentPayment>(StudentPayment.class),startIndex,pageSize);

        } catch (SQLException throwables) {
            log.error("-------->pageList()<-------"+throwables);
        }
        log.info("---------->pageList()<--end-------"+list);
        return list;
    }

    @Override
    public int getCount(String... values) {
        log.info("----->getCount(value)<--star--------");
        int count = 0;
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            Long num = queryRunner.query("select count(1) from studentpayment where paymentSituation like ?",new ScalarHandler<Long>(),"%"+values[0]+"%");
            count = num.intValue();
        } catch (SQLException throwables) {
            log.info("------------->getCount(value)<----->"+throwables);
        }
        log.info("-------->getCount(value)<--end---------");
        return count;
    }

    @Override
    public List<StudentPayment> pageByValues(int startIndex, int pageSize, String... value) {
        List<StudentPayment> list = new ArrayList<StudentPayment>();
        log.info("------->pageByValues<---star----");
        QueryRunner queryRunner = new QueryRunner(JDBCUtil.getDataSource());
        try {
            list = queryRunner.query("select * from studentpayment where paymentSituation like ? LIMIT ?,? ",new BeanListHandler<StudentPayment>(StudentPayment.class),"%"+value[0]+"%",startIndex,pageSize);

        } catch (SQLException throwables) {
            log.error("---->pageByValues<-----");
        }
        log.info("------->pageByValues<---end----");
        return list;
    }


}
