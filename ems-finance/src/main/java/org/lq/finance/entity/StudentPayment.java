package org.lq.finance.entity;


import java.sql.Timestamp;

/**
 * 学员缴费
 */
public class StudentPayment {

  private int paymentId;//减肥编号
  private int studentId;//学生编号
  private int staffId;//员工编号
  private String paymentSituation;//缴费情况
  private int paymentMethod;//缴费方式
  private Timestamp paymentTime;//缴费时间
  private double discountAmount;//折扣
  private double shouldAmount;//应收
  private double realAmount;//实收
  private double debtAmount;//欠款
  private String paymentremark;//备注

  public StudentPayment() {
  }


  public int getPaymentId() {
    return this.paymentId;
  }

  public int getStudentId() {
    return this.studentId;
  }

  public int getStaffId() {
    return this.staffId;
  }

  public String getPaymentSituation() {
    return this.paymentSituation;
  }

  public int getPaymentMethod() {
    return this.paymentMethod;
  }

  public Timestamp getPaymentTime() {
    return this.paymentTime;
  }

  public double getDiscountAmount() {
    return this.discountAmount;
  }

  public double getShouldAmount() {
    return this.shouldAmount;
  }

  public double getRealAmount() {
    return this.realAmount;
  }

  public double getDebtAmount() {
    return this.debtAmount;
  }

  public String getPaymentremark() {
    return this.paymentremark;
  }

  public void setPaymentId(int paymentId) {
    this.paymentId = paymentId;
  }

  public void setStudentId(int studentId) {
    this.studentId = studentId;
  }

  public void setStaffId(int staffId) {
    this.staffId = staffId;
  }

  public void setPaymentSituation(String paymentSituation) {
    this.paymentSituation = paymentSituation;
  }

  public void setPaymentMethod(int paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public void setPaymentTime(Timestamp paymentTime) {
    this.paymentTime = paymentTime;
  }

  public void setDiscountAmount(double discountAmount) {
    this.discountAmount = discountAmount;
  }

  public void setShouldAmount(double shouldAmount) {
    this.shouldAmount = shouldAmount;
  }

  public void setRealAmount(double realAmount) {
    this.realAmount = realAmount;
  }

  public void setDebtAmount(double debtAmount) {
    this.debtAmount = debtAmount;
  }

  public void setPaymentremark(String paymentremark) {
    this.paymentremark = paymentremark;
  }

  public boolean equals(final Object o) {
    if (o == this) return true;
    if (!(o instanceof StudentPayment)) return false;
    final StudentPayment other = (StudentPayment) o;
    if (!other.canEqual((Object) this)) return false;
    if (this.getPaymentId() != other.getPaymentId()) return false;
    if (this.getStudentId() != other.getStudentId()) return false;
    if (this.getStaffId() != other.getStaffId()) return false;
    final Object this$paymentSituation = this.getPaymentSituation();
    final Object other$paymentSituation = other.getPaymentSituation();
    if (this$paymentSituation == null ? other$paymentSituation != null : !this$paymentSituation.equals(other$paymentSituation))
      return false;
    if (this.getPaymentMethod() != other.getPaymentMethod()) return false;
    final Object this$paymentTime = this.getPaymentTime();
    final Object other$paymentTime = other.getPaymentTime();
    if (this$paymentTime == null ? other$paymentTime != null : !this$paymentTime.equals(other$paymentTime))
      return false;
    if (Double.compare(this.getDiscountAmount(), other.getDiscountAmount()) != 0) return false;
    if (Double.compare(this.getShouldAmount(), other.getShouldAmount()) != 0) return false;
    if (Double.compare(this.getRealAmount(), other.getRealAmount()) != 0) return false;
    if (Double.compare(this.getDebtAmount(), other.getDebtAmount()) != 0) return false;
    final Object this$paymentremark = this.getPaymentremark();
    final Object other$paymentremark = other.getPaymentremark();
    if (this$paymentremark == null ? other$paymentremark != null : !this$paymentremark.equals(other$paymentremark))
      return false;
    return true;
  }

  protected boolean canEqual(final Object other) {
    return other instanceof StudentPayment;
  }

  public int hashCode() {
    final int PRIME = 59;
    int result = 1;
    result = result * PRIME + this.getPaymentId();
    result = result * PRIME + this.getStudentId();
    result = result * PRIME + this.getStaffId();
    final Object $paymentSituation = this.getPaymentSituation();
    result = result * PRIME + ($paymentSituation == null ? 43 : $paymentSituation.hashCode());
    result = result * PRIME + this.getPaymentMethod();
    final Object $paymentTime = this.getPaymentTime();
    result = result * PRIME + ($paymentTime == null ? 43 : $paymentTime.hashCode());
    final long $discountAmount = Double.doubleToLongBits(this.getDiscountAmount());
    result = result * PRIME + (int) ($discountAmount >>> 32 ^ $discountAmount);
    final long $shouldAmount = Double.doubleToLongBits(this.getShouldAmount());
    result = result * PRIME + (int) ($shouldAmount >>> 32 ^ $shouldAmount);
    final long $realAmount = Double.doubleToLongBits(this.getRealAmount());
    result = result * PRIME + (int) ($realAmount >>> 32 ^ $realAmount);
    final long $debtAmount = Double.doubleToLongBits(this.getDebtAmount());
    result = result * PRIME + (int) ($debtAmount >>> 32 ^ $debtAmount);
    final Object $paymentremark = this.getPaymentremark();
    result = result * PRIME + ($paymentremark == null ? 43 : $paymentremark.hashCode());
    return result;
  }

  public String toString() {
    return "StudentPayment(paymentId=" + this.getPaymentId() + ", studentId=" + this.getStudentId() + ", staffId=" + this.getStaffId() + ", paymentSituation=" + this.getPaymentSituation() + ", paymentMethod=" + this.getPaymentMethod() + ", paymentTime=" + this.getPaymentTime() + ", discountAmount=" + this.getDiscountAmount() + ", shouldAmount=" + this.getShouldAmount() + ", realAmount=" + this.getRealAmount() + ", debtAmount=" + this.getDebtAmount() + ", paymentremark=" + this.getPaymentremark() + ")";
  }
}
