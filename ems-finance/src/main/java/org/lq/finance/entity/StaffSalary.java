package org.lq.finance.entity;

import java.sql.Timestamp;

/**员工薪水信息
 * @author 骆杨
 * @create 2020-10-13 下午 6:22
 */
public class StaffSalary {
    private int staffSalaryId;
    private int staffId;
    private int staStaffId;
    private double totalSalary;
    private double deductSalary;
    private double realSalary;
    private String isUesd;
    private Timestamp staffSalaryTime;
    private String staffRemark;

    public StaffSalary() {
    }


    public int getStaffSalaryId() {
        return this.staffSalaryId;
    }

    public int getStaffId() {
        return this.staffId;
    }

    public int getStaStaffId() {
        return this.staStaffId;
    }

    public double getTotalSalary() {
        return this.totalSalary;
    }

    public double getDeductSalary() {
        return this.deductSalary;
    }

    public double getRealSalary() {
        return this.realSalary;
    }

    public String getIsUesd() {
        return this.isUesd;
    }

    public Timestamp getStaffSalaryTime() {
        return this.staffSalaryTime;
    }

    public String getStaffRemark() {
        return this.staffRemark;
    }

    public void setStaffSalaryId(int staffSalaryId) {
        this.staffSalaryId = staffSalaryId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public void setStaStaffId(int staStaffId) {
        this.staStaffId = staStaffId;
    }

    public void setTotalSalary(double totalSalary) {
        this.totalSalary = totalSalary;
    }

    public void setDeductSalary(double deductSalary) {
        this.deductSalary = deductSalary;
    }

    public void setRealSalary(double realSalary) {
        this.realSalary = realSalary;
    }

    public void setIsUesd(String isUesd) {
        this.isUesd = isUesd;
    }

    public void setStaffSalaryTime(Timestamp staffSalaryTime) {
        this.staffSalaryTime = staffSalaryTime;
    }

    public void setStaffRemark(String staffRemark) {
        this.staffRemark = staffRemark;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof StaffSalary)) return false;
        final StaffSalary other = (StaffSalary) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.getStaffSalaryId() != other.getStaffSalaryId()) return false;
        if (this.getStaffId() != other.getStaffId()) return false;
        if (this.getStaStaffId() != other.getStaStaffId()) return false;
        if (Double.compare(this.getTotalSalary(), other.getTotalSalary()) != 0) return false;
        if (Double.compare(this.getDeductSalary(), other.getDeductSalary()) != 0) return false;
        if (Double.compare(this.getRealSalary(), other.getRealSalary()) != 0) return false;
        final Object this$isUesd = this.getIsUesd();
        final Object other$isUesd = other.getIsUesd();
        if (this$isUesd == null ? other$isUesd != null : !this$isUesd.equals(other$isUesd)) return false;
        final Object this$staffSalaryTime = this.getStaffSalaryTime();
        final Object other$staffSalaryTime = other.getStaffSalaryTime();
        if (this$staffSalaryTime == null ? other$staffSalaryTime != null : !this$staffSalaryTime.equals(other$staffSalaryTime))
            return false;
        final Object this$staffRemark = this.getStaffRemark();
        final Object other$staffRemark = other.getStaffRemark();
        if (this$staffRemark == null ? other$staffRemark != null : !this$staffRemark.equals(other$staffRemark))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof StaffSalary;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + this.getStaffSalaryId();
        result = result * PRIME + this.getStaffId();
        result = result * PRIME + this.getStaStaffId();
        final long $totalSalary = Double.doubleToLongBits(this.getTotalSalary());
        result = result * PRIME + (int) ($totalSalary >>> 32 ^ $totalSalary);
        final long $deductSalary = Double.doubleToLongBits(this.getDeductSalary());
        result = result * PRIME + (int) ($deductSalary >>> 32 ^ $deductSalary);
        final long $realSalary = Double.doubleToLongBits(this.getRealSalary());
        result = result * PRIME + (int) ($realSalary >>> 32 ^ $realSalary);
        final Object $isUesd = this.getIsUesd();
        result = result * PRIME + ($isUesd == null ? 43 : $isUesd.hashCode());
        final Object $staffSalaryTime = this.getStaffSalaryTime();
        result = result * PRIME + ($staffSalaryTime == null ? 43 : $staffSalaryTime.hashCode());
        final Object $staffRemark = this.getStaffRemark();
        result = result * PRIME + ($staffRemark == null ? 43 : $staffRemark.hashCode());
        return result;
    }

    public String toString() {
        return "StaffSalary(staffSalaryId=" + this.getStaffSalaryId() + ", staffId=" + this.getStaffId() + ", staStaffId=" + this.getStaStaffId() + ", totalSalary=" + this.getTotalSalary() + ", deductSalary=" + this.getDeductSalary() + ", realSalary=" + this.getRealSalary() + ", isUesd=" + this.getIsUesd() + ", staffSalaryTime=" + this.getStaffSalaryTime() + ", staffRemark=" + this.getStaffRemark() + ")";
    }
}
