package org.lq.ems.mark.service.impl;
import java.sql.Timestamp;

import lombok.extern.log4j.Log4j;
import org.lq.ems.mark.service.EmailService;
import org.lq.mark.dao.EmailDao;
import org.lq.mark.dao.impl.EmailDaoImpl;
import org.lq.mark.entity.Email;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author ming
 * @create 2020-10-14 8:21
 */
@Log4j
public class EmailServiceImpl implements EmailService {
    private EmailDao ed = new EmailDaoImpl();
    /**
     * t添加
     *
     * @param t
     * @return
     */
    @Override
    public boolean save(Email t) {
        log.info("进入业务层添加");
        return ed.save(t)>0;

    }

    /**
     * 修改
     *
     * @param t
     * @return
     */
    @Override
    public boolean update(Email t) {
        return ed.update(t)>0;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(int id) {
        return ed.delete(id)>0;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public Email getById(int id) {
        return ed.getById(id);
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        return ed.getCount();
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageBean<Email> pageList(int pageIndex, int pageSize) {
        PageBean<Email> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageIndex = (pageIndex -1)*pageSize;
        pageBean.setTotalRows(ed.getCount());
        List<Email> list = ed.pageList(pageIndex,pageSize);
        pageBean.setData(list);
        return pageBean;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        String[] str = new String[1];
        str[0]="";
        values=values==null?str:values;
        return ed.getCount(values);
    }

    /**
     * 根据条件分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean<Email> pageByValues(int pageIndex, int pageSize, String... value) {
        PageBean<Email> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageIndex = (pageIndex -1)*pageSize;
        String[] str = new String[1];
        str[0]="";
        value=value==null?str:value;
        pageBean.setTotalRows(ed.getCount(value));
        List<Email> list = ed.pageByValues(pageIndex,pageSize,value[0]);
        pageBean.setData(list);
        return pageBean;
    }
    public static void main(String[] args) {
        EmailService es = new EmailServiceImpl();
        System.out.println(es.pageByValues(5,5,"a"));
//        System.out.println(es.getCount(""));
    }
}
