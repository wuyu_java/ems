package org.lq.ems.mark.service;

import org.lq.mark.entity.Email;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author ming
 * @create 2020-10-14 8:18
 */
public interface EmailService {
        /**
         * t添加
         * @param t
         * @return
         */
        boolean save(Email t);

        /**
         * 修改
         * @param t
         * @return
         */
        boolean update(Email t);

        /**
         * 删除
         * @param id
         * @return
         */
        boolean delete(int id);

        /**
         * 通过ID查询
         * @param id
         * @return
         */
        Email getById(int id);

        /**
         * 总行数
         * @return
         */
        int getCount();

        /**
         * 分页查询
         * @param pageIndex
         * @param pageSize
         * @return
         */
        PageBean<Email> pageList(int pageIndex, int pageSize);

        /**
         * 根据条件查询总行数
         * @param values
         * @return
         */
        int getCount(String... values);

        /**
         * 根据条件分页查询
         * @param pageIndex
         * @param pageSize
         * @param value
         * @return
         */
        PageBean<Email> pageByValues(int pageIndex, int pageSize, String... value);



}
