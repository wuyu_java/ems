package org.lq.ems.mark.service;

import org.lq.mark.entity.MarketActive;
import org.lq.util.PageBean;

/**
 *
 * 营销活动业务层接口
 * @author 郑奥宇
 * @create 2020-10-13 23:47
 */
public interface MarketActiveService {

        /**
         * 添加
         * @param m
         * @return
         */
        boolean save(MarketActive m);

        /**
         * 修改
         * @param m
         * @return
         */
        boolean update(MarketActive m);

        /**
         * 删除
         * @param id
         * @return
         */
        boolean delete(int id);

        /**
         * 通过ID查询
         * @param id
         * @return
         */
        MarketActive getById(int id);

        /**
         * 总行数
         * @return
         */
        int getCount();

        /**
         * 分页查询
         * @param pageIndex 开始页数
         * @param pageSize 每页显示的行数
         * @return
         */
        PageBean<MarketActive> pageList(int pageIndex, int pageSize);

        /**
         * 根据条件查询总行数
         * @param values 查询条件
         * @return
         */
        int getCount(String...values);

        /**
         * 根据条件分页查询
         * @param pageIndex 开始页数
         * @param pageSize 每页显示的行数
         * @param value 查询条件
         * @return
         */
        PageBean<MarketActive> pageByValues(int pageIndex,int pageSize,String...value);

}
