package org.lq.ems.mark.service.impl;

import org.lq.ems.mark.service.MessafeInfoService;
import org.lq.mark.dao.MessafeInfoDao;
import org.lq.mark.dao.impl.MessafeInfoDaoImpl;
import org.lq.mark.entity.MessafeInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 张冲
 * @create 2020-10-13-20:55
 */
public class MessafeInfoServiceImpl implements MessafeInfoService {
    MessafeInfoDao messafeInfoDao = new MessafeInfoDaoImpl();
    /**
     * 添加
     * @param m
     * @return
     */
    @Override
    public int save(MessafeInfo m) {
        return messafeInfoDao.save(m);
    }

    /**
     * 修改
     * @param m
     * @return
     */
    @Override
    public int update(MessafeInfo m) {
        return messafeInfoDao.update(m);
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        return messafeInfoDao.delete(id);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Override
    public MessafeInfo getById(int id) {
        return messafeInfoDao.getById(id);
    }

    /**
     * 查询总行数
     * @return
     */
    @Override
    public int getCount() {
        return messafeInfoDao.getCount();
    }


    /**
     * 分页查询
     * @param pageIndex 当前页数
     * @param pageSize 每页显示的行数
     * @return
     */
    @Override
    public PageBean<MessafeInfo> pageList(int pageIndex, int pageSize) {
        PageBean<MessafeInfo> pageBean  = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount());

        List<MessafeInfo> list = messafeInfoDao.pageByValues(PageBean.countOffset(pageSize, pageIndex), pageSize);
        pageBean.setData(list);

        return pageBean;
    }

    /**
     * 查询总行数
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        return messafeInfoDao.getCount(values);
    }

    /**
     * 分页查询
     * @param pageIndex 当前页数
     * @param pageSize 每页显示的行数
     * @param value
     * @return
     */
    @Override
    public PageBean<MessafeInfo> pageByValues(int pageIndex, int pageSize, String... value) {
        PageBean<MessafeInfo> pageBean  = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(value));

        List<MessafeInfo> list = messafeInfoDao.pageByValues(PageBean.countOffset(pageSize, pageIndex), pageSize, value);
        pageBean.setData(list);

        return pageBean;
    }
}
