package org.lq.ems.mark.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.ems.mark.service.MarketActiveService;
import org.lq.mark.dao.MarketActiveDao;
import org.lq.mark.dao.impl.MarketActiveDaoImpl;
import org.lq.mark.entity.MarketActive;
import org.lq.util.PageBean;

/**
 * 营销活动业务层实现
 * @author 郑奥宇
 * @create 2020-10-13 23:59
 */
@Log4j
public class MarketActiveServiceImpl implements MarketActiveService {

    MarketActiveDao mad = new MarketActiveDaoImpl();
    /**
     * 添加
     * @param m
     * @return
     */
    @Override
    public boolean save(MarketActive m) {
        log.info("进入业务层->添加");
        boolean bool = mad.save(m)>0;
        log.info("业务层结束 ->添加结果"+bool);
        return bool;
    }

    /**
     * 修改
     *
     * @param m
     * @return
     */
    @Override
    public boolean update(MarketActive m) {
        log.info("进入业务层->修改");
        boolean bool = mad.update(m)>0;
        log.info("业务层结束 ->修改结果"+bool);
        return bool;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(int id) {
        log.info("进入业务层->删除");
        boolean bool = mad.delete(id)>0;
        log.info("业务层结束 ->删除结果"+bool);
        return bool;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public MarketActive getById(int id) {
        log.info("进入业务层->通过ID查询");
        MarketActive m = mad.getById(id);
        log.info("业务层结束 ->通过ID查询结果"+m);
        return m;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("进入业务层->总行数");
        int num = mad.getCount();
        log.info("业务层结束 ->总行数"+num);
        return num;
    }

    /**
     * 分页查询
     *
     * @param pageIndex 开始页数
     * @param pageSize  每页显示的行数
     * @return
     */
    @Override
    public PageBean<MarketActive> pageList(int pageIndex, int pageSize) {
        log.info("进入业务层->分页查询");
        PageBean<MarketActive> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount());
        int startIndex = (pageIndex-1)*pageSize;
        pageBean.setData(mad.pageByValues(startIndex,pageSize));
        log.info("结束业务层->分页查询结果"+pageBean);

        return pageBean;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values 查询条件
     * @return
     */
    @Override
    public int getCount(String... values) {
        String [] str = {""};
        values = values==null? str : values;
        log.info("进入业务层->总页数(条件)");
        int num = mad.getCount(values);
        log.info("结束业务层->总页数(条件)结果"+num);
        return num;
    }

    /**
     * 根据条件分页查询
     *
     * @param pageIndex 开始页数
     * @param pageSize  每页显示的行数
     * @param value     查询条件
     * @return
     */
    @Override
    public PageBean<MarketActive> pageByValues(int pageIndex, int pageSize, String... value) {
        String[] str = {""};
        value  = value == null ? str : value;
        log.info("进入业务层->分页查询(条件)");

        PageBean<MarketActive> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(value));
        int startIndex = (pageIndex-1)*pageSize;
        pageBean.setData(mad.pageByValues(startIndex,pageSize,value));
        log.info("结束业务层->分页查询结果(条件)"+pageBean);

        return pageBean;
    }
}
