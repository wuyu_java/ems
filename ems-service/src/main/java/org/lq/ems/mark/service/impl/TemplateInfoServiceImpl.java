package org.lq.ems.mark.service.impl;

import org.lq.ems.mark.service.TemplateInfoService;
import org.lq.mark.dao.TemplateInfoDao;
import org.lq.mark.dao.impl.TemplateInfoDaoImpl;
import org.lq.mark.entity.TemplateInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 *模板业务接口实现类
 * @anthor 孙少阳
 * @create 2020-13 19:08
 */
public class TemplateInfoServiceImpl implements TemplateInfoService {
    private TemplateInfoDao tid = new TemplateInfoDaoImpl();
    @Override
    public boolean save(TemplateInfo templateInfo) {
        return tid.save(templateInfo)>0;
    }

    @Override
    public boolean update(TemplateInfo templateInfo) {
        return tid.update(templateInfo)>0;
    }

    @Override
    public boolean delete(int id) {
        return tid.delete(id)>0;
    }

    @Override
    public TemplateInfo getById(int id) {
        return tid.getById(id);
    }

    @Override
    public int getCount() {
        return tid.getCount();
    }

    @Override
    public List<TemplateInfo> pageList(int startIndex, int pageSize) {
        return tid.pageList(startIndex,pageSize);
    }

    @Override
    public int getCount(String values) {
        String str = "%"+values+"%";
        return tid.getCount(str);
    }

    @Override
    public List<TemplateInfo> pageByValues(int startIndex, int pageSize, String value) {
        String str = "%"+value+"%";
        return tid.pageByValues(startIndex,pageSize,str);
    }

    /**
     * 分页对象
     * @param pageindex 当前页
     * @param pageSize 每页行数
     * @param name 查询条件
     * @return
     */
    @Override
    public PageBean<TemplateInfo> getPageBean(int pageindex, int pageSize, String name) {
        PageBean<TemplateInfo> pageBean = new PageBean();
        pageBean.setPageSize(pageSize);
        pageBean.setCurrentPage(pageindex);
        pageBean.setTotalRows(this.getCount(name));
        int startPage = (pageindex-1)*pageSize;
        pageBean.setData(this.pageByValues(startPage,pageSize,name));
        return pageBean;
    }

    /**
     * 添加全部属性(包含id)  避免编号的重复
     * @param templateInfo
     * @return 存在/成功/失败
     */
    public String addTemplateInfo(TemplateInfo templateInfo){

        if(this.getById(templateInfo.getTemplateId())==null){
            if(tid.add(templateInfo)>0){
                return "添加成功";
            }else{
                return "添加失败";
            }
        }else{
            return "已经存在";
        }

    }


}
