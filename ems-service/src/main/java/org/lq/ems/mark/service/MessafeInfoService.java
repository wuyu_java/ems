package org.lq.ems.mark.service;

import org.lq.mark.entity.MessafeInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 张冲
 * @create 2020-10-13-20:24
 */
public interface MessafeInfoService{
    /**
     * t添加
     * @param m
     * @return
     */
    int save(MessafeInfo m);

    /**
     * 修改
     * @param m
     * @return
     */
    int update(MessafeInfo m);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    MessafeInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PageBean<MessafeInfo> pageList(int pageIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String... values);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<MessafeInfo> pageByValues(int pageIndex, int pageSize, String... value);
}
