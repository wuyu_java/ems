package org.lq.ems.mark.service;

import org.lq.mark.entity.TemplateInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * 模板业务层接口
 * @anthor 孙少阳
 * @create 2020-13 19:02
 */
public interface TemplateInfoService<BasePage> {

    /**
     * t添加
     * @param
     * @return
     */
    boolean save(TemplateInfo templateInfo);

    /**
     * 修改
     * @param
     * @return
     */
    boolean update(TemplateInfo templateInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    TemplateInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<TemplateInfo> pageList(int startIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String values);

    /**
     * 根据条件分页查询
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    List<TemplateInfo> pageByValues(int startIndex, int pageSize, String value);

    /**
     * 分页对象
     * @param pageindex 当前页
     * @param pageSize 每页行数
     * @param name 查询条件
     * @return
     */
    PageBean<TemplateInfo>  getPageBean(int pageindex, int pageSize, String name);
}
