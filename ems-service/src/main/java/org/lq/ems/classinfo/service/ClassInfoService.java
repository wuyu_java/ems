package org.lq.ems.classinfo.service;

import org.lq.classinfo.entity.ClassInfo;

import java.util.List;

/**
 * @author wynn
 * @create2020-10-13 19:58
 */
public interface ClassInfoService {
    /**
     * 添加
     * @param classInfo
     * @return
     */
    boolean save(ClassInfo classInfo);

    /**
     * 修改
     * @param classInfo
     * @return
     */
    boolean update(ClassInfo classInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    ClassInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    boolean getCount();

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<ClassInfo> pageList(int startIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    boolean getCount(String...values);

    /**
     * 根据条件分页查询
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    List<ClassInfo> pageByValues(int startIndex,int pageSize,String...value);

}
