package org.lq.ems.classinfo.service;

import org.lq.classinfo.entity.ClassroomInfo;
import org.lq.util.PageBean;


/**
 * 教室业务层接口
 * @author 邹倩倩
 * @create 2020 10 13 2020-10-13
 */
public interface ClassroominfoService {
    /**
     * t添加
     * @param c
     * @return
     */
    boolean save(ClassroomInfo c);

    /**
     * 修改
     * @param c
     * @return
     */
    boolean update(ClassroomInfo c);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    ClassroomInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param currentPage
     * @param pageSize
     * @return
     */
    PageBean pageList(int currentPage, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String...values);

    /**
     * 根据条件分页查询
     * @param currentPage
     * @param pageSize
     * @param value
     * @return
     */
    PageBean pageByValues(int currentPage, int pageSize, String...value);
}
