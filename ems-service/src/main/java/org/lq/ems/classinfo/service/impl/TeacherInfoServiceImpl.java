package org.lq.ems.classinfo.service.impl;

import org.lq.classinfo.dao.TeacherInfoDao;
import org.lq.classinfo.dao.impl.TeacherInfoDaoImpl;
import org.lq.classinfo.entity.TeacherInfo;
import org.lq.ems.classinfo.service.TeacherInfoService;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 张三
 * @create 2020-10-13 20:18
 */
public class TeacherInfoServiceImpl implements TeacherInfoService {

    private TeacherInfoDao dao = new TeacherInfoDaoImpl();

    /**
     * 添加教师信息
     * @param t
     * @return
     */
    @Override
    public boolean save(TeacherInfo t) {
        return dao.save(t)>0;
    }

    /**
     * 修改教师信息
     * @param t
     * @return
     */
    @Override
    public boolean update(TeacherInfo t) {
        return dao.update(t)>0;
    }

    /**
     * 根据ID删除教师信息
     * @param id
     * @return
     */
    @Override
    public boolean delete(int id) {
        return dao.delete(id)>0;
    }

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    @Override
    public TeacherInfo getById(int id) {
        return dao.getById(id);
    }

    /**
     * 总行数
     * @return
     */
    @Override
    public int getCount() {
        return dao.getCount();
    }

    /**
     * 分页查询
     * @param pageindex 当前页
     * @param pageSize
     * @return
     */
    @Override
    public List<TeacherInfo> pageList(int pageindex, int pageSize) {
        int startIndex = PageBean.countOffset(pageindex,pageSize);
        return dao.pageList(startIndex, pageSize);
    }

    /**
     * 根据姓名查询的教师信息总数
     * @param name
     * @return
     */
    @Override
    public int getCountByName(String name) {
        name = name == null ? "" : name;
        return dao.getCountByName(name);
    }

    /**
     * 根据性查询的教师信息总数
     * @param sex
     * @return
     */
    @Override
    public int getCountBySex(String sex) {
        sex = sex == null ? "" : sex;
        return dao.getCountBySex(sex);
    }

    /**
     * 根据姓名查询教师信息
     * @param pageindex 页面索引
     * @param pageSize 页面大小
     * @param name 姓名关键字
     * @return
     */
    @Override
    public List<TeacherInfo> pageByName(int pageindex, int pageSize, String name) {
        name = name == null ? "" : name;
        int startIndex = PageBean.countOffset(pageindex,pageSize);
        return dao.pageByName(startIndex,pageSize,name);
    }

    /**
     * 根据性别查询教师信息
     * @param pageindex 页面索引
     * @param pageSize 页面大小
     * @param sex 性别关键字
     * @return
     */
    @Override
    public List<TeacherInfo> pageBySex(int pageindex, int pageSize, String sex) {
        sex = sex == null ? "" : sex;
        int startIndex = PageBean.countOffset(pageindex,pageSize);
        return dao.pageBySex(startIndex,pageSize,sex);
    }

}
