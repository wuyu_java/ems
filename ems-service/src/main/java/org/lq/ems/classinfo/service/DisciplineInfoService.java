package org.lq.ems.classinfo.service;

import org.lq.classinfo.entity.DisciplineInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * 学科信息业务接口
 * @author Sky
 * @create 2020-10-13 18:58
 */
public interface DisciplineInfoService {

    /**
     * t添加
     * @param disciplineInfo
     * @return
     */
    boolean saveDisciplineInfo(DisciplineInfo disciplineInfo);

    /**
     * 修改
     * @param disciplineInfo
     * @return
     */
    boolean updateDisciplineInfo(DisciplineInfo disciplineInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean deleteDisciplineInfo(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    DisciplineInfo getDisciplineInfoById(int id);

    /**
     * 总行数
     * @return
     */
    int getDisciplineInfoCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PageBean<DisciplineInfo> pageDisciplineInfoList(int pageIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getDisciplineInfoCount(String...values);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<DisciplineInfo> pageDisciplineInfoByValues(int pageIndex,int pageSize,String...value);

}
