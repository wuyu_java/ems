package org.lq.ems.classinfo.service;

import org.lq.classinfo.entity.TeacherInfo;

import java.util.List;

/**
 * @author 张三
 * @create 2020-10-13 20:13
 */
public interface TeacherInfoService {

    /**
     * 添加
     * @param t
     * @return
     */
    boolean save(TeacherInfo t);

    /**
     * 修改
     * @param t
     * @return
     */
    boolean update(TeacherInfo t);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    TeacherInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageindex 当前页
     * @param pageSize
     * @return
     */
    List<TeacherInfo> pageList(int pageindex,int pageSize);

    /**
     * 根据姓名查询的教师信息总数
     * @param name
     * @return
     */
    int getCountByName(String name);

    /**
     * 根据性查询的教师信息总数
     * @param sex
     * @return
     */
    int getCountBySex(String sex);

    /**
     * 根据姓名查询教师信息
     * @param pageindex 页面索引
     * @param pageSize 页面大小
     * @param name 姓名关键字
     * @return
     */
    List<TeacherInfo> pageByName(int pageindex, int pageSize, String name);

    /**
     * 根据性别查询教师信息
     * @param pageindex 页面索引
     * @param pageSize 页面大小
     * @param sex 性别关键字
     * @return
     */
    List<TeacherInfo> pageBySex(int pageindex, int pageSize, String sex);
}
