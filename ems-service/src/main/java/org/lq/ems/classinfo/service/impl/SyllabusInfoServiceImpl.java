package org.lq.ems.classinfo.service.impl;

import org.lq.classinfo.dao.SyllabusInfoDao;
import org.lq.classinfo.dao.impl.SyllabusInfoDaoImpl;
import org.lq.classinfo.entity.SyllabusInfo;
import org.lq.ems.classinfo.service.SyllabusInfoService;
import org.lq.util.PageBean;

import java.util.List;


/**
 * 课程表业务处理层
 * @author 刘明昕
 * @created 2020-10-13 19:06
 */
public class SyllabusInfoServiceImpl implements SyllabusInfoService {
    private SyllabusInfoDao syllabusInfoDao = new SyllabusInfoDaoImpl();

    /**
     * 添加课程
     * @param syllabusInfo
     * @return
     */
    @Override
    public int save(SyllabusInfo syllabusInfo) {
        return syllabusInfoDao.save(syllabusInfo);
    }

    /**
     * 修改课程
     * @param syllabusInfo
     * @return
     */
    @Override
    public int update(SyllabusInfo syllabusInfo) {
        return syllabusInfoDao.update(syllabusInfo);
    }

    /**
     * 删除课程
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        return syllabusInfoDao.delete(id);
    }

    /**
     * 根据id查询课程
     * @param id
     * @return
     */
    @Override
    public SyllabusInfo getById(int id) {
        return syllabusInfoDao.getById(id);
    }

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageBean<SyllabusInfo> pageList(int pageIndex, int pageSize) {
        PageBean<SyllabusInfo> pageBean = new PageBean<>();
        int totalRows = syllabusInfoDao.getCount();
        pageBean.setTotalRows(totalRows);
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        int startIndex = PageBean.countOffset(pageSize,pageIndex);
        List<SyllabusInfo> data = syllabusInfoDao.pageList(startIndex, pageSize);
        pageBean.setData(data);
        return pageBean;
    }

    /**
     * 分页模糊查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean<SyllabusInfo> pageByValues(int pageIndex, int pageSize, String... value) {
        PageBean<SyllabusInfo> pageBean = new PageBean<>();
        int totalRows = syllabusInfoDao.getCount(value);
        pageBean.setTotalRows(totalRows);
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        int startIndex = PageBean.countOffset(pageSize,pageIndex);
        List<SyllabusInfo> data = syllabusInfoDao.pageByValues(startIndex, pageSize,value);
        pageBean.setData(data);
        return pageBean;
    }
}
