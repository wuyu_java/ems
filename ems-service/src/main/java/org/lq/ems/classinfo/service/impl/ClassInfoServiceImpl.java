package org.lq.ems.classinfo.service.impl;

import org.lq.classinfo.dao.ClassInfoDao;
import org.lq.classinfo.dao.impl.ClassInfoDaoImpl;
import org.lq.classinfo.entity.ClassInfo;
import org.lq.ems.classinfo.service.ClassInfoService;

import java.util.List;

/**
 * @author wynn
 * @create2020-10-13 20:04
 */
public class ClassInfoServiceImpl implements ClassInfoService {
    ClassInfoDao classInfoDao = new ClassInfoDaoImpl();
    @Override
    public boolean save(ClassInfo classInfo) {
        return classInfoDao.save(classInfo)>0;
    }

    @Override
    public boolean update(ClassInfo classInfo) {
        return classInfoDao.update(classInfo)>0;
    }

    @Override
    public boolean delete(int id) {
        return classInfoDao.delete(id)>0;
    }

    @Override
    public ClassInfo getById(int id) {
        return classInfoDao.getById(id);
    }

    @Override
    public boolean getCount() {
        return classInfoDao.getCount()>0;
    }

    @Override
    public List<ClassInfo> pageList(int startIndex, int pageSize) {
        return classInfoDao.pageList(startIndex,pageSize);
    }

    @Override
    public boolean getCount(String... values) {
        return classInfoDao.getCount(values)>0;
    }

    @Override
    public List<ClassInfo> pageByValues(int startIndex, int pageSize, String... value) {
        return classInfoDao.pageByValues(startIndex,pageSize,value);
    }
}
