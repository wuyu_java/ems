package org.lq.ems.classinfo.service;

import org.lq.classinfo.entity.SyllabusInfo;
import org.lq.util.PageBean;


/**
 * @author 刘明昕
 * @created 2020-10-13 19:00
 */
public interface SyllabusInfoService {
    /**
     * t添加
     * @param t
     * @return
     */
    int save(SyllabusInfo syllabusInfo);

    /**
     * 修改
     * @param t
     * @return
     */
    int update(SyllabusInfo syllabusInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    SyllabusInfo getById(int id);

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PageBean<SyllabusInfo> pageList(int pageIndex, int pageSize);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<SyllabusInfo> pageByValues(int pageIndex,int pageSize,String...value);


}
