package org.lq.ems.classinfo.service.impl;

import org.lq.classinfo.dao.ClassroominfoDao;
import org.lq.classinfo.dao.impl.ClassroominfoDaoImpl;
import org.lq.classinfo.entity.ClassroomInfo;
import org.lq.ems.classinfo.service.ClassroominfoService;
import org.lq.util.PageBean;


/**
 * 教室业务层接口实现类
 * @author 邹倩倩
 * @create 2020 10 13 2020-10-13
 */
public class ClassinforoomServiceImpl implements ClassroominfoService {
//   声明教书数据访问层实现类
    ClassroominfoDao cd = new ClassroominfoDaoImpl();
    /**
     * t添加
     *
     * @param c
     * @return
     */
    @Override
    public boolean save(ClassroomInfo c) {
        return cd.save(c)>0;
    }

    /**
     * 修改
     *
     * @param c
     * @return
     */
    @Override
    public boolean update(ClassroomInfo c) {
        return cd.update(c)>0;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(int id) {
        return cd.delete(id)>0;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public ClassroomInfo getById(int id) {
        return cd.getById(id);
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        return cd.getCount();
    }

    /**
     * 分页查询
     *
     * @param currentPage
     * @param pageSize
     * @return
     */
    @Override
    public PageBean pageList(int currentPage, int pageSize) {
        //创建一个分页对象
        PageBean pageBean = new PageBean();
        pageBean.setCurrentPage(currentPage);
        pageBean.setPageSize(pageSize);
//        pageBean.setTotalRows(getCount());
        //设置分页对象的数据，数据的起始位置是dao中获取的当前页的教室集合
        pageBean.setData(cd.pageList(pageBean.countOffset(pageSize, currentPage),pageSize));
        return pageBean;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        return cd.getCount(values);
    }

    /**
     * 根据条件分页查询
     *
     * @param currentPage
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean pageByValues(int currentPage, int pageSize, String... value) {
        //创建一个分页对象
        PageBean pageBean = new PageBean();
        pageBean.setCurrentPage(currentPage);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(value[0]));
        //设置分页对象的数据，数据的起始位置是dao中获取的当前页的教室集合
        pageBean.setData(cd.pageByValues(pageBean.countOffset(pageSize, currentPage),pageSize,value[0]));
        return pageBean;
    }
}
