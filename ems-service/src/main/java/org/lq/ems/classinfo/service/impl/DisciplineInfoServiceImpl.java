package org.lq.ems.classinfo.service.impl;

import org.lq.classinfo.dao.DisciplineInfoDao;
import org.lq.classinfo.dao.impl.DisciplineInfoDaoImpl;
import org.lq.classinfo.entity.DisciplineInfo;
import org.lq.ems.classinfo.service.DisciplineInfoService;
import org.lq.util.PageBean;

import java.util.List;

/**
 * 学科信息业务接口实现类
 * @author Sky
 * @create 2020-10-13 19:04
 */
public class DisciplineInfoServiceImpl implements DisciplineInfoService {

    private DisciplineInfoDao infoDao = new DisciplineInfoDaoImpl();

    /**
     * 学科信息添加
     *
     * @param disciplineInfo
     * @return
     */
    @Override
    public boolean saveDisciplineInfo(DisciplineInfo disciplineInfo) {
        return infoDao.save(disciplineInfo)>0;
    }

    /**
     * 学科信息修改
     *
     * @param disciplineInfo
     * @return
     */
    @Override
    public boolean updateDisciplineInfo(DisciplineInfo disciplineInfo) {
        return infoDao.update(disciplineInfo)>0;
    }

    /**
     * 学科信息删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteDisciplineInfo(int id) {
        return infoDao.delete(id)>0;
    }

    /**
     * 通过ID查询学科信息
     *
     * @param id
     * @return
     */
    @Override
    public DisciplineInfo getDisciplineInfoById(int id) {
        return infoDao.getById(id);
    }

    /**
     * 学科信息总行数
     *
     * @return
     */
    @Override
    public int getDisciplineInfoCount() {
        return infoDao.getCount();
    }

    /**
     * 分页查询学科信息
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageBean<DisciplineInfo> pageDisciplineInfoList(int pageIndex, int pageSize) {

        PageBean<DisciplineInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(infoDao.getCount());
        List<DisciplineInfo> list = infoDao.pageList(PageBean.countOffset(pageSize, pageIndex), pageSize);
        pageBean.setData(list);
        return pageBean;
    }

    /**
     * 根据条件查询学科信息总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getDisciplineInfoCount(String... values) {
        return infoDao.getCount(values);
    }

    /**
     * 根据条件分页查询学科信息
     *
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean<DisciplineInfo> pageDisciplineInfoByValues(int pageIndex, int pageSize, String... value) {
        PageBean<DisciplineInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(infoDao.getCount(value ));
        List<DisciplineInfo> list = infoDao.pageList(PageBean.countOffset(pageSize, pageIndex), pageSize);
        pageBean.setData(list);
        return pageBean;
    }
}
