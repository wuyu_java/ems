package org.lq.ems.recruitstudent.service;

import org.lq.recruitstudent.entity.StudentInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 黑猫警长
 * @create 2020-10-14
 */
public interface StudentInfoService {
    /**
     * t添加
     * @param studentInfo
     * @return
     */
    int save(StudentInfo studentInfo);

    /**
     * 修改
     * @param studentInfo
     * @return
     */
    int update(StudentInfo studentInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    StudentInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PageBean<StudentInfo> pageList(int pageIndex, int pageSize);
    /**
     * 根据条件查询总行数
     * @param name
     * @param state
     * @return
     */
    int getCount(String name,int state);

    /**
     * 根据条件模糊分页查询
     * @param pageIndex
     * @param pageSize
     * @param name
     * @param state
     * @return
     */
    PageBean<StudentInfo> pageByValues(int pageIndex, int pageSize, String name, int state);
    /**
     * 根据名字模糊查询
     * @param pageIndex
     * @param pageSize
     * @param name
     * @return
     */
    PageBean<StudentInfo> findByName(int pageIndex, int pageSize, String name);
}
