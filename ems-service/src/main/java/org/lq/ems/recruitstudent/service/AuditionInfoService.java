package org.lq.ems.recruitstudent.service;

import org.lq.recruitstudent.entity.AuditionInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @Author 冯志远
 * @create 2020-10-13 19:04
 */
public interface AuditionInfoService {
    /**
     * 添加
     * @param auditionInfo
     * @return
     */
    boolean saveAuditionInfo(AuditionInfo auditionInfo);

    /**
     * 修改
     * @param auditionInfo
     * @return
     */
    boolean updateAuditionInfo(AuditionInfo auditionInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean deleteAuditionInfoById(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    AuditionInfo getAuditionInfoById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<AuditionInfo> pageList(int startIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String...values);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<AuditionInfo> pageByValues(int pageIndex, int pageSize, String...value);


}
