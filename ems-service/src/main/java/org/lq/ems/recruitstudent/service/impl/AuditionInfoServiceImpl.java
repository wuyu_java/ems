package org.lq.ems.recruitstudent.service.impl;

import org.lq.ems.recruitstudent.service.AuditionInfoService;
import org.lq.recruitstudent.dao.AuditionInfoDao;
import org.lq.recruitstudent.dao.impl.AuditionInfoDaoImpl;
import org.lq.recruitstudent.entity.AuditionInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @Author 冯志远
 * @create 2020-10-13 20:29
 */
public class AuditionInfoServiceImpl implements AuditionInfoService {
    private AuditionInfoDao auditionInfoDao = new AuditionInfoDaoImpl();
    @Override
    public boolean saveAuditionInfo(AuditionInfo auditionInfo) {
        return auditionInfoDao.save(auditionInfo)>0;
    }

    @Override
    public boolean updateAuditionInfo(AuditionInfo auditionInfo) {
        return auditionInfoDao.update(auditionInfo)>0;
    }

    @Override
    public boolean deleteAuditionInfoById(int id) {
        return auditionInfoDao.delete(id)>0;
    }

    @Override
    public AuditionInfo getAuditionInfoById(int id) {
        return auditionInfoDao.getById(id);
    }

    @Override
    public int getCount() {
        return auditionInfoDao.getCount();
    }

    @Override
    public List<AuditionInfo> pageList(int startIndex, int pageSize) {
        return auditionInfoDao.pageList(startIndex,pageSize);
    }

    @Override
    public int getCount(String... values) {
        return auditionInfoDao.getCount(values);
    }

    @Override
    public PageBean<AuditionInfo> pageByValues(int pageIndex, int pageSize, String... value) {
        PageBean<AuditionInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(value));
        int startIndex = (pageIndex-1)*pageSize;
        pageBean.setData(auditionInfoDao.pageByValues(startIndex,pageSize,value));
        return pageBean;
    }





}
