package org.lq.ems.recruitstudent.service.impl;

import org.lq.ems.recruitstudent.service.TrackRecodService;
import org.lq.recruitstudent.dao.TrackRecordInfoDao;
import org.lq.recruitstudent.dao.impl.TrackRecordInfoDaoImpl;
import org.lq.recruitstudent.entity.TrackRecordInfo;
import org.lq.util.CastUtil;
import org.lq.util.PageBean;

/**
 * @author 一颗大李子
 * @create 2020-10-13 20:42
 */
public class TrackRecodServiceImpl implements TrackRecodService {

    TrackRecordInfoDao trackRecordInfoDao = new TrackRecordInfoDaoImpl();
    /**
     * 添加
     *
     * @param trackRecordInfo
     * @return
     */
    @Override
    public int save(TrackRecordInfo trackRecordInfo) {
        return trackRecordInfoDao.save(trackRecordInfo);
    }

    /**
     * 修改
     *
     * @param trackRecordInfo
     * @return
     */
    @Override
    public int update(TrackRecordInfo trackRecordInfo) {
        return trackRecordInfoDao.update(trackRecordInfo);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        return trackRecordInfoDao.delete(id);
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public TrackRecordInfo getById(int id) {
        return trackRecordInfoDao.getById(id);
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        return trackRecordInfoDao.getCount();
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageBean<TrackRecordInfo> pageList(int pageIndex, int pageSize) {
        PageBean<TrackRecordInfo> pageBean = new PageBean<>();
        pageBean.setPageSize(pageSize);
        pageBean.setCurrentPage(pageSize);
        pageBean.setTotalRows(getCount());
        int startPage = (pageIndex - 1)*pageSize;
        pageBean.setData(trackRecordInfoDao.pageList(startPage,pageSize));
        return pageBean;
    }

    /**
     * 根据条件查询总行数
     *
     * @param name
     * @param enrollment
     * @return
     */
    @Override
    public int getCount(String name, int enrollment) {
        name = name == null?"":name;
        return trackRecordInfoDao.getCount(name, CastUtil.castString(enrollment));
    }

    /**
     * 根据条件分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param name
     * @param enrollment
     * @return
     */
    @Override
    public PageBean<TrackRecordInfo> pageByValues(int pageIndex, int pageSize, String name, int enrollment) {
        name = name == null ? "" : name;
        PageBean<TrackRecordInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(name,enrollment));
        int startIndex = (pageIndex - 1)*pageSize;
        pageBean.setData(trackRecordInfoDao.pageByValues(startIndex,pageSize,name,CastUtil.castString(enrollment)));
        return pageBean;
    }


}
