package org.lq.ems.recruitstudent.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.ems.recruitstudent.service.StudentInfoService;
import org.lq.recruitstudent.dao.StudentInfoDao;
import org.lq.recruitstudent.dao.impl.StudentInfoDaoImpl;
import org.lq.recruitstudent.entity.StudentInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 黑猫警长
 * @create 2020-10-14
 */
@Log4j
public class StudentInfoServiceImpl implements StudentInfoService {
    private StudentInfoDao sid = new StudentInfoDaoImpl();
    /**
     * t添加
     *
     * @param studentInfo
     * @return
     */
    @Override
    public int save(StudentInfo studentInfo) {
        log.info("业务层==[StudentInfoServiceImpl]->save(StudentInfo studentInfo)返回结果"+sid.save(studentInfo));
        return sid.save(studentInfo);
    }

    /**
     * 修改
     *
     * @param studentInfo
     * @return
     */
    @Override
    public int update(StudentInfo studentInfo) {
        log.info("业务层==[StudentInfoServiceImpl]->update(StudentInfo studentInfo)");
        return sid.update(studentInfo);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("业务层==[StudentInfoServiceImpl]->delete(int id)");
        return sid.delete(id);
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public StudentInfo getById(int id) {
        log.info("业务层==[StudentInfoServiceImpl]->delete(int id)");
        return sid.getById(id);
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("业务层==[StudentInfoServiceImpl]->getCount()");
        return sid.getCount();
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageBean<StudentInfo> pageList(int pageIndex, int pageSize) {
        log.info("业务层==[StudentInfoServiceImpl]->pageList(int pageIndex, int pageSize),开始执行");
        PageBean<StudentInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(sid.getCount());
        List<StudentInfo> list = sid.pageList(PageBean.countOffset(pageSize,pageIndex),pageSize);
        pageBean.setData(list);
        log.info("返回结果:"+pageBean);
        log.info("业务层==[StudentInfoServiceImpl]->pageList(int pageIndex, int pageSize),执行结束");
        return pageBean;
//        PageBean<StudentInfo> pageBean = new PageBean<>();
//        pageBean.setCurrentPage(pageIndex);
//        pageBean.setPageSize(pageSize);
//        pageBean.setTotalRows(getCount());
//        int startIndex= (pageIndex-1) * pageSize;
//        pageBean.setData(sid.pageList(startIndex,pageSize));
//        return pageBean;
    }

    /**
     * 根据条件查询总行数
     *
     * @param name
     * @param state
     * @return
     */
    @Override
    public int getCount(String name, int state) {
        log.info("业务层==[StudentInfoServiceImpl]->getCount(String name, int state)");
        return sid.getCount(name,state);
    }

    /**
     * 根据条件模糊分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param name
     * @param state
     * @return
     */
    @Override
    public PageBean<StudentInfo> pageByValues(int pageIndex, int pageSize, String name, int state) {
        name=name==null?"":name;
        log.info("业务层==[StudentInfoServiceImpl]->pageByValues(int pageIndex, int pageSize, String name, int state),开始执行");
        PageBean<StudentInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(sid.getCount());
        List<StudentInfo> list = sid.pageByValues(PageBean.countOffset(pageSize,pageIndex),pageSize,name,state);
        pageBean.setData(list);
        log.info("返回结果:"+pageBean);
        log.info("业务层==[StudentInfoServiceImpl]->pageByValues(int pageIndex, int pageSize, String name, int state),执行结束");
        return pageBean;
//        name=name==null?"":name;
//        PageBean<StudentInfo> pageBean = new PageBean<>();
//        pageBean.setCurrentPage(pageIndex);
//        pageBean.setPageSize(pageSize);
//        pageBean.setTotalRows(getCount(name,state));
//        int startIndex= (pageIndex-1) * pageSize;
//        pageBean.setData(sd.pageByValues(startIndex,pageSize,name,""));
//        return pageBean;
    }

    /**
     * 根据名字模糊查询
     *
     * @param pageIndex
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public PageBean<StudentInfo> findByName(int pageIndex, int pageSize, String name) {
        name=name==null?"":name;
        log.info("业务层==[StudentInfoServiceImpl]->findByName(int pageIndex, int pageSize, String name),开始执行");
        PageBean<StudentInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(sid.getCount());
        List<StudentInfo> list = sid.pageByValues(PageBean.countOffset(pageSize,pageIndex),pageSize,name);
        pageBean.setData(list);
        log.info("返回结果:"+pageBean);
        log.info("业务层==[StudentInfoServiceImpl]->findByName(int pageIndex, int pageSize, String name),执行结束");
        return pageBean;
    }
}
