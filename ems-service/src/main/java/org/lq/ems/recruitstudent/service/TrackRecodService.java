package org.lq.ems.recruitstudent.service;

import org.lq.recruitstudent.entity.TrackRecordInfo;
import org.lq.util.PageBean;

/**
 * @author 一颗大李子
 * @create 2020-10-13 20:41
 */
public interface TrackRecodService {

    /**
     * 添加
     * @param trackRecordInfo
     * @return
     */
    int save(TrackRecordInfo trackRecordInfo);

    /**
     * 修改
     * @param trackRecordInfo
     * @return
     */
    int update(TrackRecordInfo trackRecordInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    TrackRecordInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PageBean<TrackRecordInfo> pageList(int pageIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param name
     * @param enrollment
     * @return
     */
    int getCount(String name,int enrollment);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param name
     * @param enrollment
     * @return
     */
    PageBean<TrackRecordInfo> pageByValues(int pageIndex,int pageSize,String name,int enrollment);
}
