package org.lq.ems.finance.service;

import org.lq.finance.entity.StudentPayment;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 岳鑫
 * @creat 2020-10-13 20:57
 */
public interface StudentPaymentService {

    /**
     * t添加
     * @param s
     * @return
     */
    boolean save(StudentPayment s);

    /**
     * 修改
     * @param s
     * @return
     */
    boolean update(StudentPayment s);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    StudentPayment getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<StudentPayment> pageList(int pageIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String...values);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<StudentPayment> pageByValues(int pageIndex, int pageSize, String...value);
}
