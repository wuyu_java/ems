package org.lq.ems.finance.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.ems.finance.service.StudentPaymentService;
import org.lq.finance.dao.StudentPaymentDao;
import org.lq.finance.dao.impl.StudentPaymentDaoImpl;
import org.lq.finance.entity.StudentPayment;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 岳鑫
 * @creat 2020-10-13 21:01
 */
@Log4j
public class StudentPaymentServiceImpl implements StudentPaymentService {
    private StudentPaymentDao studentPaymentDao = new StudentPaymentDaoImpl();
    @Override
    public boolean save(StudentPayment s) {
        log.info("数据业务层--添加学员缴费");
        return studentPaymentDao.save(s) > 0;
    }

    @Override
    public boolean update(StudentPayment s) {
        log.info("数据业务层--修改学员缴费");
        return studentPaymentDao.update(s) > 0;
    }

    @Override
    public boolean delete(int id) {
        log.info("数据业务层--删除学员缴费");
        return studentPaymentDao.delete(id) > 0;
    }

    @Override
    public StudentPayment getById(int id) {
        log.info("数据业务层--根据id查询学员缴费");
        return studentPaymentDao.getById(id);
    }

    @Override
    public int getCount() {
        log.info("数据业务层--getCount()");
        return studentPaymentDao.getCount();
    }

    @Override
    public List<StudentPayment> pageList(int pageIndex, int pageSize) {
        log.info("数据业务层--pageList()");
        int startIndex = (pageIndex-1) * pageSize;
        return studentPaymentDao.pageList(startIndex,pageSize);
    }

    @Override
    public int getCount(String... values) {
        log.info("数据业务层--getCount(values)");
        return studentPaymentDao.getCount(values);
    }

    @Override
    public PageBean<StudentPayment> pageByValues(int pageIndex, int pageSize, String... value) {
        log.info("数据业务层--pageByValues()");
        PageBean<StudentPayment> pageBean = new PageBean<StudentPayment>();
        //当前页数
        pageBean.setCurrentPage(pageIndex);
        //每页显示的行数
        pageBean.setPageSize(pageSize);  //总行数
        int startIndex = (pageIndex-1)*pageSize;
        pageBean.setTotalRows(studentPaymentDao.getCount());
        pageBean.setTotalPage(pageSize);
        List<StudentPayment> list = studentPaymentDao.pageByValues(pageBean.countOffset(pageSize, pageIndex), pageSize, value);
        pageBean.setData(list);
        return pageBean;
    }


}
