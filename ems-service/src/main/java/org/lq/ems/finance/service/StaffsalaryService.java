package org.lq.ems.finance.service;

import org.lq.finance.entity.StaffSalary;
import org.lq.util.PageBean;

import java.util.List;

/**员工薪水业务层
 * @author 骆杨
 * @create 2020-10-14 上午 8:59
 */
public interface StaffsalaryService {
    /**
     * 添加
     * @param s
     * @return
     */
    boolean addStaffsalary(StaffSalary s);
    boolean deleteStaffsalary(int id);
    boolean updateStaffsalary(StaffSalary s);
    StaffSalary getStaffsalaryById(int id);

    /**
     * 总行数
     * @return
     */
    int  getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<StaffSalary> pageList(int pageIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String...values);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<StaffSalary> pageByValues(int pageIndex, int pageSize, String...value);


}
