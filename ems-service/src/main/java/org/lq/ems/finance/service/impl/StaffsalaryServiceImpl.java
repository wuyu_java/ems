package org.lq.ems.finance.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.ems.finance.service.StaffsalaryService;
import org.lq.finance.dao.StaffsalaryDao;
import org.lq.finance.dao.impl.StaffsalaryDaoImpl;
import org.lq.finance.entity.StaffSalary;
import org.lq.util.PageBean;

import java.util.List;

/**员工薪水业务实现
 * @author 骆杨
 * @create 2020-10-14 下午 5:42
 */
@Log4j
public class StaffsalaryServiceImpl implements StaffsalaryService {
    StaffsalaryDao staffsalaryDao = new StaffsalaryDaoImpl();

    /**
     * 添加
     * @param s
     * @return
     */
    @Override
    public boolean addStaffsalary(StaffSalary s) {
        log.info("--->业务逻辑层,添加 [StaffsalaryServiceimpl] addStaffsalary");
        return staffsalaryDao.save(s)>0;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public boolean deleteStaffsalary(int id) {
        log.info("--->业务逻辑层,删除 [StaffsalaryServiceimpl] deleteStaffsalary");
        return staffsalaryDao.delete(id)>0;
    }

    /**
     * 修改
     * @param s
     * @return
     */
    @Override
    public boolean updateStaffsalary(StaffSalary s) {
        log.info("--->业务逻辑层,修改 [StaffsalaryServiceimpl] updateStaffsalary");
        return staffsalaryDao.update(s)>0;
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @Override
    public StaffSalary getStaffsalaryById(int id) {
        log.info("--->业务逻辑层, 根据ID查询 [StaffsalaryServiceimpl] getStaffsalaryById(int id)");
        return staffsalaryDao.getById(id);
    }

    /**
     * 总行数
     * @return
     */
    @Override
    public int getCount() {
        log.info("--->业务逻辑层,总行数 [StaffsalaryServiceimpl] getCount()");
        return staffsalaryDao.getCount();
    }

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<StaffSalary> pageList(int pageIndex, int pageSize) {
        int startindex = (pageIndex-1)*pageSize;//开始位置
        log.info("--->业务逻辑层,分页查询 [StaffsalaryServiceimpl] pageList");
        return staffsalaryDao.pageList(startindex,pageSize);
    }

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        values[0] = values[0] == null ? "" :values[0];
        log.info("--->业务逻辑层,根据条件查询总行数 [StaffsalaryServiceimpl] getCount(String... values)");
        return staffsalaryDao.getCount();
    }

    /**
     * 根据条件查询分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean<StaffSalary> pageByValues(int pageIndex, int pageSize, String... value) {
        value[0] = value[0] == null ? "" :value[0];
        PageBean<StaffSalary> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(staffsalaryDao.getCount(value));
        List<StaffSalary> list = staffsalaryDao.pageByValues(PageBean.countOffset(pageSize,pageIndex),pageSize,value);
        pageBean.setData(list);
        log.info("--->业务逻辑层,根据条件查询分页查询 [StaffsalaryServiceimpl] pageByValues(int pageIndex, int pageSize, String... value) ");
        return pageBean;
    }
}
