package org.lq.ems.education.service;

import org.lq.education.entity.ClassTransactionInfo;
import org.lq.util.PageBean;

/**
 * @author HXD
 * @create 2020-10-13 20:49
 */
public interface ClassTransactionInfoService {

    /**
     * t添加
     * @param c
     * @return
     */
    boolean save(ClassTransactionInfo c);

    /**
     * 修改
     * @param c
     * @return
     */
    boolean update(ClassTransactionInfo c);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    ClassTransactionInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex 当前页
     * @param pageSize 每页显示的行数
     * @return
     */
    PageBean<ClassTransactionInfo> pageList(int pageIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String...values);

    /**
     * 根据条件分页查询
     * @param pageIndex 当前页
     * @param pageSize 每页显示的行数
     * @param value
     * @return
     */
    PageBean<ClassTransactionInfo> pageByValues(int pageIndex,int pageSize,String...value);
}
