package org.lq.ems.education.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.education.dao.ClassTransactionInfoDao;
import org.lq.education.dao.impl.ClassTransactionInfoDaoImpl;
import org.lq.education.entity.ClassTransactionInfo;
import org.lq.ems.education.service.ClassTransactionInfoService;
import org.lq.util.PageBean;

/**
 * @author HXD
 * @create 2020-10-13 20:51
 */
@Log4j
public class ClassTransactionInfoServiceImpl implements ClassTransactionInfoService {
    ClassTransactionInfoDao cdao = new ClassTransactionInfoDaoImpl();
    /**
     * t添加
     *
     * @param c
     * @return
     */
    @Override
    public boolean save(ClassTransactionInfo c) {
        boolean save_bool = cdao.save(c)>0 ? true : false;
        log.info("业务层,添加："+save_bool);
        return save_bool;
    }

    /**
     * 修改
     * @param c
     * @return
     */
    @Override
    public boolean update(ClassTransactionInfo c) {
        boolean update_bool = cdao.update(c)>0 ? true : false;
        log.info("业务层,添加："+update_bool);
        return update_bool;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(int id) {
        boolean delete_bool = cdao.delete(id)>0 ? true : false;
        log.info("业务层,删除："+delete_bool);
        return delete_bool;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public ClassTransactionInfo getById(int id) {
        ClassTransactionInfo byId = cdao.getById(id);
        log.info("业务层,根据id查询："+byId);
        return byId;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        int count = cdao.getCount();
        log.info("业务层,数据总行数"+count);
        return count;
    }

    /**
     * 分页查询
     *
     * @param pageIndex 当前页
     * @param pageSize 每页显示的行数
     * @return
     */
    @Override
    public PageBean<ClassTransactionInfo> pageList(int pageIndex, int pageSize) {

        PageBean<ClassTransactionInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);//当前页数
        pageBean.setPageSize(pageSize); // 每页显示的行数
        pageBean.setTotalRows(getCount());//总行数
        pageBean.setData(cdao.pageList(pageBean.countOffset(pageSize,pageIndex),pageSize));
        log.info("业务层,分页查询："+pageBean);
        return pageBean;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        // TODO: 条件未知
        int count = cdao.getCount(values[0]);
        log.info("业务层,根据条件查询总行数"+count);
        return count;
    }

    /**
     * 根据条件分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean<ClassTransactionInfo> pageByValues(int pageIndex, int pageSize, String... value) {
        PageBean<ClassTransactionInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);//当前页数
        pageBean.setPageSize(pageSize); // 每页显示的行数
        pageBean.setTotalRows(getCount());//总行数
        // TODO: 条件未知
        pageBean.setData(cdao.pageByValues(pageBean.countOffset(pageSize,pageIndex),pageSize,value[0]));
        log.info("业务层,分页查询："+pageBean);
        return pageBean;
    }
}
