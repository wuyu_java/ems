package org.lq.ems.education.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.education.dao.AttendanceDao;
import org.lq.education.dao.impl.AttendanceDaoImpl;
import org.lq.education.entity.AttendanceInfo;
import org.lq.ems.education.service.AttendanceService;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 崔竞辉
 * @create 2020-10-13 20:49
 */
@Log4j
public class AttendanceServiceImpl implements AttendanceService {
    AttendanceDao attendanceDao = new AttendanceDaoImpl();

    /**
     * attendanceInfo添加
     * @param attendanceInfo
     * @return
     */
    @Override
    public int save(AttendanceInfo attendanceInfo) {
        log.info("业务逻辑层:学员考勤-save-start--");
        return attendanceDao.save(attendanceInfo);
    }

    /**
     * 修改
     * @param attendanceInfo
     * @return
     */
    @Override
    public int update(AttendanceInfo attendanceInfo) {
        log.info("业务逻辑层:学员考勤-update-start--");
        return attendanceDao.update(attendanceInfo);
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("业务逻辑层:学员考勤-delete-start--");
        return attendanceDao.delete(id) ;
    }


    /**
     * 通过ID查询
     * @param id
     * @return
     */
    @Override
    public AttendanceInfo getById(int id) {
        log.info("业务逻辑层:学员考勤-getById-start--");
        return attendanceDao.getById(id);
    }


    /**
     * 总行数
     * @return
     */
    @Override
    public int getCount() {
        log.info("业务逻辑层:学员考勤-getCount-start--");
        return attendanceDao.getCount();
    }


    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<AttendanceInfo> pageList(int pageIndex, int pageSize) {
        log.info("业务逻辑层:学员考勤-pageList-start--");
        int startIndex = (pageSize-1)*pageSize;//开始位置
        return attendanceDao.pageList(startIndex,pageSize);
    }


    /**
     * 根据条件查询总行数
     * @param value
     * @return
     */
    @Override
    public int getCount(String value) {
        log.info("业务逻辑层:学员考勤-带参getCount-start--");
        value = value == null ? "":value;
        return attendanceDao.getCount(value);
    }


    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean<AttendanceInfo> pageByValues(int pageIndex, int pageSize, String value) {
        log.info("业务逻辑层:学员考勤-pageByValues-start--");
        value = value == null ? "":value;
        PageBean<AttendanceInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(value));
        int startIndex = (pageIndex-1)*pageSize;
        pageBean.setData(attendanceDao.pageByValues(pageIndex,pageSize,value));
        log.info("业务逻辑层:学员考勤-pageByValues-end--");
        return pageBean;
    }
}
