package org.lq.ems.education.service;

import org.lq.education.entity.AttendanceInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 崔竞辉
 * @create 2020-10-13 20:48
 */
public interface AttendanceService {
    /**
     * attendanceInfo添加
     * @param attendanceInfo
     * @return
     */
    int save(AttendanceInfo attendanceInfo);

    /**
     * 修改
     * @param attendanceInfo
     * @return
     */
    int update(AttendanceInfo attendanceInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    AttendanceInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<AttendanceInfo> pageList(int pageIndex,int pageSize);

    /**
     * 根据条件查询总行数
     * @param value
     * @return
     */
    int getCount(String value);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<AttendanceInfo> pageByValues(int pageIndex, int pageSize, String value);

}
