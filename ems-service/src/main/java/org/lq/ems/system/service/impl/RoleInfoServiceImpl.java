package org.lq.ems.system.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.RoleInfoService;
import org.lq.system.dao.RoleInfoDao;
import org.lq.system.dao.impl.RoleInfoDaoImpl;
import org.lq.system.entity.RoleInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 李冠良
 * @create 2020-10-13
 */
@Log4j
public class RoleInfoServiceImpl implements RoleInfoService<RoleInfo> {
    private RoleInfoDao roleInfoDao = new RoleInfoDaoImpl();

    /**
     * 添加
     *
     * @param roleInfo
     * @return
     */
    @Override
    public int save(RoleInfo roleInfo) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->save");
        int num = 0;
        num = roleInfoDao.save(roleInfo);
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [RoleInfoServiceImpl]->save,执行结束");
        return num;
    }

    /**
     * 修改
     *
     * @param roleInfo
     * @return
     */
    @Override
    public int update(RoleInfo roleInfo) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->update");
        int num = 0;
        num = roleInfoDao.update(roleInfo);
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [RoleInfoServiceImpl]->update,执行结束");
        return num;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->delete");
        int num = 0;
        num = roleInfoDao.delete(id);
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [RoleInfoServiceImpl]->delete,执行结束");
        return num;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public RoleInfo getById(int id) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->getById");
        RoleInfo roleInfo = null;
        roleInfo = roleInfoDao.getById(id);
        log.info("-->执行结果: " + roleInfo);
        log.info("-->业务层, [RoleInfoServiceImpl]->getById,执行结束");
        return roleInfo;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->getCount(无参数)");
        int num = 0;
        num = roleInfoDao.getCount();
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [RoleInfoServiceImpl]->getCount(无参数),执行结束");
        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<RoleInfo> pageList(int startIndex, int pageSize) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->pageList(int startIndex, int pageSize)");
        List<RoleInfo> list = null;
        list = roleInfoDao.pageList(startIndex, pageSize);
        log.info("-->执行结果: " + list);
        log.info("-->业务层, [RoleInfoServiceImpl]->pageList(int startIndex, int pageSize),执行结束");
        return list;
    }

    /**
     * 总行数
     *
     * @param roleName
     * @return
     */
    @Override
    public int getCount(String roleName) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->getCount(String roleName)");
        int num = 0;
        num = roleInfoDao.getCount(roleName);
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [RoleInfoServiceImpl]->getCount(String roleName),执行结束");
        return num;
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param roleName
     * @return
     */
    @Override
    public List<RoleInfo> pageByValuesState(int pageIndex, int pageSize, String roleName) {
        int startIndex = (pageIndex-1) * pageSize;
        return roleInfoDao.pageByValues(startIndex,pageSize,roleName,"1");
    }

    /**
     * 总行数
     *
     * @param roleName
     * @return
     */
    @Override
    public int getCountState(String roleName) {
        return roleInfoDao.getCount(roleName,"1");
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param roleName
     * @return
     */
    @Override
    public List<RoleInfo> pageByValues(int startIndex, int pageSize, String roleName) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->pageByValues(int startIndex, int pageSize, String roleName)");
        List<RoleInfo> list = null;
        list = roleInfoDao.pageByValues(startIndex, pageSize,roleName);
        log.info("-->执行结果: " + list);
        log.info("-->业务层, [RoleInfoServiceImpl]->pageByValues(int startIndex, int pageSize, String roleName),执行结束");
        return list;
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param roleName
     * @return
     */
    @Override
    public PageBean<RoleInfo> findPageBean(int pageIndex, int pageSize, String roleName) {
        log.info("-->开始执行业务层, [RoleInfoServiceImpl]->findPageBean(int pageIndex, int pageSize, String roleName)");
        PageBean<RoleInfo> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);   //设置当前页数
        log.info("-->执行结果(currentpage): " + pageIndex);
        pageBean.setPageSize(pageSize);  //设置每页显示的数据条数
        log.info("-->执行结果(pagesize): " + pageSize);
        roleName = roleName == null ? "" :roleName;
        pageBean.setTotalRows(roleInfoDao.getCount(roleName));
        int startIndex = PageBean.countOffset(pageSize, pageIndex);

        List<RoleInfo> list = roleInfoDao.pageByValues(startIndex, pageSize, roleName);
        pageBean.setData(list);
        log.info("-->执行结果(data): " + list);
        log.info("-->执行结果: " + pageBean);
        log.info("-->业务层, [RoleInfoServiceImpl]->findPageBean(int pageIndex, int pageSize, String roleName),执行结束");
        return pageBean;
    }

    @Override
    public int updateRoleById(int id, int state) {
        return roleInfoDao.updateRoleById(id,state);
    }
}
