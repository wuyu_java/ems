package org.lq.ems.system.service;

import org.lq.system.entity.DataDictionary;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 李冠良
 * @create 2020-10-16
 */
public interface DataDictionaryService {
    /**
     * 添加
     * @param dataDictionary
     * @return
     */
    int save(DataDictionary dataDictionary);

    /**
     * 修改
     * @param dataDictionary
     * @return
     */
    int update(DataDictionary dataDictionary);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    DataDictionary getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<DataDictionary> pageList(int startIndex, int pageSize);

    /**
     * 总行数
     * @param content
     * @return
     */
    int getCount(String content);

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @param content
     * @return
     */
    List<DataDictionary> pageList(int startIndex, int pageSize, String content);

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @param content
     * @return
     */
    PageBean<DataDictionary> findPageBean(int pageIndex, int pageSize, String content);

    List<DataDictionary> getEducationalLevel();

}
