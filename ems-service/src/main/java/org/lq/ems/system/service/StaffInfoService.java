package org.lq.ems.system.service;

import org.lq.base.BaseDao;
import org.lq.system.entity.StaffInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 马秋阳
 * @create 2020-10-13 20:56
 */
public interface StaffInfoService{

    /**
     * t添加
     * @param t
     * @return
     */
    int save(StaffInfo staffInfo);

    /**
     * 修改
     * @param t
     * @return
     */
    int update(StaffInfo staffInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    StaffInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PageBean<StaffInfo> pageList(int pageIndex,int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String values);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<StaffInfo> pageByValues(int pageIndex, int pageSize, String values);
    /**
     * 根据角色名称查询
     * @param roleId
     * @return
     */
    List<StaffInfo> getByRoleId(int roleId);


}
