package org.lq.ems.system.service.impl;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j;
import org.apache.commons.beanutils.BeanUtils;
import org.lq.ems.system.service.AnthortyInfoService;
import org.lq.system.dao.AnthortyInfoDao;
import org.lq.system.dao.impl.AnthortyInfoDaoImpl;
import org.lq.system.entity.AnthortyInfo;
import org.lq.system.entity.dto.AnthortyInfoDto;
import org.lq.util.PageBean;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * 权限信息业务层接口实现
 *
 * @author HXT
 * @create 2020-10-13 21:49
 */
@Log4j
public class AnthortyInfoServiceImpl implements AnthortyInfoService {
    private AnthortyInfoDao anthortyInfoDao = new AnthortyInfoDaoImpl();

    /**
     * 添加权限信息
     *
     * @param anthortyInfo
     * @return
     */
    @Override
    public int save(AnthortyInfo anthortyInfo) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->save");
        int num = 0;
        num = anthortyInfoDao.save(anthortyInfo);
        log.info("-->执行结果：" + num);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->save,执行结束");
        return num;
    }

    /**
     * 修改权限信息
     *
     * @param anthortyInfo
     * @return
     */
    @Override
    public int update(AnthortyInfo anthortyInfo) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->update");
        int num = 0;
        num = anthortyInfoDao.update(anthortyInfo);
        log.info("-->执行结果：" + num);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->update,执行结束");
        return num;
    }

    /**
     * 按id删除权限信息
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->delete");
        int num = 0;
        num = anthortyInfoDao.delete(id);
        log.info("-->执行结果：" + num);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->delete,执行结束");
        return num;
    }

    /**
     * 根据id获取权限信息
     *
     * @param id
     * @return
     */
    @Override
    public AnthortyInfo getById(int id) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->getById");
        AnthortyInfo byId = anthortyInfoDao.getById(id);
        log.info("-->执行结果：" + byId);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->getById,执行结束");
        return byId;
    }

    /**
     * 获取总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->getCount");
        int count = anthortyInfoDao.getCount();
        log.info("-->执行结果：" + count);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->getCount,执行结束");
        return count;
    }

    /**
     * 分页查询
     *
     * @param pageIndex 页数
     * @param pageSize 步长
     * @return
     */
    @Override
    public List<AnthortyInfo> pageList(int pageIndex, int pageSize) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->pageList");
        List<AnthortyInfo> list = anthortyInfoDao.pageList((pageIndex-1)*pageSize, pageSize);
        log.info("-->执行结果：" + list);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->pageList,执行结束");
        return list;
    }

    /**
     * 根据权限信息名字进行模糊查询，返回数据条数
     *
     * @param values
     * @return
     */
    @Override
    public int getCountByName(String values) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->getCountByName");
        int countByName = anthortyInfoDao.getCountByName(values);
        log.info("-->执行结果：" + countByName);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->getCountByName,执行结束");
        return countByName;
    }

    /**
     * 根据名字模糊查询，然后指定开始位置和步长，返回集合列表
     *
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<AnthortyInfo> pageByValues(int pageIndex, int pageSize, String value) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->pageByValues");
        List<AnthortyInfo> list = anthortyInfoDao.pageByValues((pageIndex-1)*pageSize, pageSize, value);
        log.info("-->执行结果：" + list);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->getCountByName,执行结束");
        return list;
    }

    /**
     * 根据权限信息名字获取对应的权限编号
     *
     * @param name
     * @return
     */
    @Override
    public int getAuthortyInfoIdByName(String name) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->getAuthortyInfoIdByName");
        int num = anthortyInfoDao.getAuthortyInfoIdByName(name);
        log.info("-->执行结果：" + num);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->getAuthortyInfoIdByName,执行结束");
        return num;
    }

    /**
     * 根据上级编号获取同一级下的所有权限信息
     *
     * @param pid
     * @return
     */
    @Override
    public List<AnthortyInfo> getAuthortyInfoByPid(int pid) {
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->getAuthortyInfoByPid");
        List<AnthortyInfo> list = anthortyInfoDao.getAuthortyInfoByPid(pid);
        log.info("-->执行结果：" + list);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->getAuthortyInfoByPid,执行结束");
        return list;
    }

    /**
     * 根据页数，步长，模糊查询关键字获取PageBean对象
     * @param pageIndex
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public PageBean<AnthortyInfo> findPageBean(int pageIndex,int pageSize,String name){
        log.info("-->开始执行业务层,[AnthortyInfoServiceImpl]->findPageBean");
        name = name == null ? "" : name;
        PageBean<AnthortyInfo> pageBean = new PageBean<>();//获取pagebean对象
        pageBean.setCurrentPage(pageIndex);//设置当前页数
        pageBean.setPageSize(pageSize);//设置步长
        pageBean.setTotalRows(anthortyInfoDao.getCountByName(name));//设置数据总行数
        List<AnthortyInfo> list = anthortyInfoDao.pageByValues(PageBean.countOffset(pageSize,pageIndex),pageSize,name);
        pageBean.setData(list);
        log.info("-->执行结果：" + pageBean);
        log.info("-->业务层,[AnthortyInfoServiceImpl]->findPageBean,执行结束");
        return pageBean;
    }


    /**
     * 返回Tree节点的JSON对象
     *
     * @return
     */
    @Override
    public String getTreeJSON() {
        AnthortyInfoDto parent = new AnthortyInfoDto();
        parent.setSpread(true);
        AnthortyInfo info = getById(1);
        List<AnthortyInfo> list = anthortyInfoDao.pageList(0,getCount());
        AnthortyInfoDto tree = null;
        try {
            BeanUtils.copyProperties(parent,info);
             tree = getTree(list, parent);
        } catch (IllegalAccessException e) {
            log.error("转换对象错误",e);
        } catch (InvocationTargetException e) {
            log.error("转换对象错误",e);
        }

        return new Gson().toJson(tree);
    }

    @Override
    public AnthortyInfoDto getTree() {
        AnthortyInfoDto parent = new AnthortyInfoDto();
        parent.setSpread(true);
        AnthortyInfo info = getById(1);
        List<AnthortyInfo> list = anthortyInfoDao.pageList(0,getCount());
        AnthortyInfoDto tree = null;
        try {
            BeanUtils.copyProperties(parent,info);
            tree = getTree(list, parent);
        } catch (IllegalAccessException e) {
            log.error("转换对象错误",e);
        } catch (InvocationTargetException e) {
            log.error("转换对象错误",e);
        }
        return tree;
    }


    @Override
    public List<AnthortyInfo> getParent() {
//        List<AnthortyInfo> list = new ArrayList<>();
//        list.add(getById(1));
        List<AnthortyInfo> parents = getAuthortyInfoByPid(1);
//        list.addAll(parents);
//        parents.set(0,getById(1));
        parents.add(0,getById(1));
        return parents;
    }

    //递归循环->无限层次循环结构
    protected AnthortyInfoDto getTree(List<AnthortyInfo> list, AnthortyInfoDto dto) throws InvocationTargetException, IllegalAccessException {
        for (AnthortyInfo info : list){
            if(info.getAnthortyPid() == dto.getAnthortyId()){
                AnthortyInfoDto child = new AnthortyInfoDto();
                BeanUtils.copyProperties(child,info);
                dto.getList().add(child);
                getTree(list,child);
            }
        }
        return dto;
    }

}
