package org.lq.ems.system.service;

import org.lq.system.entity.RoleAnthority;

import java.util.List;

/**
 * @author 付金晨
 * @create 2020 10 13 22:29
 */
public interface RoleAnthorityService {

    /**
     * t添加
     * @param t
     * @return
     */
    int save(RoleAnthority t);

    /**
     * 修改
     * @param t
     * @return
     */
    int update(RoleAnthority t);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    RoleAnthority getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<RoleAnthority> pageList(int startIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String...values);

    /**
     * 根据条件分页查询
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    List<RoleAnthority> pageByValues(int startIndex,int pageSize,String...value);

    /**
     * 授权
     * @param aid
     * @param pid
     * @param rid
     * @return
     */
    boolean grantAuthorization(int aid,int pid,int rid);

    /**
     * 撤销权限
     * @param aid
     * @param pid
     * @param rid
     * @return
     */
    boolean revocationAuthorization(int aid,int pid,int rid);


    /**
     * 授权
     * @param rid
     * @return
     */
    boolean grantAuthorization(String json,int rid);

    /**
     * 撤销权限
     * @param rid
     * @return
     */
    boolean revocationAuthorization(String json,int rid);

    public List<Integer> getAnthotyByRid(int rid);

}
