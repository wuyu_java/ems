package org.lq.ems.system.service.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.lq.ems.system.service.StaffInfoService;
import org.lq.system.dao.StaffInfoDao;
import org.lq.system.dao.impl.StaffInfoDaoImpl;
import org.lq.system.entity.StaffInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 马秋阳
 * @create 2020-10-13 20:57
 */
public class StaffInfoServiceImpl implements StaffInfoService {
    StaffInfoDao sd = new StaffInfoDaoImpl();
    /**
     * t添加
     *
     * @param staffInfo
     * @return
     */
    @Override
    public int save(StaffInfo staffInfo) {
        String password =  DigestUtils.md5Hex(staffInfo.getUserPassowrd()+staffInfo.getUserNumber()+staffInfo.getStaffName());
        staffInfo.setUserPassowrd(password);
        return sd.save(staffInfo);
    }

    /**
     * 修改
     *
     * @param staffInfo
     * @return
     */
    @Override
    public int update(StaffInfo staffInfo) {
        if("".equals(staffInfo.getUserPassowrd())){
            StaffInfo oldStaff = sd.getById(staffInfo.getStaffId());
            staffInfo.setUserPassowrd(oldStaff.getUserPassowrd());
        }else{
            String password =  DigestUtils.md5Hex(staffInfo.getUserPassowrd()+staffInfo.getUserNumber()+staffInfo.getStaffName());
            staffInfo.setUserPassowrd(password);
        }
        return sd.update(staffInfo);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        return sd.delete(id);
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public StaffInfo getById(int id) {
        return sd.getById(id);
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        return sd.getCount();
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageBean<StaffInfo> pageList(int pageIndex, int pageSize) {
        PageBean<StaffInfo> pageBean = new PageBean<>();
        pageBean.setTotalRows(sd.getCount());
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        List<StaffInfo> all = sd.pageList(PageBean.countOffset(pageSize,pageIndex),pageSize);
        pageBean.setData(all);
        return pageBean;
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String values) {
        values = values == null ? "" : values;
        return sd.getCount(values);
    }

    /**
     * 根据条件分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param values
     * @return
     */
    @Override
    public PageBean<StaffInfo> pageByValues(int pageIndex, int pageSize, String values) {
        values = values == null ? "" : values;
        PageBean<StaffInfo> pageBean = new PageBean<>();
        pageBean.setPageSize(pageSize);
        pageBean.setCurrentPage(pageIndex);
        pageBean.setTotalRows(sd.getCount(values));
        List<StaffInfo> all = sd.pageByValues(PageBean.countOffset(pageSize, pageIndex), pageSize, values);
        pageBean.setData(all);
        return pageBean;
    }

    /**
     * 根据角色名称查询
     *
     * @param roleId
     * @return
     */
    @Override
    public List<StaffInfo> getByRoleId(int roleId) {
        return sd.getByRoleId(roleId);
    }
}
