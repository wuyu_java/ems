package org.lq.ems.system.service;

import org.lq.system.entity.AnthortyInfo;
import org.lq.system.entity.dto.AnthortyInfoDto;
import org.lq.util.PageBean;

import java.util.List;

/**
 * 权限信息业务层接口
 * @author HXT
 * @create 2020-10-13 21:40
 */
public interface AnthortyInfoService {
    /**
     * 添加权限信息
     * @param anthortyInfo
     * @return
     */
    int save(AnthortyInfo anthortyInfo);

    /**
     * 修改权限信息
     * @param anthortyInfo
     * @return
     */
    int update(AnthortyInfo anthortyInfo);

    /**
     * 按id删除权限信息
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 根据id获取权限信息
     * @param id
     * @return
     */
    AnthortyInfo getById(int id);

    /**
     * 获取总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<AnthortyInfo> pageList(int pageIndex,int pageSize);
    /**
     * 根据权限信息名字进行模糊查询，返回数据条数
     * @param values
     * @return
     */
    int getCountByName(String values);

    /**
     * 根据名字模糊查询，然后指定开始位置和步长，返回集合列表
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    List<AnthortyInfo> pageByValues(int pageIndex, int pageSize, String value);

    /**
     * 根据权限信息名字获取对应的权限编号
     * @param name
     * @return
     */
    int getAuthortyInfoIdByName(String name);

    /**
     * 根据上级编号获取同一级下的所有权限信息
     * @param pid
     * @return
     */
    List<AnthortyInfo> getAuthortyInfoByPid(int pid);
    /**
     * 根据页数，步长，模糊查询关键字获取PageBean对象
     * @param pageIndex
     * @param pageSize
     * @param name
     * @return
     */
    PageBean<AnthortyInfo> findPageBean(int pageIndex, int pageSize, String name);

    /**
     * 返回Tree节点的JSON对象
     * @return
     */
    String getTreeJSON();

    AnthortyInfoDto getTree();


    List<AnthortyInfo> getParent();

}
