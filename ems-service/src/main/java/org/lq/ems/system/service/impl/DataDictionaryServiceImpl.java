package org.lq.ems.system.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.DataDictionaryService;
import org.lq.system.dao.DataDictionaryDao;
import org.lq.system.dao.impl.DataDictionaryDaoImpl;
import org.lq.system.entity.DataDictionary;
import org.lq.util.DataDicationary;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 李冠良
 * @create 2020-10-16
 */
@Log4j
public class DataDictionaryServiceImpl implements DataDictionaryService {
    private DataDictionaryDao dataDictionaryDao = new DataDictionaryDaoImpl();
    /**
     * 添加
     *
     * @param dataDictionary
     * @return
     */
    @Override
    public int save(DataDictionary dataDictionary) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->save");
        int num = 0;
        num = dataDictionaryDao.save(dataDictionary);
        log.info("-->执行结果： "+num);
        log.info("-->业务层, [DataDictionaryServiceImpl]->save,执行结束");
        return num;
    }

    /**
     * 修改
     *
     * @param dataDictionary
     * @return
     */
    @Override
    public int update(DataDictionary dataDictionary) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->update");
        int num = 0;
        num = dataDictionaryDao.update(dataDictionary);
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [DataDictionaryServiceImpl]->update,执行结束");
        return num;
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->delete");
        int num = 0;
        num = dataDictionaryDao.delete(id);
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [DataDictionaryServiceImpl]->delete,执行结束");
        return num;
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public DataDictionary getById(int id) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->getById");
        DataDictionary dataDictionary = null;
        dataDictionary = dataDictionaryDao.getById(id);
        log.info("-->执行结果: " + dataDictionary);
        log.info("-->业务层, [DataDictionaryServiceImpl]->getById,执行结束");
        return dataDictionary;
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->getCount(无参数)");
        int num = 0;
        num = dataDictionaryDao.getCount();
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [DataDictionaryServiceImpl]->getCount(无参数),执行结束");
        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<DataDictionary> pageList(int startIndex, int pageSize) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->pageList(int startIndex, int pageSize)");
        List<DataDictionary> list = null;
        list = dataDictionaryDao.pageList(startIndex, pageSize);
        log.info("-->执行结果: " + list);
        log.info("-->业务层, [DataDictionaryServiceImpl]->pageList(int startIndex, int pageSize),执行结束");
        return list;
    }

    /**
     * 总行数
     *
     * @param content
     * @return
     */
    @Override
    public int getCount(String content) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->getCount(String content)");
        int num = 0;
        num = dataDictionaryDao.getCount(content);
        log.info("-->执行结果: " + num);
        log.info("-->业务层, [DataDictionaryServiceImpl]->getCount(String content),执行结束");
        return num;
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param content
     * @return
     */
    @Override
    public List<DataDictionary> pageList(int startIndex, int pageSize, String content) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->pageByValues(int startIndex, int pageSize, String content)");
        List<DataDictionary> list = null;
        list = dataDictionaryDao.pageByValues(startIndex, pageSize,content);
        log.info("-->执行结果: " + list);
        log.info("-->业务层, [DataDictionaryServiceImpl]->pageByValues(int startIndex, int pageSize, String content),执行结束");
        return list;
    }

    /**
     * 分页查询
     *
     * @param pageIndex
     * @param pageSize
     * @param content
     * @return
     */
    @Override
    public PageBean<DataDictionary> findPageBean(int pageIndex, int pageSize, String content) {
        log.info("-->开始执行业务层, [DataDictionaryServiceImpl]->findPageBean(int pageIndex, int pageSize, String content)");
        PageBean<DataDictionary> pageBean = new PageBean<>();
        pageBean.setCurrentPage(pageIndex);   //设置当前页数
        log.info("-->执行结果(currentpage): " + pageIndex);
        pageBean.setPageSize(pageSize);  //设置每页显示的数据条数
        log.info("-->执行结果(pagesize): " + pageSize);
        content = content == null ? "" :content;
        pageBean.setTotalRows(dataDictionaryDao.getCount(content));
        int startIndex = PageBean.countOffset(pageSize, pageIndex);

        List<DataDictionary> list = dataDictionaryDao.pageByValues(startIndex, pageSize, content);
        pageBean.setData(list);
        log.info("-->执行结果(data): " + list);
        log.info("-->执行结果: " + pageBean);
        log.info("-->业务层, [DataDictionaryServiceImpl]->findPageBean(int pageIndex, int pageSize, String content),执行结束");
        return pageBean;
    }

    @Override
    public List<DataDictionary> getEducationalLevel() {
        return dataDictionaryDao.getDataByType(DataDicationary.EDUCATIONAL_LEVEL.getValue());
    }
}
