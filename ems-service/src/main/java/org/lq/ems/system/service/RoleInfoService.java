package org.lq.ems.system.service;

import org.lq.system.entity.RoleInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 李冠良
 * @create 2020-10-13
 */
public interface RoleInfoService<RoleInfo> {
    /**
     * 添加
     * @param roleInfo
     * @return
     */
    int save(RoleInfo roleInfo);

    /**
     * 修改
     * @param roleInfo
     * @return
     */
    int update(RoleInfo roleInfo);

    /**
     * 删除
     * @param id
     * @return
     */
    int delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    RoleInfo getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @return
     */
    List<RoleInfo> pageList(int startIndex, int pageSize);



    /**
     * 分页查询
     * @param startIndex
     * @param pageSize
     * @param roleName
     * @return
     */
    List<RoleInfo> pageByValues(int startIndex, int pageSize, String roleName);

    /**
     *分页查询
     * @param pageIndex
     * @param pageSize
     * @param roleName
     * @return
     */
    PageBean<RoleInfo> findPageBean(int pageIndex, int pageSize, String roleName);
    /**
     * 总行数
     * @param roleName
     * @return
     */
    int getCount(String roleName);
    /**
     *分页查询
     * @param pageIndex
     * @param pageSize
     * @param roleName
     * @return
     */
    List<RoleInfo> pageByValuesState(int pageIndex, int pageSize, String roleName);
    /**
     * 总行数
     * @param roleName
     * @return
     */
    int getCountState(String roleName);

    int updateRoleById(int id,int state);
}
