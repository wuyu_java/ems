package org.lq.ems.system.service.impl;


import com.google.gson.Gson;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.lq.ems.system.service.RoleAnthorityService;
import org.lq.system.dao.impl.RoleAnthorityDaoImpl;
import org.lq.system.entity.RoleAnthority;
import org.lq.util.CastUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 付金晨
 * @create 2020 10 13 23:08
 */
@Log4j
public class RoleAnthorityServiceImpl implements RoleAnthorityService {

    RoleAnthorityDaoImpl roleAnthorityDao = new RoleAnthorityDaoImpl();
    /**
     * t添加
     *
     * @param t
     * @return
     */
    @Override
    public int save(RoleAnthority t) {
        return roleAnthorityDao.save(t);
    }

    /**
     * 修改
     *
     * @param t
     * @return
     */
    @Override
    public int update(RoleAnthority t) {
        return roleAnthorityDao.update(t);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @Override
    public int delete(int id) {
        return roleAnthorityDao.delete(id);
    }

    /**
     * 通过ID查询
     *
     * @param id
     * @return
     */
    @Override
    public RoleAnthority getById(int id) {
        return roleAnthorityDao.getById(id);
    }

    /**
     * 总行数
     *
     * @return
     */
    @Override
    public int getCount() {
        return roleAnthorityDao.getCount();
    }

    /**
     * 分页查询
     *
     * @param startIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<RoleAnthority> pageList(int startIndex, int pageSize) {
        return roleAnthorityDao.pageList(startIndex,pageSize);
    }

    /**
     * 根据条件查询总行数
     *
     * @param values
     * @return
     */
    @Override
    public int getCount(String... values) {
        return roleAnthorityDao.getCount(values);
    }

    /**
     * 根据条件分页查询
     *
     * @param startIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public List<RoleAnthority> pageByValues(int startIndex, int pageSize, String... value) {
        return roleAnthorityDao.pageByValues(startIndex,pageSize,value);
    }

    /**
     * 授权
     *
     * @param aid
     * @param pid
     * @param rid
     * @return
     */
    @Override
    public boolean grantAuthorization(int aid, int pid, int rid) {

        /*
        1. 查询关联表中是否存在当前父菜单对象
            如果有添加当前菜单
            如果没有,先添加父菜单然后在添加子菜单
         */
        if( roleAnthorityDao.getRoleMenu(rid,pid) == 0 ){
            roleAnthorityDao.save(RoleAnthority.builder().roleId(rid).anthortyId(pid).build());
        }
        roleAnthorityDao.save(RoleAnthority.builder().roleId(rid).anthortyId(aid).build());


        return false;
    }

    /**
     * 撤销权限
     *
     * @param aid
     * @param pid
     * @param rid
     * @return
     */
    @Override
    public boolean revocationAuthorization(int aid, int pid, int rid) {
        /**
         * 1. 查询当前关联表中,当前角色还剩余多少条权限
         *  如果剩余数量 == 2 代表只剩自身删除菜单和父菜单,删除当前角色下面的所有权限
         *  如果不等于2 代表还有其他权限,只需要删除自身权限就可以了
         *
         */
        if ( roleAnthorityDao.getRACountByRid(rid) == 2 ) {
            roleAnthorityDao.deleteRAByRoleId(rid);
        }else{
            roleAnthorityDao.deleteRAByRoleIdAndAnthoryId(rid,aid);
        }
        return false;
    }

    @Override
    public boolean grantAuthorization(String json, int rid) {
        log.debug(json);
        Gson gson = new Gson();
        //讲当前选中的所有权限菜单,转换成java集合
        List<Map<String,Double>> as = gson.fromJson(json,new ArrayList<Map<String,Double>>().getClass());
        log.debug(as);
        as.forEach((e)->{
            int aid = e.get("value").intValue();
            int pid = e.get("anthortyPid").intValue();
            grantAuthorization(aid,pid,rid);
        });

        return false;
    }

    @Override
    public boolean revocationAuthorization(String json, int rid) {
        Gson gson = new Gson();

//讲当前选中的所有权限菜单,转换成java集合
        List<Map<String,Double>> as = gson.fromJson(json,new ArrayList<Map<String,Double>>().getClass());

        as.forEach((e)->{
            int aid = e.get("value").intValue();
            int pid = e.get("anthortyPid").intValue();
            revocationAuthorization(aid,pid,rid);
        });
        return false;
    }

    @Override
    public List<Integer> getAnthotyByRid(int rid) {
        return roleAnthorityDao.getAnthotyByRid(rid);
    }
}
