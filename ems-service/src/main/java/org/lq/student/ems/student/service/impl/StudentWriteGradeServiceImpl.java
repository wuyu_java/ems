package org.lq.student.ems.student.service.impl;

import lombok.extern.log4j.Log4j;
import org.lq.student.dao.impl.StudentWriteGradeDaoImpl;
import org.lq.student.ems.student.service.StudentWriteGradeService;
import org.lq.student.dao.StudentWriteGradeDao;
import org.lq.student.entity.StudentWriteGrade;
import org.lq.util.PageBean;

/**
 * @author haha
 * @create 2020-10-13 20:26
 */
@Log4j
public class StudentWriteGradeServiceImpl implements StudentWriteGradeService {
    private StudentWriteGradeDao studentWriteGradeDao =new StudentWriteGradeDaoImpl();

    /**
     * 添加学生成绩
     * @param t
     * @return
     */
    @Override
    public boolean save(StudentWriteGrade t) {
        return studentWriteGradeDao.save(t)>0;
    }

    /**
     * 修改学生成绩
     * @param t
     * @return
     */
    @Override
    public boolean update(StudentWriteGrade t) {
        return studentWriteGradeDao.update(t)>0;
    }

    /**
     * 删除学生成绩
     * @param id
     * @return
     */
    @Override
    public boolean delete(int id) {
        return studentWriteGradeDao.delete(id)>0;
    }

    /**
     * 根据学生成绩id查询
     * @param id
     * @return
     */
    @Override
    public StudentWriteGrade getById(int id) {
        return studentWriteGradeDao.getById(id);
    }

    /**
     * 获取成绩表的总行数
     * @return
     */
    @Override
    public int getCount() {
        return studentWriteGradeDao.getCount();
    }

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public PageBean<StudentWriteGrade> pageList(int pageIndex, int pageSize) {
        PageBean<StudentWriteGrade> pageBean=new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(studentWriteGradeDao.getCount());
        pageBean.setTotalPage(pageBean.getTotalPage());
        int startIndex=(pageIndex-1)*pageSize;
        pageBean.setData(studentWriteGradeDao.pageList(startIndex,pageSize));
        return pageBean;
    }

    /**
     * 模糊查询获取总行数
     * @param values
     * @return
     */
    @Override
    public int getCount(String values) {
        return studentWriteGradeDao.getCount(values);
    }

    /**
     * 分页模糊查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    @Override
    public PageBean<StudentWriteGrade> pageByValues(int pageIndex, int pageSize, String value) {
        PageBean<StudentWriteGrade> pageBean=new PageBean<>();
        pageBean.setCurrentPage(pageIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(studentWriteGradeDao.getCount(value));
        pageBean.setTotalPage(pageBean.getTotalPage());
        int startIndex=(pageIndex-1)*pageSize;
        pageBean.setData(studentWriteGradeDao.pageByValues(startIndex,pageSize,value));
        return pageBean;
    }
}
