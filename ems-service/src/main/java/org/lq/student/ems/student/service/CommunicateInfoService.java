package org.lq.student.ems.student.service;

import org.lq.student.entity.CommunicateInfo;
import org.lq.util.PageBean;

/**
 * @author 南以南
 * @create 2020-10-13 23:14
 */
public interface CommunicateInfoService {

    boolean save(CommunicateInfo communicateInfo);

    boolean delete(int id);

    boolean update(CommunicateInfo communicateInfo);

    CommunicateInfo getById(int id);

    int getCount(String value);

    PageBean<CommunicateInfo> pageByValues(int startIndex, int pageSize, String value);


}
