package org.lq.student.ems.student.service;

import org.lq.student.entity.StudentWriteGrade;
import org.lq.util.PageBean;

/**
 * @author 无语
 * @create 2020-10-13 16:12
 */
public interface StudentWriteGradeService {
    /**
     * t添加
     * @param t
     * @return
     */
    boolean save(StudentWriteGrade t);

    /**
     * 修改
     * @param t
     * @return
     */
    boolean update(StudentWriteGrade t);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean delete(int id);

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    StudentWriteGrade getById(int id);

    /**
     * 总行数
     * @return
     */
    int getCount();

    /**
     * 分页查询
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PageBean<StudentWriteGrade> pageList(int pageIndex, int pageSize);

    /**
     * 根据条件查询总行数
     * @param values
     * @return
     */
    int getCount(String values);

    /**
     * 根据条件分页查询
     * @param pageIndex
     * @param pageSize
     * @param value
     * @return
     */
    PageBean<StudentWriteGrade> pageByValues(int pageIndex,int pageSize,String value);
}
