package org.lq.student.ems.student.service.impl;

import org.lq.student.dao.impl.StudentInfoDaoImpl;
import org.lq.student.dao.StudentInfoDao;

import org.lq.student.ems.student.service.StudentInfoService;
import org.lq.student.entity.StudentInfo;
import org.lq.util.PageBean;

/**
 * @author 秦洪涛
 * @create 2020-10-13-22:32
 */
public class StudentInfoServiceImpl implements StudentInfoService {
    private StudentInfoDao dao=new StudentInfoDaoImpl();
    @Override
    public boolean save(StudentInfo studentInfo) {
        return dao.save(studentInfo)>0;
    }

    @Override
    public boolean delete(int id) {
        return dao.delete(id)>0;
    }

    @Override
    public boolean update(StudentInfo studentInfo) {
        return dao.update(studentInfo)>0;
    }

    @Override
    public StudentInfo getStuById(int id) {
        return dao.getById(id);
    }

    @Override
    public StudentInfo getStudentByName(String name) {
        return dao.getStudentByName(name);
    }

    @Override
    public int getCount(String value) {
        value=value==null ? "" :value;
        return dao.getCount(value);
    }

    @Override
    public PageBean<StudentInfo> pageByValues(int startIndex, int pageSize, String value) {
        value=value==null ? "" :value;
        PageBean<StudentInfo> pageBean=new PageBean<>();
        pageBean.setCurrentPage(startIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(value));
        int start=(startIndex-1)*pageSize;
        pageBean.setData(dao.pageByValues(start,pageSize,value));
        return pageBean;
    }
}
