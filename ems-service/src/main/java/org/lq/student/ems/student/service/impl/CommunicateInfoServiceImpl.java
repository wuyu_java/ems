package org.lq.student.ems.student.service.impl;

import org.lq.student.dao.CommunicateInfoDao;
import org.lq.student.dao.impl.CommunicateInfoDaoImpl;
import org.lq.student.ems.student.service.CommunicateInfoService;
import org.lq.student.entity.CommunicateInfo;
import org.lq.util.PageBean;

/**
 * @author 南以南
 * @create 2020-10-14 16:23
 */
public class CommunicateInfoServiceImpl implements CommunicateInfoService {
    private CommunicateInfoDao communicateInfoDao = new CommunicateInfoDaoImpl();
    @Override
    public boolean save(CommunicateInfo communicateInfo) {
        return communicateInfoDao.save(communicateInfo)>0;
    }

    @Override
    public boolean delete(int id) {
        return communicateInfoDao.delete(id)>0;
    }

    @Override
    public boolean update(CommunicateInfo communicateInfo) {
        return communicateInfoDao.update(communicateInfo)>0;
    }

    @Override
    public CommunicateInfo getById(int id) {
        return communicateInfoDao.getById(id);
    }



    @Override
    public int getCount(String value) {
        value=value==null ? "" :value;
        return communicateInfoDao.getCount(value);
    }

    @Override
    public PageBean<CommunicateInfo> pageByValues(int startIndex, int pageSize, String value) {
        value=value==null ? "" :value;
        PageBean<CommunicateInfo> pageBean=new PageBean<>();
        pageBean.setCurrentPage(startIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(getCount(value));
        int start=(startIndex-1)*pageSize;
        pageBean.setData(communicateInfoDao.pageByValues(start,pageSize,value));
        return pageBean;
    }
}
