package org.lq.student.ems.student.service.impl;

import org.lq.student.dao.EvaluationInfoDao;
import org.lq.student.dao.impl.EvaluationInfoDaoImpl;
import org.lq.student.ems.student.service.EvaluationInfoService;
import org.lq.student.entity.EvaluationInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author hello
 * @create 2020-10 -13
 */
public class EvaluationInfoServiceImpl implements EvaluationInfoService {
    EvaluationInfoDao evaluationInfoDao = new EvaluationInfoDaoImpl();
    @Override
    public boolean save(EvaluationInfo e) {
        return evaluationInfoDao.save(e)>0;
    }

    @Override
    public boolean update(EvaluationInfo e) {
        return evaluationInfoDao.update(e)>0;
    }

    @Override
    public boolean delete(int id) {
        return evaluationInfoDao.delete(id)>0;
    }

    @Override
    public EvaluationInfo getByid(int id) {
        return evaluationInfoDao.getById(id);
    }



    @Override
    public int getCount(String value) {
        value=value==null?" ":value;
        return evaluationInfoDao.getCount(value);
    }

    @Override
    public PageBean<EvaluationInfo> pageByValues(int startIndex, int pageSize, String value) {
        PageBean<EvaluationInfo> pageBean = new PageBean<>();

        pageBean.setCurrentPage(startIndex);
        pageBean.setPageSize(pageSize);
        pageBean.setTotalRows(evaluationInfoDao.getCount(value));

        List<EvaluationInfo> list = evaluationInfoDao.pageByValues(PageBean.countOffset(pageSize, startIndex), pageSize,value);
        pageBean.setData(list);

        return  pageBean;


//        return evaluationInfoDao.pageByValues(startIndex,pageSize,value);
    }

}
