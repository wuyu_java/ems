package org.lq.student.ems.student.service;

import org.lq.student.entity.EvaluationInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author hello
 * @create 2020-10 -13
 */
public interface EvaluationInfoService {

    boolean save(EvaluationInfo e);
    boolean update(EvaluationInfo e);
    boolean delete(int id);
    EvaluationInfo getByid(int id);
//    boolean getCount();
//    List<EvaluationInfo> pageList(int startIndex, int pageSize);
    int getCount(String value);

    PageBean<EvaluationInfo> pageByValues(int startIndex, int pageSize, String value);


}
