package org.lq.student.ems.student.service;

import org.lq.student.entity.StudentInfo;
import org.lq.util.PageBean;

import java.util.List;

/**
 * @author 秦洪涛
 * @create 2020-10-13-22:28
 */
public interface StudentInfoService{

    boolean save(StudentInfo studentInfo);

    boolean delete(int id);

    boolean update(StudentInfo studentInfo);

    StudentInfo getStuById(int id);

    StudentInfo getStudentByName(String name);

    int getCount(String value);

    PageBean<StudentInfo> pageByValues(int startIndex, int pageSize, String value);
}
