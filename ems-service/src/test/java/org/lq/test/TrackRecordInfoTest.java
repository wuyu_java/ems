package org.lq.test;

import org.lq.ems.recruitstudent.service.TrackRecodService;
import org.lq.ems.recruitstudent.service.impl.TrackRecodServiceImpl;
import org.lq.recruitstudent.entity.TrackRecordInfo;

/**
 * @Author 冯志远
 * @create 2020-10-14 18:23
 */
public class TrackRecordInfoTest {
    public static void main(String[] args) {
        TrackRecodService trackRecodService = (TrackRecodService) new TrackRecodServiceImpl();
        TrackRecordInfo trackRecordInfo = new TrackRecordInfo();
        trackRecordInfo.setStudentId(1);
        trackRecordInfo.setTrackRecordTitle("你好aaaa");
        trackRecordInfo.setTrackRecordContent("这是一句话");
        trackRecordInfo.setTrackRecordId(10);
        //添加
//        System.out.println(trackRecodService.save(trackRecordInfo));
        //修改
//        System.out.println(trackRecodService.update(trackRecordInfo));
//        System.out.println(trackRecodService.delete(1));
        //根据id查询
//        System.out.println(trackRecodService.getById(5));
        //根据条件分页
//        PageBean<TrackRecordInfo> pageBean = trackRecodService.pageByValues(1, 2, "", 18);
//        System.out.println(pageBean);

        //获取总行数
//        System.out.println(trackRecodService.getCount());
        //分页
//      PageBean<TrackRecordInfo> pageBean = trackRecodService.pageList(2, 3);
//      System.out.println(pageBean);

//        根据条件获取行数
//        TrackRecordInfoDao trackRecordInfoDao = new TrackRecordInfoDaoImpl();
//        int num = trackRecordInfoDao.getCount("1", "18");
//        System.out.println(num);
        //分页
//        List<TrackRecordInfo> list = trackRecordInfoDao.pageByValues(0,3,"1", "18");
//        System.out.println(list);

    }
}
