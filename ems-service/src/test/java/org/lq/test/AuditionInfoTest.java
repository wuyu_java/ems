package org.lq.test;

import com.google.gson.Gson;
import org.lq.ems.recruitstudent.service.AuditionInfoService;
import org.lq.ems.recruitstudent.service.impl.AuditionInfoServiceImpl;
import org.lq.recruitstudent.entity.AuditionInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author 冯志远
 * @create 2020-10-14 18:43
 */
public class AuditionInfoTest {
    public static void main(String[] args) {

        String  json = "[{\"value\":11,\"title\":\"员工管理\",\"anthortyPid\":2},{\"value\":12,\"title\":\"角色管理\",\"anthortyPid\":2},{\"value\":13,\"title\":\"角色变更\",\"anthortyPid\":2},{\"value\":14,\"title\":\"权限管理\",\"anthortyPid\":2},{\"value\":15,\"title\":\"权限变更\",\"anthortyPid\":2},{\"value\":16,\"title\":\"数据字典\",\"anthortyPid\":2},{\"value\":43,\"title\":\"ceshi\",\"anthortyPid\":2},{\"value\":44,\"title\":\"ceshi\",\"anthortyPid\":2}]";

        Gson gson = new Gson();
        ArrayList<Map<String,Double>> arrayList = gson.fromJson(json, new ArrayList<Map<String, Double>>().getClass());
        arrayList.forEach((e)->{
            System.out.println(e.get("anthortyPid").intValue());
        });

//        AuditionInfoService auditionInfoService = (AuditionInfoService) new AuditionInfoServiceImpl();
//        AuditionInfo auditionInfo = new AuditionInfo();
//        auditionInfo.setAuditionId(5);
//        auditionInfo.setAuditionAddr("aaaa");
//        auditionInfo.setAuditionCourse("eeee");
//        auditionInfo.setStudentId(1);
//        auditionInfo.setAuditionDesc("aawsdfasdf");


//        auditionInfoService.saveAuditionInfo(auditionInfo);
//        auditionInfoService.updateAuditionInfo(auditionInfo);
//        auditionInfoService.deleteAuditionInfoById(5);
//        System.out.println(auditionInfoService.getAuditionInfoById(1));
//        System.out.println(auditionInfoService.getCount("朱","J"));
//        System.out.println(auditionInfoService.pageByValues(1,1,"朱","J"));
    }

}
