package org.lq.test;

import org.lq.ems.recruitstudent.service.StudentInfoService;
import org.lq.ems.recruitstudent.service.impl.StudentInfoServiceImpl;
import org.lq.recruitstudent.entity.StudentInfo;

/**
 * @Author 冯志远
 * @create 2020-10-14 18:13
 */
public class StudentInfoTest {
    public static void main(String[] args) {
        StudentInfoService sis = new StudentInfoServiceImpl();
        StudentInfo si = new StudentInfo();
        si.setStudentId(10);
        si.setClassId(2);
        si.setStudentName("1213212");
        si.setStudentAge(18);
        si.setStaffId(13);
        si.setStudentAddress("新乡市");
//        System.out.println(sis.save(si));
//        System.out.println(sis.update(si));
        System.out.println(sis.delete(12));
//        System.out.println(sis.getCount());
//        System.out.println(sis.pageByValues(1,4,"1",17));
//        System.out.println(sis.getCount("1"));
    }
}
