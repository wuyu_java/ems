package org.lq.test;

import org.junit.Test;
import org.lq.ems.system.service.RoleAnthorityService;
import org.lq.ems.system.service.impl.RoleAnthorityServiceImpl;

import static org.junit.Assert.*;

/**
 * @author 无语
 * @create 2020-10-21 16:26
 */
public class RoleAnthorityServiceImplTest {

    @Test
    public void test(){
        RoleAnthorityService ras = new RoleAnthorityServiceImpl();
        ras.grantAuthorization(12,2,1);
    }

    @Test
    public void revocationAuthorization() {
        RoleAnthorityService ras = new RoleAnthorityServiceImpl();
        ras.revocationAuthorization(11,2,1);
    }

    @Test
    public void getAnthotyByRid() {
        RoleAnthorityService ras = new RoleAnthorityServiceImpl();
        System.out.println(ras.getAnthotyByRid(1));
    }
}